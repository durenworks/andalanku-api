<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require 'plugins/phpmailer/src/Exception.php';
	require 'plugins/phpmailer/src/PHPMailer.php';
	require 'plugins/phpmailer/src/SMTP.php';
	
	$email	= sanitize_sql_string(trim($_REQUEST["email"]));
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email address';
		
		echo json_encode($api_response);
		exit;
	}
	
	if ($email <> '') {

		$queryCheck = "select * from customers where email='$email'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck) == 0) {
			
			$api_response['status'] 		= 'failed';
			$api_response['message'] 		= 'Invalid email address';
			
			echo json_encode($api_response);
			exit;
		}
		else {

			$dataCheck	  = mysqli_fetch_array($resultCheck);
			$verification_code  = $dataCheck['verification_code'];
			$customer_name		= $dataCheck['customer_name'];
		
			$customerName	= $customer_name;
			$customerEmail	= $email;
			$email_activation_url = $frontend_url.'/email_verification?c='.$verification_code;
			$emailSubject = 'Konfirmasi Email Andalanku';
			$emailContent = 'Pengguna Yth, <br><br>';
			$emailContent = $emailContent.'Terima kasih telah melakukan pendaftaran di aplikasi Andalanku.<br>';
			$emailContent = $emailContent.'Kami memerlukan konfirmasi alamat email Anda benar-benar aktif dan dapat digunakan.<br>';
			$emailContent = $emailContent.'Silahkan klik tautan di bawah ini untuk melakukan konfirmasi alamat email Anda ';
			$emailContent = $emailContent.'(atau copy dan paste di browser Anda jika tautan tidak dapat diklik) : <br>';
			$emailContent = $emailContent.'<a href="'.$email_activation_url.'" target="_blank">'.$email_activation_url.'</a><br><br><br>';
			$emailContent = $emailContent."Terima kasih, <br><br>";
			$emailContent = $emailContent."Andalan Finance";
			
			include "mail_engine.php";
			
			$api_response['status'] 		= 'success';
			$api_response['message'] 		= 'Email verification sent';
			
			echo json_encode($api_response);
			exit;
		}
	} else {

		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Invalid email address';
		
		echo json_encode($api_response);
		exit;
	}
?>