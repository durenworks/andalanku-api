<?php

	function afis_call($afis_api_url, $method, $body_param=array(), $is_json = FALSE) {

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $afis_api_url);

		if($method == "POST") {

			$fields_string = '';

			if (!$is_json)
			{
				foreach($body_param as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
				rtrim($fields_string, '&');

				curl_setopt($ch,CURLOPT_POST, count($fields_string));
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			}
			else {
				$fields_string = json_encode($body_param);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    'Content-Type: application/json',
				    'Content-Length: ' . strlen($fields_string))
				);
			}

			//var_dump($fields_string);die();
		}

		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$result = curl_exec($ch);

		return $result;
	}
	
	function espay_call($espay_url, $body_param=array()) {
		
		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $espay_url);
			
		$fields_string = '';
		
		foreach($body_param as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		curl_setopt($ch, CURLOPT_POST, count($fields_string));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$result = curl_exec($ch);

		return $result;
	}
	
	function ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $json_data) {

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL, $ayopop_api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'KEY:'.$ayopop_api_key,
			'TOKEN:'.$ayopop_token,
			'VERSION:1.0',
			'Content-Type: application/json')
		);
		
		//var_dump($json_data);die();
		
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$result = curl_exec($ch);

		return $result;
	}
?>
