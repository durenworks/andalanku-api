<?php
	ini_set("display_errors","0");
	error_reporting(0);

	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$id_customer = sanitize_int($_REQUEST["id_customer"]);

	$query = "select * from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);

	//var_dump($data);die();
	$id_customer	= $data['id_customer'];
	$id_number		= $data['id_number'];
	$phone_number	= $data['phone_number'];

	//=================================================================================================================
	$afis_api_url  = $afis_api_url.'/Supplier/Info/'.$id_number.'/'.$phone_number;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	//var_dump($id_number);
	//var_dump($phone_number);
	$andalan_customer_id =  trim($responseArray[0]->CustomerID);

	$customerName		= $responseArray[0]->Name;
	$customerGender		= $responseArray[0]->Gender;
	$customerBirthPlace	= $responseArray[0]->BirthPlace;
	$customerBirthDate	= $responseArray[0]->BirthDate;
	$customerMotherName	= $responseArray[0]->MotherName;

	if($andalan_customer_id <>'' && $andalan_customer_id <>'0') {

		$queryUpdate = "update customers set andalan_customer_id='$andalan_customer_id', 
						gender='$customerGender', customer_name='$customerName',
						place_of_birth='$customerBirthPlace', date_of_birth='$customerBirthDate',
						mother_name='$customerMotherName'
						where id_customer='$id_customer'";
		//var_dump($queryUpdate);die();
		mysqli_query($mysql_connection, $queryUpdate);

		//simpan alamat
		$cityName	= trim($responseArray[0]->City);
		$queryCity	= "select * from regencies where name like '%$cityName'";
		$resultCity	= mysqli_query($mysql_connection, $queryCity);
		if(mysqli_num_rows($resultCity) == 1) {

			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}
		else if(mysqli_num_rows($resultCity) > 1) {

			$queryCity	= "select * from regencies where name = 'KOTA $cityName'";
			$resultCity	= mysqli_query($mysql_connection, $queryCity);
			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}

		$districtName	= trim($responseArray[0]->Kecamatan);
		$queryDistrict	= "select * from districts where name = '$districtName'";
		$resultDistrict	= mysqli_query($mysql_connection, $queryDistrict);
		$dataDistrict	= mysqli_fetch_array($resultDistrict);
		$districtID		= $dataDistrict['id'];

		$villageName	= trim($responseArray[0]->Kelurahan);
		$queryVillage	= "select * from villages where name = '$villageName'";
		$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
		$dataVillage	= mysqli_fetch_array($resultVillage);
		$villageID		= $dataVillage['id'];
		
		if($villageID=='' || $villageID=='0') {
			
			//ambil id kelurahan yang terakhir
			$queryVillage	= "select id from villages where district_id = '$districtID' order by id DESC";
			$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
			$dataVillage	= mysqli_fetch_array($resultVillage);
			$lastVillageID	= $dataVillage['id'];
			$villageID		= $lastVillageID + 1;
			
			$queryVillage	= "insert into villages(id, district_id, name) values('$villageID', '$districtID', '$villageName')";
			mysqli_query($mysql_connection, $queryVillage);
		}

		$zipCode	= trim($responseArray[0]->ZipCode);
		$address	= trim($responseArray[0]->Address);
		$rt			= trim($responseArray[0]->RT);
		$rw			= trim($responseArray[0]->RW);
		
		$addressType = $responseArray[0]->AddressType;

		if($addressType == 'L') $addressType = 'LEGAL';
		else if($addressType == 'D') $addressType = 'DOMICILE';
		else if($addressType == 'O') $addressType = 'OFFICE';
		else $addressType = 'DOMICILE';

		$queryInsert = "insert into address(province_id, regency_id, district_id, village_id,
		 				zip_code, address, rt, rw)
		 				values('$provinceID', '$cityID', '$districtID', '$villageID',
		 				'$zipCode', '$address', '$rt', '$rw')";
		mysqli_query($mysql_connection, $queryInsert);
		$queryID 		= "select id from address where address='$address' order by id DESC LIMIT 1";
		$resultID		= mysqli_query($mysql_connection, $queryID);
		$dataID  		= mysqli_fetch_assoc($resultID);
		$id_address 	= $dataID['id'];
		
		//update dulu yang lain menjadi inactive
		$queryUpdate = "update address_customers set is_active='0' where user_id='$id_customer' and address_type='$addressType' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
		$queryInsert = "insert into address_customers(address_type, user_id,  address_id, is_active)
						values('$addressType', '$id_customer', '$id_address', '1')";
		mysqli_query($mysql_connection, $queryInsert);

		//cari address yang aktif
		/*$queryAddressCustomer = "select address_id from address_customers
											where user_id = '$id_customer' and address_type = '$addressType' and is_active = 1 LIMIT 1";

		$resultAddressCustomer = mysqli_query($mysql_connection, $queryAddressCustomer);
		$dataAddressCustomer = mysqli_fetch_assoc($resultAddressCustomer);
		$id_address = $dataAddressCustomer['address_id'];

		//update address yang aktif
		$queryUpdateAddress = "update address set province_id = '$provinceID', regency_id = '$cityID',
											district_id = '$districtID', village_id = '$villageID', zip_code = '$zipCode',
											address = '$address', rt = '$rt', rw = '$rw'
											where id = '$id_address'";
											//var_dump($queryUpdateAddress);die();
		mysqli_query($mysql_connection, $queryUpdateAddress);*/
	}

	//=================================================================================================================

	$query = "select a.*, b.name as occupation_name
			  from customers a
			  left join occupations b on a.occupation=b.id
			  where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);

	$api_response['status'] 					= 'success';
	$api_response['customer_name'] 				= $data['customer_name'];
	$api_response['andalan_customer_id']		= $data['andalan_customer_id'];
	$api_response['id_number'] 					= $data['id_number'];
	$api_response['email'] 						= $data['email'];
	$api_response['phone_number'] 				= $data['phone_number'];
	$api_response['home_phone_number'] 			= $data['home_phone_number'];
	$api_response['area_code'] 					= $data['area_code'];
	$api_response['place_of_birth'] 			= $data['place_of_birth'];
	$api_response['date_of_birth'] 				= $data['date_of_birth'];
	$api_response['status_pernikahan']			= $data['status_pernikahan'];
	$api_response['pendidikan_terakhir']		= $data['pendidikan_terakhir'];
	$api_response['status_rumah_tinggal']		= $data['status_rumah_tinggal'];
	$api_response['lama_tinggal_bulan']			= $data['lama_tinggal_bulan'];
	$api_response['lama_tinggal_tahun']			= $data['lama_tinggal_tahun'];
	$api_response['mother_name'] 				= $data['mother_name'];
	$api_response['occupation'] 				= $data['occupation_name'];
	$api_response['monthly_income'] 			= $data['monthly_income'];
	$api_response['additional_income'] 			= $data['additional_income'];
	$api_response['side_job'] 					= $data['side_job'];
	$api_response['gender'] 					= $data['gender'];
	$api_response['agent_code'] 				= $data['agent_code'];
	$api_response['referral_code'] 				= $data['referral_code'];
	$api_response['bank_name'] 					= $data['bank_name'];
	$api_response['bank_account_number'] 		= $data['bank_account_number'];
	$api_response['bank_account_holder'] 		= $data['bank_account_holder'];
	$api_response['customer_type'] 				= $data['customer_type'];
	$api_response['kerabat_afi']				= $data['kerabat_afi'];
	$api_response['kerabat_afi_jabatan']		= $data['kerabat_afi_jabatan'];
	$api_response['nip'] 						= $data['nip'];
	$api_response['npwp'] 						= $data['npwp'];
    $api_response['address_domicile'] 			= '';
    $api_response['rt_domicile'] 				= '';
    $api_response['rw_domicile'] 				= '';
    $api_response['village_id_domicile'] 		= '';
    $api_response['village_name_domicile'] 		= '';
    $api_response['district_id_domicile'] 		= 0;
    $api_response['district_name_domicile'] 	= '';
    $api_response['city_id_domicile'] 			= 0;
    $api_response['city_name_domicile'] 		= '';
    $api_response['province_id_domicile'] 		= 0;
    $api_response['province_name_domicile'] 	= '';
    $api_response['zip_code_domicile'] 			= '';
    $api_response['address_legal'] 			    = '';
    $api_response['rt_legal'] 				    = '';
    $api_response['rw_legal'] 				    = '';
    $api_response['village_id_legal'] 		    = 0;
    $api_response['village_name_legal'] 		= '';
    $api_response['district_id_legal'] 		    = 0;
    $api_response['district_name_legal'] 	    = '';
    $api_response['city_id_legal'] 			    = 0;
    $api_response['city_name_legal'] 		    = '';
    $api_response['province_id_legal'] 		    = 0;
    $api_response['province_name_legal'] 	    = '';
    $api_response['zip_code_legal'] 			= '';
    $api_response['address_office'] 			= '';
    $api_response['rt_office'] 				    = '';
    $api_response['rw_office'] 				    = '';
    $api_response['village_id_office'] 		    = 0;
    $api_response['village_name_office'] 		= '';
    $api_response['district_id_office'] 		= 0;
    $api_response['district_name_office'] 	    = '';
    $api_response['city_id_office'] 			= 0;
    $api_response['city_name_office'] 		    = '';
    $api_response['province_id_office'] 		= 0;
    $api_response['province_name_office'] 	    = '';
    $api_response['zip_code_office'] 			= '';
	
	if($data['bank_account_image'] <> '')
		$api_response['bank_account_image'] = $backend_url.'/'.$customer_document_image_folder.'/'.$data['bank_account_image'];
	else 
		$api_response['bank_account_image'] = '';
	
	if($data['avatar'] <> '')
		$api_response['avatar'] = $backend_url.'/'.$customer_document_image_folder.'/'.$data['avatar'];
	else 
		$api_response['avatar'] = '';
	
	// CUSTOMER DOCUMENT (Foto KTP, KK, dll..) 
	$api_response['foto_ktp'] 				= '';
	$api_response['foto_npwp'] 				= '';
	$api_response['foto_kk'] 				= '';
	$api_response['foto_slip_gaji'] 		= '';
	$api_response['foto_rekening_koran'] 	= '';
	$api_response['foto_keuangan_lainnya'] 	= '';
	
	$queryDocs 	= "select * from customer_documents where 
				   customer_id='$id_customer' and is_active='1' ";
	$resultDocs	= mysqli_query($mysql_connection, $queryDocs);
	while($dataDocs = mysqli_fetch_array($resultDocs)) {
		
		if($dataDocs['type'] == 'FOTO KTP') $api_response['foto_ktp'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
		else if($dataDocs['type'] == 'FOTO NPWP') $api_response['foto_npwp'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
		else if($dataDocs['type'] == 'FOTO KK') $api_response['foto_kk'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
		else if($dataDocs['type'] == 'FOTO SLIP GAJI') $api_response['foto_slip_gaji'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
		else if($dataDocs['type'] == 'FOTO REKENING KORAN') $api_response['foto_rekening_koran'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
		else if($dataDocs['type'] == 'FOTO KEUANGAN LAINNYA') $api_response['foto_keuangan_lainnya'] = $backend_url.'/'.$customer_document_image_folder.'/'.$dataDocs['image'];
	}


    $queryAddress = "select a.*,
                         b.address, b.rt, b.rw, b.zip_code,
                         b.village_id, b.district_id, b.regency_id, b.province_id,
                         c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
                         from address_customers a
                         left join address b on a.address_id=b.id
                         left join provinces c on b.province_id=c.id
                         left join regencies d on b.regency_id=d.id
                         left join districts e on b.district_id=e.id
                         left join villages f on b.village_id=f.id
                         where a.user_id='$id_customer'
                         and a.address_type='DOMICILE'
                         and a.is_active='1' ";
    $resultAddress= mysqli_query($mysql_connection, $queryAddress);
    $dataAddressDomicile  = mysqli_fetch_array($resultAddress);

    if ($dataAddressDomicile) {
        $api_response['address_domicile'] 			= $dataAddressDomicile['address'];
        $api_response['rt_domicile'] 				= $dataAddressDomicile['rt'];
        $api_response['rw_domicile'] 				= $dataAddressDomicile['rw'];
        $api_response['village_id_domicile'] 		= $dataAddressDomicile['village_id'];
        $api_response['village_name_domicile'] 		= $dataAddressDomicile['village_name'];
        $api_response['district_id_domicile'] 		= $dataAddressDomicile['district_id'];
        $api_response['district_name_domicile'] 	= $dataAddressDomicile['district_name'];
        $api_response['city_id_domicile'] 			= $dataAddressDomicile['regency_id'];
        $api_response['city_name_domicile'] 		= $dataAddressDomicile['city_name'];
        $api_response['province_id_domicile'] 		= $dataAddressDomicile['province_id'];
        $api_response['province_name_domicile'] 	= $dataAddressDomicile['province_name'];
        $api_response['zip_code_domicile'] 			= $dataAddressDomicile['zip_code'];
    }

    $queryAddress = "select a.*,
                             b.address, b.rt, b.rw, b.zip_code,
                             b.village_id, b.district_id, b.regency_id, b.province_id,
                             c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
                             from address_customers a
                             left join address b on a.address_id=b.id
                             left join provinces c on b.province_id=c.id
                             left join regencies d on b.regency_id=d.id
                             left join districts e on b.district_id=e.id
                             left join villages f on b.village_id=f.id
                             where a.user_id='$id_customer'
                             and a.address_type='LEGAL'
                             and a.is_active='1' ";
    $resultAddress= mysqli_query($mysql_connection, $queryAddress);
    $dataAddressLegal  = mysqli_fetch_array($resultAddress);

    if ($dataAddressLegal) {
        $api_response['address_legal'] 			= $dataAddressLegal['address'];
        $api_response['rt_legal'] 				= $dataAddressLegal['rt'];
        $api_response['rw_legal'] 				= $dataAddressLegal['rw'];
        $api_response['village_id_legal'] 		= $dataAddressLegal['village_id'];
        $api_response['village_name_legal'] 	= $dataAddressLegal['village_name'];
        $api_response['district_id_legal'] 		= $dataAddressLegal['district_id'];
        $api_response['district_name_legal'] 	= $dataAddressLegal['district_name'];
        $api_response['city_id_legal'] 			= $dataAddressLegal['regency_id'];
        $api_response['city_name_legal'] 		= $dataAddressLegal['city_name'];
        $api_response['province_id_legal'] 		= $dataAddressLegal['province_id'];
        $api_response['province_name_legal'] 	= $dataAddressLegal['province_name'];
        $api_response['zip_code_legal'] 		= $dataAddressLegal['zip_code'];
    }

	$queryAddress = "select a.*,
                             b.address, b.rt, b.rw, b.zip_code,
                             b.village_id, b.district_id, b.regency_id, b.province_id,
                             c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
                             from address_customers a
                             left join address b on a.address_id=b.id
                             left join provinces c on b.province_id=c.id
                             left join regencies d on b.regency_id=d.id
                             left join districts e on b.district_id=e.id
                             left join villages f on b.village_id=f.id
                             where a.user_id='$id_customer'
                             and a.address_type='OFFICE'
                             and a.is_active='1' ";
    $resultAddress= mysqli_query($mysql_connection, $queryAddress);
    $dataAddressOffice  = mysqli_fetch_array($resultAddress);

    if ($dataAddressOffice) {
        $api_response['address_office'] 		= $dataAddressOffice['address'];
        $api_response['rt_office'] 				= $dataAddressOffice['rt'];
        $api_response['rw_office'] 				= $dataAddressOffice['rw'];
        $api_response['village_id_office'] 		= $dataAddressOffice['village_id'];
        $api_response['village_name_office'] 	= $dataAddressOffice['village_name'];
        $api_response['district_id_office'] 	= $dataAddressOffice['district_id'];
        $api_response['district_name_office'] 	= $dataAddressOffice['district_name'];
        $api_response['city_id_office'] 		= $dataAddressOffice['regency_id'];
        $api_response['city_name_office'] 		= $dataAddressOffice['city_name'];
        $api_response['province_id_office'] 	= $dataAddressOffice['province_id'];
        $api_response['province_name_office'] 	= $dataAddressOffice['province_name'];
        $api_response['zip_code_office'] 		= $dataAddressOffice['zip_code'];
    }

	echo json_encode($api_response);
	exit;
?>
