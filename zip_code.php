<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_village = sanitize_sql_string($_REQUEST["id_village"]);
	
	$query 			= "select name,district_id from villages where id='$id_village'";
	$result			= mysqli_query($mysql_connection, $query);
	$data  			= mysqli_fetch_array($result);
	$village_name 	= $data['name'];
	$id_district 	= $data['district_id'];
	
	$query 			= "select name from districts where id='$id_district'";
	$result			= mysqli_query($mysql_connection, $query);
	$data  			= mysqli_fetch_array($result);
	$district_name 	= $data['name'];
	
	$query 			= "select zip_code from zip_code 
					   where village='$village_name' and district='$district_name'";
	$result			= mysqli_query($mysql_connection, $query);
	$data  			= mysqli_fetch_array($result);
	$zip_code 		= $data['zip_code'];
	
	$api_response['status'] 	= 'success';
	$api_response['zip_code'] 	= $zip_code;
	
	echo json_encode($api_response);
	exit;
?>