<?php 
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	//parameter yang dikirim dari espay
	$transaction_code 	= sanitize_sql_string($_REQUEST['order_id']);
	$rq_datetime		= sanitize_sql_string($_REQUEST['rq_datetime']);
	$espaySignature		= sanitize_sql_string($_REQUEST['signature']);
	
	//data credential merchant Andalanku
	$signature_key = $espay_signature_key;
	
	
	//validate signature
	//untuk memastikan request ke file ini datang dari espay
	$uppercase			= strtoupper('##'.$signature_key.'##'.$rq_datetime.'##'.$transaction_code.'##INQUIRY##');
    $merchantSignature	= hash('sha256', $uppercase);
	
	if($merchantSignature <> $espaySignature) {
		echo '1201;Invalid signature;;;;;';
		$str_log_content = '1201;Invalid signature;;;;;';
		$log_request_date = date('Y-m-d H:i:s');
		$log_request_type = 'Inquiry Failed Espay';
		include "inc-write-log.php";
		exit;
	}
	
	$transCodePrefix = substr($transaction_code, 0, 3);

	if($transCodePrefix == 'INS') {
	
		$query = "select a.id, a.amount, a.admin_fee, a.penalty, a.total_amount, a.penalty_pay, a.payment_expire, b.email    
				  from transaction_installment a 
				  left join customers b on a.id_customer=b.id_customer 
				  where transaction_code='$transaction_code' 
				  and (payment_status!='PAYMENT_DONE' 
				  or payment_status!='EXPIRED')";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			echo '1202;Invalid transaction code;;;;;';
			exit;
		}
	}
	else if($transCodePrefix == 'AYO') {
	
		$query = "select a.id, a.amount, a.total_admin, a.processing_fee, a.payment_expire, b.email    
				  from ppob_transaction a 
				  left join customers b on a.customer_id=b.id_customer 
				  where order_id='$transaction_code' 
				  and (transaction_status!='PAYMENT_DONE'
				  or transaction_status!='EXPIRED')";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			echo '1202;Invalid transaction code;;;;;';
			exit;
		}
	}
	
	$data = mysqli_fetch_array($result);
	
	//cek apakah sudah expired
	$now = date('Y-m-d H:i:s');

	if(strtotime($now) >= strtotime($data['payment_expire'])) {
		echo '1203;Payment Expired;;;;;';
		exit;
	}
	
	$note = 'Pembayaran Transaksi '.$transaction_code;
	
	//update status menjadi PAYMENT
	if($transCodePrefix == 'INS') {
		
		$queryUpdate = "update transaction_installment set payment_status='PAYMENT' 
						where transaction_code='$transaction_code' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
		if($data['penalty_pay'] == 'Y') {
		
			$transaction_amount = $data['total_amount'];
		}
		else {
			
			$transaction_amount = $data['amount'] + $data['admin_fee'];
		}
	}
	else if($transCodePrefix == 'AYO') {
		
		$queryUpdate = "update ppob_transaction set transaction_status='PAYMENT' 
						where transaction_code='$transaction_code' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
		$transaction_amount = $data['amount']; // + $data['total_admin'] + + $data['processing_fee'];
	}
	
	$trx_date = date("d/m/Y H:i:s");	
	$payment_expire = date("d/m/y H:i:s", strtotime($data['payment_expire']));

	$str_log_content = '0;Success;'.$transaction_code.';'.$transaction_amount.';IDR;'.$note.';'.$trx_date.';;;'.$data['email'].';'.$payment_expire;
	$log_request_date = date('Y-m-d H:i:s');
	$log_request_type = 'Inquiry Espay';
	include "inc-write-log.php";

	echo $str_log_content;
	exit;
?>