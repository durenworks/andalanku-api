<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$id_customer		= sanitize_int($_POST['id_customer']);
	$prepayment_date	= sanitize_sql_string(trim($_POST['prepayment_date']));
	$agreement_no		= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($prepayment_date=='' || $agreement_no=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$now = date("Y-m-d");
	
	//cek dulu apakah sudah ada pengajuan untuk tanggal yang sama
	$queryCheck = "select id from prepayment_requests 
				   where customer_id='$id_customer' and agreement_number='$agreement_no' 
				   and prepayment_date = '$prepayment_date' 
				   and (status='SUBMITTED' or status='APPROVED') ";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'already submit';
		
		echo json_encode($api_response);
		exit;
	}
	
	$ticket_number = 'PRP'.rand(0000000, 9999999);
	
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from prepayment_requests where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'PRP'.rand(0000000, 9999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
		
	$queryInsert = "insert into prepayment_requests(ticket_number, customer_id, 
					request_date, agreement_number, prepayment_date, status) 
					values('$ticket_number', '$id_customer', 
					'$now', '$agreement_no', '$prepayment_date', 
					'SUBMITTED')";
	mysqli_query($mysql_connection, $queryInsert);
	
	$now = date("Y-m-d");
	
	$afis_api_url	= $afis_api_url.'/Agreement/PengajuanPelunasan';
	$body_params	= array();
	$body_params['TicketNo']		= $ticket_number;
	$body_params['RequestDate']		= $now;
	$body_params['AgreementNo']		= $agreement_no;
	$body_params['PrepaymentDate']	= $prepayment_date;
	
	$jsonData = json_encode($body_params);
	$queryUpdate = "UPDATE prepayment_requests SET json_data = '$jsonData' WHERE ticket_number = '$ticket_number'";
	mysqli_query($mysql_connection, $queryUpdate);
	
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_params));
	
	if ($afis_response->StatusCode == 500) {
		
		$queryUpdate = "UPDATE prepayment_requests SET afis_success = 0 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	} 
	else {
		$queryUpdate = "UPDATE prepayment_requests SET afis_success = 1 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	}
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'SUBMITTED';
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami terima. ';
	$inbox_message				 .= 'Kami akan segera menindaklanjuti pengajuan Anda.';
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'collateral request', 'E-Request Pelunasan Maju', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Prepayment request successful';
	
	echo json_encode($api_response);
	exit;
?>