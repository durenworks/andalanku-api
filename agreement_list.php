<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$id_customer = sanitize_int($_REQUEST["id_customer"]);
	
	$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id = $data['andalan_customer_id'];
	
	$agreement_list = array();
	
	
	if($andalan_customer_id<>'' && $andalan_customer_id<>'0') {		
			
		$api_url  	   = $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$andalan_customer_id;
		$afis_response = json_decode(afis_call($api_url, 'GET'));
		
		$responseArray = $afis_response->Response->Data;
		
		$i = 0;
		
		$strQuery = "";
		
		$nowSQL = date("Y-m-d H:i:s");
		
		foreach($responseArray as $key=>$value) {
			
			$agreementNo 	= trim($value->agreementNo);
			$branch 		= trim($value->branchfullname);
			$contractstatus = trim($value->contractstatus);
			
			if($agreementNo<>'' && $agreementNo<>'-') {
				
				$tempArray = array();
				
				//sekalian ambil kategori asuransi untuk nomor kontrak ini 
				$api_url  			= $afis_api_url.'/AndalanKu/KlaimAsuransi/'.$agreementNo;
				$afis_response 		= json_decode(afis_call($api_url, 'GET'));
				$responseArrayIns	= $afis_response->Response->Data; 
				$insuranceCategory	= $responseArrayIns[0]->MainCoverage;
				
				//sekalian ambil sisa tagihan
				$now = date("Y-m-d");
				$api_url			= $afis_api_url.'/Agreement/SimulasiPelunasan/'.$now.'/'.$agreementNo;
				$afis_response		= json_decode(afis_call($api_url, 'GET'));
				$responseArrayTag	= $afis_response->Response->Data;
				$sisaPinjaman		= trim($responseArrayTag[0]->SisaPinjaman);
				
				//ambil data tagihan
				$api_url			= $afis_api_url.'/Agreement/detailFinancial/'.$agreementNo;
				$afis_response		= json_decode(afis_call($api_url, 'GET'));
				$responseArrayTag	= $afis_response->Response->Data; 
				
				$jtempo					= trim($responseArrayTag[0]->NextInstallmentDate);
				$tempArray2				= explode("T", $jtempo);
				$jatuh_tempo_berikutnya = $tempArray2[0];
				$tempArray2				= explode("-", $jatuh_tempo_berikutnya);
				$tanggal_jatuh_tempo	= $tempArray2[2];
				
				$tutup_kontrak			= trim($responseArrayTag[0]->MaturityDate);
				$tempArray2				= explode("T", $tutup_kontrak);
				$tutup_kontrak 			= $tempArray2[0];
				$angsuran				= trim($responseArrayTag[0]->InstallmentAmount);
				
				//ambil data kendaraan
				$api_url			= $afis_api_url.'/Agreement/detailUnit/'.$agreementNo;
				$afis_response		= json_decode(afis_call($api_url, 'GET'));
				$responseArrayUnit	= $afis_response->Response->Data;
				$plat_nomor			= trim($responseArrayUnit[0]->LicensePlate);
				
				$strQuery = $strQuery."('$id_customer', '$agreementNo', '$branch', 
										'$insuranceCategory', '$sisaPinjaman', '$nowSQL', '$contractstatus',
										'$tanggal_jatuh_tempo', '$jatuh_tempo_berikutnya', '$tutup_kontrak',
										'$angsuran', '$plat_nomor'),";
											
				$tempArray['agreement_no']			= $agreementNo;
				$tempArray['branch']				= $branch;
				$tempArray['insurance_category']	= $insuranceCategory;
				$tempArray['sisa_pinjaman']			= number_format($sisaPinjaman,0,',','.');
				$tempArray['contract_status']		= $contractstatus;
				$tempArray['tanggal_jatuh_tempo']	= $tanggal_jatuh_tempo;
				$tempArray['jatuh_tempo_berikutnya']= $jatuh_tempo_berikutnya;
				$tempArray['tutup_kontrak']			= $tutup_kontrak;
				$tempArray['angsuran']				= number_format($angsuran,0,',','.');
				$tempArray['plat_nomor']			= $plat_nomor;
				$agreement_list[$i] =  $tempArray;
				$i++;
			}
		}		
	}
	
	if($i > 0) {
		
		//hapus dulu data yang lama
		$query = "delete from agreement_list where customer_id='$id_customer'";
		mysqli_query($mysql_connection, $query);
		
		$queryInsert = "insert into agreement_list(customer_id, agreement_number, branch, insurance_category, 
						sisa_pinjaman, last_update, contract_status, tanggal_jatuh_tempo, jatuh_tempo_berikutnya, 
						tutup_kontrak, angsuran, plat_nomor) values ";
		$strQuery 	 = substr($strQuery, 0, strlen($strQuery)-1);
		$queryInsert = $queryInsert.$strQuery;
		mysqli_query($mysql_connection, $queryInsert);
	}
	else {
		
		//tidak ada respon dari afis, pakai data sebelumnya yang sudah ada di database
		$query = "select * from agreement_list where id_customer='$id_customer'";
		$result= mysqli_query($mysql_connection, $query);
		
		$i = 0;
		
		while($data = mysqli_fetch_array($result)) {
			
			$tempArray = array();
			
			$tempArray['agreement_no']			= $data['agreement_number'];
			$tempArray['insurance_category']	= $data['insurance_category'];
			$tempArray['sisa_pinjaman']			= number_format($data['sisa_pinjaman'],0,',','.');
			$tempArray['contract_status']		= $data['contract_status'];
			$tempArray['tanggal_jatuh_tempo']	= $data['tanggal_jatuh_tempo'];
			$tempArray['jatuh_tempo_berikutnya']= $data['jatuh_tempo_berikutnya'];
			$tempArray['tutup_kontrak']			= $data['tutup_kontrak'];
			$tempArray['angsuran']				= number_format($data['angsuran'],0,',','.');
			$tempArray['plat_nomor']			= $data['plat_nomor'];
			$agreement_list[$i] =  $tempArray;
			$i++;
		}
	}
	
	$api_response['status'] 		= 'success';
	$api_response['agreement_list'] = $agreement_list;
	
	echo json_encode($api_response);
	exit;
?>