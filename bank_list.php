<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$keyword = sanitize_sql_string($_REQUEST["keyword"]);
	
	$query 	= "select * from bank_master 
			   where bank_name like '%$keyword%' or short_name like '%$keyword%' 
			   order by short_name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$bank_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$bank_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 	= 'success';
	$api_response['bank_list'] 	= $bank_list;
	
	echo json_encode($api_response);
	exit;
?>