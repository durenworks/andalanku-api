<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function base64_to_image($backend_folder, $image_folder, $base64_string) {
				
		$extension = 'png';
		$folder = $backend_folder."/".$image_folder;
		
		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;
		
		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = ''; 
		}

		fclose( $ifp ); 

		return $newnamefile; 
	}
	
	$id_customer 	= sanitize_int($_POST['id_customer']);
	$id_card_image	= sanitize_sql_string(trim($_POST['id_card_image']));
	
	//---ALAMAT
	$province_id		= sanitize_int($_POST['province_id']);
	if ($province_id == 0) {
		$province_id = sanitize_sql_string($_POST['province_id']);
		$query = "select id from provinces where name='$province_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id = $data['id'];
	}
	
	$regency_id			= sanitize_int($_POST['regency_id']);
	if ($regency_id == 0) {
		$regency_id = sanitize_sql_string($_POST['regency_id']);
		$query = "select id from regencies where name='$regency_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$regency_id = $data['id'];
	}

	$district_id		= sanitize_int($_POST['district_id']);
	if ($district_id == 0) {
		$district_id = sanitize_sql_string($_POST['district_id']);
		$query = "select id from districts where name='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id = $data['id'];
	}
	
	$village_id			= sanitize_sql_string(trim($_POST['village_id']));
	if ($village_id == 0) {
		$village_id = sanitize_sql_string($_POST['village_id']);
		$query = "select id from villages where name='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id = $data['id'];
	}
	
	$zip_code		= sanitize_sql_string(trim($_POST['zip_code']));
	$address		= sanitize_sql_string(trim($_POST['address']));
	$rt 			= sanitize_sql_string(trim($_POST['rt']));
	$rw				= sanitize_sql_string(trim($_POST['rw']));
	
	$address_type	= sanitize_sql_string(trim($_POST['address_type'])); //DOMICILE, LEGAL, OFFICE
	
	if($id_customer=='0') {
			
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id = $data['andalan_customer_id'];
	
	//jika belum punya andalan_customer_id data tidak perlu lengkap
	if($andalan_customer_id=='' || $andalan_customer_id=='0') {
	
		if($province_id=='0' || $regency_id=='0' || $district_id=='0' || $village_id=='0' ||
		   $zip_code=='' || $address=='' || $rt=='' || $rw=='' ) {
			
			$api_response['status']		= 'failed';
			$api_response['message'] 	= 'Input not complete';
			
			echo json_encode($api_response);
			exit;
		}
	}
	
	//=================================================================================================================
	
	// jika belum punya andalan_customer_id dan belum pernah mengisi alamat maka tidak perlu approval 
	if($andalan_customer_id=='' || $andalan_customer_id=='0' || $address_type=='DOMICILE' || $address_type=='OFFICE') {
		
		//upload gambar id card
		$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $id_card_image); 
		if($imageFileName <> '') {
			
			$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
						VALUES('$id_customer', 'FOTO KTP', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		//insert ke tabel address
		$queryInsert = "insert into address(province_id, regency_id, 
						district_id, village_id, zip_code, 
						address, rt, rw) 
						values('$province_id', '$regency_id', 
						'$district_id', '$village_id', '$zip_code', 
						'$address', '$rt', '$rw')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//ambil id yang terakhir
		$query = "select id from address where address='$address' order by id DESC LIMIT 1";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_address = $data['id'];
		
		//update tabel address_customers
		$queryUpdate = "update address_customers set is_active='0' 
						where address_type='$address_type' and user_id='$id_customer'";
		mysqli_query($mysql_connection, $queryUpdate);
		
		//insert ke tabel address_customers
		$queryInsert = "insert into address_customers(address_type, user_id, 
						address_id, is_active) 
						values('$address_type', '$id_customer', 
						'$id_address', '1')";
		mysqli_query($mysql_connection, $queryInsert);
		
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Profile update request successful';
		$api_response['ticket_number'] 	= '';
	} 
	else if($andalan_customer_id<>'' && $andalan_customer_id<>'0' && $address_type=='LEGAL') {
		
		$ticket_number = 'UP'.rand(000000000, 999999999);
	
		$ticket_number_exist = true;
		
		while($ticket_number_exist) {
			
			$query = "select id from profile_update_request where ticket_number='$ticket_number'";
			$result= mysqli_query($mysql_connection, $query);
			if(mysqli_num_rows($result) == 0) {
				$ticket_number_exist = false;
			}
			else {
				$ticket_number = 'UP'.rand(000000000, 999999999);
			}
		}
		
		$now = date("Y-m-d H:i:s");
		
		//upload gambar id card
		$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $id_card_image); 
		if($imageFileName <> '') {
			
			$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
						VALUES('$id_customer', 'FOTO KTP', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		//insert ke tabel profile_update_request
		$queryInsert = "insert into profile_update_request(ticket_number, user_id, 
						request_date, phone, id_card_image, 
						status) 
						values('$ticket_number', '$id_customer', 
						'$now', '$phone', '$imageFileName', 
						'PROCESS')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//ambil id yang terakhir
		$query = "select id from profile_update_request where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_profile_update_request = $data['id'];
		
		//insert ke tabel address
		$queryInsert = "insert into address(province_id, regency_id, 
						district_id, village_id, zip_code, 
						address, rt, rw) 
						values('$province_id', '$regency_id', 
						'$district_id', '$village_id', '$zip_code', 
						'$address', '$rt', '$rw')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//ambil id yang terakhir
		$query = "select id from address where address='$address' order by id DESC LIMIT 1";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_address = $data['id'];
		
		//insert ke tabel profile_update_request_address
		$queryInsert = "insert into profile_update_request_address(address_type, profile_update_request_id, address_id) 
						values('$address_type', '$id_profile_update_request', '$id_address')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $ticket_number;
		$content['input_date'] 		 = $now;
		$content['status'] 		 	 = 'SUBMITTED';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
		$inbox_message				 .= 'Kami telah menerima permohonan pembaharuan data diri Anda dan akan segera memproses permohonan Anda. ';
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'update profile', 'Update Profile', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================
		
		//==================================== AFIS CALL ====================================
		$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$andalan_customer_id = $data['andalan_customer_id'];
		
		$query = "select name from villages where id='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_name = $data['name'];
		
		$query = "select name from districts where id='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_name = $data['name'];
		
		$query = "select name from regencies where id='$regency_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_name = $data['name'];
		
		$afis_api_url	= $afis_api_url.'/Supplier/pembaharuan';
		
		$body_param		= array();
		$body_param['CustomerId']	= $andalan_customer_id;
		$body_param['AddressType']	= substr($address_type,0,1);
		$body_param['Address']		= $address;
		$body_param['RT']			= $rt;
		$body_param['RW']			= $rw;
		$body_param['Kelurahan']	= $village_name;
		$body_param['Kecamatan']	= $district_name;
		$body_param['City']			= $city_name;
		$body_param['ZipCode']		= $zip_code;
		$body_param['AreaPhone']	= '';
		$body_param['PhoneNo']		= '';
		$body_param['MobileNo']		= '';
		
		$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
		//==================================== AFIS CALL ====================================
		
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Profile update request successful';
		$api_response['ticket_number'] 	= $ticket_number;
	}	
	
	echo json_encode($api_response);
	exit;
?>