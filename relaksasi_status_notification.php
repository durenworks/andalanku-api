<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require 'plugins/phpmailer/src/Exception.php';
	require 'plugins/phpmailer/src/PHPMailer.php';
	require 'plugins/phpmailer/src/SMTP.php';
	
	$agreement_number		= sanitize_sql_string(trim($_POST['agreement_number']));
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$queryCheck = "select customer_id from agreement_list where agreement_number='$agreement_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);

	/*if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid ticket number';
		
		echo json_encode($api_response);
		exit;
	}*/
	
	/*
		NAP = Input Aplikasi
		CST = Analisa Kredit
		APR = Approval Kredit
		GLV = Approved
		RJC = Reject
		CAN = Canceled
	*/
	
	if($status == 'NAP') {
		$status			= 'Input Aplikasi';
		$status_afis	= 'NAP';
	}
	else if($status == 'CST') {
		$status			= 'Analisa Kredit';
		$status_afis	= 'CST';
	}
	else if($status == 'APR') {
		$status			= 'Approval';
		$status_afis	= 'APR';
		$message		= "Selamat! Pengajuan relaksasi kredit anda dengan nomor ". $agreement_number ." telah disetujui, dalam waktu dekat perubahan pola angsuran anda akan segera aktif.";
	}
	else if($status == 'GLV') {
		$status			= 'Approved';
		$status_afis	= 'GLV';
		$message 		= "Konsumen Yth. \r\n Kontrak relaksasi kredit anda dengan nomor ". $agreement_number ." sudah aktif, silahkan cek detail kontrak dan jadwal angsuran anda melalui aplikasi andalanku.id \r\nTerima Kasih.";
	}
	else if($status == 'RJC') {
		$status			= 'Reject';
		$status_afis	= 'RJC';
	}
	else if($status == 'CAN') {
		$status			= 'Canceled';
		$status_afis	= 'CAN';
	}
	else {
		
		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Unknown status code';
		
		echo json_encode($api_response);
		exit;
	}
	
	$now				 = date('Y-m-d H:i:s');
	
	
	//==================== INBOX dan EMAIL ====================
	//$message = 'Dear Konsumen yang terhormat, \r\n\r\n';
	
	$emailContent = 'Pengguna Yth, <br><br>';
	$emailContent = $emailContent.'Terima kasih telah menggunakan aplikasi Andalanku.<br>';
	$emailContent = $emailContent.'Kami telah menerima pengajuan kredit dari Anda dan telah memproses permohonan Anda.<br><br>';
	$emailContent = $emailContent.'Nomor tiket pengajuan kredit Anda adalah : <b>'.$ticket_number.'</b><br><br>';
	
	$content = array();
	$content['message'] 		 = $message;
	$content['ticket_number'] 	 = "";
	$content['status'] 			 = "";
	$content['tangal_pengajuan'] = "";
	$content = json_encode($content);
	$title = "Status Pengajuan Relaksasi Kredit";
	
	//update tabel inbox

	while ($dataCustomer = mysqli_fetch_assoc($resultCheck)) {

		$id_customer = $dataCustomer["customer_id"];
		$queryInsert = "insert into inbox(customer_id, sender_id, 
						date, type, title, content, status) 
						values('$id_customer', '-3', '$now',
						'informasi', '$title', '$content', '0')";
	//var_dump($queryInsert);die();
	}

	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	// Kirim email pemberitahuan kepada orang yang diajukan	
	/*if($loan_application_referal == 'Y') {
		
		$customerName	= $customer_name;
		$customerEmail	= $email;		
		$mail = new PHPMailer();
		$mail->isSMTP();  
		$mail->Host       = $smtp_url;  							
		$mail->Port       = $smtp_port;                             
		$mail->SMTPAuth   = true;                                   
		$mail->Username   = $smtp_username;                     	
		$mail->Password   = $smtp_password;                         
		$mail->setFrom($app_email_from, $app_email_from_name);
		$mail->addAddress($customerEmail, $customerName);
		$mail->isHTML(true);                                  		
		$mail->Subject = $emailSubject;
		$mail->Body    = $emailContent;
		$mail->send();
	}
	
	// Kirim email pemberitahuan kepada user yang melakukan input
	$queryCheck = "select customer_name, email from customers where id_customer='$id_customer'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck	= mysqli_fetch_array($resultCheck);
	$customerName	= $dataCheck['customer_name'];
	$customerEmail	= $dataCheck['email'];
	$mail = new PHPMailer();
	$mail->isSMTP();  
	$mail->Host       = $smtp_url;  							
	$mail->Port       = $smtp_port;                             
	$mail->SMTPAuth   = true;                                   
	$mail->Username   = $smtp_username;                     	
	$mail->Password   = $smtp_password;                         
	$mail->setFrom($app_email_from, $app_email_from_name);
	$mail->addAddress($customerEmail, $customerName);
	$mail->isHTML(true);                                  		
	$mail->Subject = $emailSubject;
	$mail->Body    = $emailContent;
	$mail->send();*/
	
	//=================================================================================================================================
	//=================================================================================================================================
	
	
	// TO-DO : Kirim push notification
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Message Sent successfully';
	
	echo json_encode($api_response);
	exit;
?>