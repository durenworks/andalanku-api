<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	include "jwt.php";
	include "afis_call.php";
	
	$category 	= sanitize_sql_string($_REQUEST["category"]);
	
	if($category=='') {
		
		$api_response['status']			= 'failed';
		$api_response['message'] 		= 'Input not complete';
		$api_response['product_list'] 	= '';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query 	= "select distinct(biller) from ppob_product 
			   where category='$category' and active='1' 
			   order by biller ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$product_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$tempArray = array();
			
		$tempArray['name'] 		= $data['biller'];
		$tempArray['biller'] 	= $data['biller'];
		if($data['logo']<>'') 
			$data['logo'] = $api_url.'/'.$product_logo_image_folder.'/'.$data['logo'];
		else
			$tempArray['logo'] = '';
		
		$product_list[$i] = $tempArray;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['product_list'] 	= $product_list;
	
	echo json_encode($api_response);
	exit;
?>