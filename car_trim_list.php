<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_car_type = sanitize_int($_REQUEST["id_car_type"]);

	$query 	= "select * from car_trims where id_car_type='$id_car_type' and is_active='1' order by name ASC";
	$result = mysqli_query($mysql_connection, $query);
	
	$car_trim_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$car_trim_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['car_trim_list']	= $car_trim_list;
	
	echo json_encode($api_response);
	exit;
?>