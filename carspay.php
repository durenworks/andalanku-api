<?php 
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	$transaction_code = sanitize_sql_string(trim($_REQUEST['transaction_code']));
	$penalty_pay	  = sanitize_sql_string(trim($_REQUEST['penalty_pay']));
	$type			  = sanitize_sql_string(trim($_REQUEST['type']));
	
	if($penalty_pay == '') $penalty_pay = 'Y';
	if($type == '') $type = 'installment';
	
	if($transaction_code == '') {
		echo 'Error 9101 : Empty Transaction Code';
		exit;
	}
	
	if($type == 'installment') {
		
		$query = "select id 
				from transaction_installment 
				where transaction_code='$transaction_code' 
				and (payment_status!='PAYMENT_DONE' 
				or payment_status!='EXPIRED')";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			echo 'Error 9102 : Invalid Transaction Code';
			exit;
		}

		//disimpan dulu apakah customer akan membayar penalti atau tidak
		$queryUpdate = "update transaction_installment set penalty_pay='$penalty_pay' 
		where transaction_code='$transaction_code'";
		$result= mysqli_query($mysql_connection, $queryUpdate);
	}
	else if($type == 'ppob') {
		
		$query = "select id 
				  from ppob_transaction   
				  where order_id='$transaction_code' 
				  and (transaction_status!='PAYMENT_DONE' 
				  or transaction_status!='EXPIRED')";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			echo 'Error 9102 : Invalid Transaction Code';
			exit;
		}
	}
	
?>

<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Andalanku</title>
        <meta name="description" content="An interactive getting started guide for Brackets.">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
        <iframe id="sgoplus-iframe" src="" scrolling="no" frameborder="0"></iframe>
    </body>
    <script type="text/javascript" src="<?php echo $js_file; ?>"></script>
	<script type="text/javascript">
		window.onload = function() {
			var data = {
				key: "<?php echo $espay_api_key; ?>",
				paymentId: "<?php echo $transaction_code; ?>",
				backUrl: "<?php echo $backUrl; ?>"
			},
			sgoPlusIframe = document.getElementById("sgoplus-iframe");
			if (sgoPlusIframe !== null) sgoPlusIframe.src = SGOSignature.getIframeURL(data);
			SGOSignature.receiveForm();
		};
	</script>
</html>