<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	// update data yang sudah expired 
	$now = date('Y-m-d H:i:s');
	$queryUpdate = "update transaction_installment set payment_status='EXPIRED' 
					where payment_expire<'$now' and payment_status = 'PAYMENT'";
					
				//var_dump($queryUpdate);die();
	mysqli_query($mysql_connection, $queryUpdate);

	$id_customer = sanitize_int($_REQUEST["id_customer"]);
	$page 		 = sanitize_int($_REQUEST["page"]);
	
	if($page == '0') $page = '1';
	
	$query 			= "select COUNT(a.id) as num
					   from transaction_installment  
					   where payment_status!='INQUIRY' and id_customer='$id_customer' ";					   
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_assoc($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;
	
	$query 	= "select * 
			   from transaction_installment  
			   where payment_status!='INQUIRY' and id_customer='$id_customer' 
			   order by id DESC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);
	
	$payment_history_list = array();
	
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		//tambahkan detailnya
		$queryDetail  = "select payment_datetime, 
						 debit_from_name, debit_from, 
						 credit_to_name, credit_to, 
						 product_code, 
						 amount as payment_amount , ccy as currency
					     from espay_payment 
						 where id_payment_history='".$data['id']."'";
		$resultDetail = mysqli_query($mysql_connection, $queryDetail);
		$dataDetail   = mysqli_fetch_assoc($resultDetail);
		
		$data['payment_datetime'] 	= $dataDetail['payment_datetime'];
		$data['debit_from_name'] 	= $dataDetail['debit_from_name'];
		$data['debit_from'] 		= $dataDetail['debit_from'];
		$data['credit_to_name'] 	= $dataDetail['credit_to_name'];
		$data['credit_to'] 			= $dataDetail['credit_to'];
		$data['product_code'] 		= $dataDetail['product_code'];
		if ($dataDetail['payment_amount'] != null) {
			$data['payment_amount'] 	= $dataDetail['payment_amount'];
		}
		else {
			if ($data['penalty_pay'] == 'Y') {
				$data['payment_amount'] = $data['total_amount'];
			}
			else {
				$data['payment_amount'] = $data['amount'];
			}
		}
		$data['currency'] 			= $dataDetail['currency'];
		
		$payment_history_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 				= 'success';
	$api_response['payment_history_list'] 	= $payment_history_list;
	
	echo json_encode($api_response);
	exit;
?>