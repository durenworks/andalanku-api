<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$name 	= sanitize_sql_string(trim($_POST['name']));
	$email	= sanitize_sql_string(trim($_POST['email']));
	$phone	= sanitize_sql_string(trim($_POST['phone']));
	$product= sanitize_sql_string(trim($_POST['product']));
	
	//untuk pengajuan via website
	$simulation_type= sanitize_sql_string(trim($_POST['simulation_type']));	// NEW CAR, USED CAR, DANA ANDALANKU
	$plafond		= sanitize_int($_POST['plafond']);						// Khusus untuk DANA ANDALANKU
	$otr			= sanitize_int($_POST['otr']);
	$dp_percent		= sanitize_int($_POST['dp_percent']);
    $dp_amount		= sanitize_int($_POST['dp_amount']);
	$tenor			= sanitize_int($_POST['tenor']);						//(jumlah tenor dalam bulan)
	$insurance		= sanitize_sql_string(trim($_POST['insurance']));		//(TLO, ARK)
	$region			= sanitize_int($_POST['region']);						//(1, 2, 3)
	
	if ($dp_percent > 100) {
        $dp_percent = $dp_percent / 100;
    }
	
	
	if($name=='' || $email=='' || $phone=='' || $product=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email format';
		
		echo json_encode($api_response);
		exit;
	}	
	
	/*$queryCheck = "select id_subscriber from subscribers where email='$email'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Email already registered';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select id_subscriber from subscribers where phone='$phone'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Phone number already registered';
		
		echo json_encode($api_response);
		exit;
	}*/
	
	//=================================================================================================================
	
	$doInsert = true;
	
	if($product == 'none') {
		
		$queryCheck = "select id_subscriber from subscribers where email='$email' or phone='$phone'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck) > 0) $doInsert = false;
	}
	
	if($doInsert) {
		
		$now = date('Y-m-d H:i:s');
		
		/*
		$simulation_type= sanitize_sql_string(trim($_POST['simulation_type']));	// NEW CAR, USED CAR, DANA ANDALANKU
		$plafond		= sanitize_int($_POST['plafond']);						// Khusus untuk DANA ANDALANKU
		$otr			= sanitize_int($_POST['otr']);
		$dp_percent		= sanitize_int($_POST['dp_percent']);
		$dp_amount		= sanitize_int($_POST['dp_amount']);
		$tenor			= sanitize_int($_POST['tenor']);						//(jumlah tenor dalam bulan)
		$insurance		= sanitize_sql_string(trim($_POST['insurance']));		//(TLO, ARK)
		$region			= sanitize_int($_POST['region']);						//(1, 2, 3)
		*/
		
		$queryInsert = "insert into subscribers(name, email, phone, product, created_at,
						simulation_type, plafond, otr, dp_percent, 
						dp_amount, tenor, insurance, region) 
						values('$name', '$email', '$phone', '$product', '$now',
						'$simulation_type', '$plafond', '$otr', '$dp_percent', 
						'$dp_amount', '$tenor', '$insurance', '$region')";
		mysqli_query($mysql_connection, $queryInsert);
	}
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Add subscriber successful';
	
	echo json_encode($api_response);
	exit;
?>