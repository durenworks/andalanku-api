<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$api_url  	   = $afis_api_url.'/Andalanku/BranchList';
	$afis_response = json_decode(afis_call($api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	
	$branch_list = array();
	$i = 0;
	
	foreach($responseArray as $key=>$value) {
		
		$tempArray = array();
		
		$tempArray['branch_id']	= trim($value->BranchID); 
		$tempArray['name']		= trim($value->Name); 
		$tempArray['address']	= trim($value->Address); 
		$tempArray['city']		= trim($value->City); 
		$tempArray['province']	= trim($value->Provinsi); 
		$tempArray['zip_code']	= trim($value->Zipcode); 
		$tempArray['phone']		= trim($value->Phone); 
		$tempArray['fax']		= trim($value->fax); 
		
		$branch_list[$i] =  $tempArray;
		$i++;
	}
	
	
	$api_response['status'] 		= 'success';
	$api_response['branch_list'] 	= $branch_list;
	
	echo json_encode($api_response);
	exit;
?>