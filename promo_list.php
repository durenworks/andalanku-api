<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$promo_type = sanitize_sql_string($_REQUEST["promo_type"]);
	$keyword 	= sanitize_sql_string($_REQUEST["keyword"]);
	$page 	 	= sanitize_int($_REQUEST["page"]);
	
	if($page == '0') $page = '1';
	
	$now = date("Y-m-d");
	
	$query 			= "select COUNT(a.id_promo_type) as num
					   from promos a 
					   left join promo_type b on a.id_promo_type=b.id_promo_type 
					   where (title like '%$keyword%' or content like '%$keyword%') 
					   and (promo_start_date <= '$now' and promo_expired_date >= '$now') ";
					   
	if($promo_type <> '') $query = $query." and b.promo_type_name='$promo_type' ";
					   
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_assoc($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;
	
	$query 	= "select a.*, b.promo_type_name 
			   from promos a 
			   left join promo_type b on a.id_promo_type=b.id_promo_type 
 			   where (title like '%$keyword%' or content like '%$keyword%') 
			   and (promo_start_date <= '$now' and promo_expired_date >= '$now')";
					   
	if($promo_type <> '') $query = $query." and b.promo_type_name='$promo_type' ";
	
	$query = $query." order by promo_date DESC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);
	
	$promo_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$data['image'] = $backend_url."/".$promo_image_folder."/".$data['image'];
		
		$content = $data['content'];
		$data['content']	= $content;
		
		$content = strip_tags($content);
		$data['content_string']		= $content;
		
		$promo_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['promo_list'] 	= $promo_list;
	
	echo json_encode($api_response);
	exit;
?>