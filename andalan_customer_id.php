<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_number 		= sanitize_sql_string(trim($_POST['id_number']));
	$phone_number	= sanitize_sql_string(trim($_POST['phone_number']));
	
	// cek dulu id_customer andalanku
	$queryCheck = "select id_customer from customers where id_number='$id_number' and phone_number='$phone_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Customer not found';
		
		echo json_encode($api_response);
		exit;
	} else {
		
		$dataCheck = mysqli_fetch_array($resultCheck);
		$id_customer = $dataCheck['id_customer'];
	}
	
	if($afis_dummy) {
		
		$andalan_customer_id = '8102018';
		
	} else {
		
		//TO-DO : CALL AFIS
		
	}
	
	if($andalan_customer_id <> '' && $andalan_customer_id <> '0') {
		
		$queryCheck = "update customers set andalan_customer_id='$andalan_customer_id' where id_customer='$id_customer'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	}
	
	$api_response['status'] 				= 'success';
	$api_response['andalan_customer_id'] 	= $andalan_customer_id;
	
	echo json_encode($api_response);
	exit;
?>