<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function downloadFile($url, $path)
	{
		$newfname = $path;
		$file = fopen ($url, 'rb');
		$file_created = false;
		if ($file) {
			
			$file_created = true;
			
			$newf = fopen ($newfname, 'wb');
			if ($newf) {
				while(!feof($file)) {
					fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
				}
			}
		}
		if ($file) {
			fclose($file);
		}
		if ($newf) {
			fclose($newf);
		}
		
		return $file_created;
	}

	$agreement_no 	= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($agreement_no=='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$afis_api_url  = $afis_api_url.'/Agreement/detailInsurance/'.$agreement_no;
	
	$folder_agreement_no = str_replace("/","",$agreement_no);
	$folder 	   = 'files/'.$folder_agreement_no;
	if(!file_exists($folder)) mkdir($folder);
	
	$newFileName   = 'Andalanku_Polis_Asuransi_'.md5($agreement_no).'.pdf';
	
	if(downloadFile($afis_api_url, $folder . '/' . $newFileName)) {
		$api_response['status'] 		= 'success';
		$api_response['kartu_piutang'] 	= $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		
		// jika tidak ada file dari afis maka pakai file yang lama
		if(file_exists($folder . '/' . $newFileName)) {
			$api_response['status'] 		= 'success';
			$api_response['kartu_piutang'] 	= $api_url.'/'.$folder . '/' . $newFileName;
		}
		else {
			$api_response['status'] 		= 'failed';
			$api_response['message'] 		= 'Data belum tersedia';
			$api_response['kartu_piutang'] 	= '';
		}
	}
	
	echo json_encode($api_response);
	exit;
?>