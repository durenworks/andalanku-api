<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$keyword = sanitize_sql_string($_REQUEST["keyword"]);
	$page 	 = sanitize_int($_REQUEST["page"]);
	if($page == '0') $page = '1';
	
	$query 			= "select COUNT(*) as num
					   from news
					   where title like '%$keyword%' or content like '%$keyword%' ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_assoc($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;
	
	$query 	= "select *
			   from news
 			   where title like '%$keyword%' or content like '%$keyword%'
			   order by news_date DESC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);
	
	$news_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$data['image'] 			= $backend_url."/".$news_image_folder."/".$data['image'];
		
		$content = $data['content'];
		$data['content']	= $content;
		
		$content = strip_tags($content);
		$data['content_string']		= $content;
		
		$news_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 	= 'success';
	$api_response['news_list'] 	= $news_list;
	
	echo json_encode($api_response);
	exit;
?>