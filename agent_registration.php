<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function base64_to_image($backend_folder, $image_folder, $base64_string) {
				
		$extension = 'png';
		$folder = $backend_folder."/".$image_folder;
		
		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;
		
		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = ''; 
		}

		fclose( $ifp ); 

		return $newnamefile; 
	}
	
	$id_customer 			= sanitize_int($_POST['id_customer']);
	$nik_upline  			= sanitize_sql_string(trim($_POST['nik_upline']));
	$upline_name  			= sanitize_sql_string(trim($_POST['upline_name']));
	$agent_name  			= sanitize_sql_string(trim($_POST['agent_name']));
	$gender		  			= sanitize_sql_string(trim($_POST['gender']));
	$place_of_birth  		= sanitize_sql_string(trim($_POST['place_of_birth']));
	$date_of_birth  		= sanitize_sql_string(trim($_POST['date_of_birth']));
	$id_card_number			= sanitize_sql_string(trim($_POST['id_card_number']));
	$id_card_image			= sanitize_sql_string(trim($_POST['id_card_image']));
	$area_code				= sanitize_sql_string(trim($_POST['area_code']));
	$home_phone_number		= sanitize_sql_string(trim($_POST['home_phone_number']));
	$phone_number			= sanitize_sql_string(trim($_POST['phone_number']));
	$email					= sanitize_sql_string(trim($_POST['email']));
	$occupation_id			= sanitize_int($_POST['occupation_id']);
	$npwp					= sanitize_sql_string(trim($_POST['npwp']));
	$npwp_image				= sanitize_sql_string(trim($_POST['npwp_image']));
	$branch_id  			= sanitize_sql_string(trim($_POST['branch_id']));
	$status_pernikahan		= sanitize_sql_string(trim($_POST['status_pernikahan']));
	$pendidikan_terakhir	= sanitize_sql_string(trim($_POST['pendidikan_terakhir']));
	$status_rumah_tinggal	= sanitize_sql_string(trim($_POST['status_rumah_tinggal']));

	// KERABAT AFI
	$kerabat_afi			= sanitize_sql_string(trim($_POST['kerabat_afi']));
	$kerabat_afi_jabatan	= sanitize_sql_string(trim($_POST['kerabat_afi_jabatan']));
	
	// LEGAL
	$province_id		= sanitize_int($_POST['province_id']);
	$regency_id			= sanitize_int($_POST['city_id']);
	$district_id		= sanitize_int($_POST['district_id']);
	$village_id			= sanitize_sql_string($_POST['village_id']);
	$zip_code			= sanitize_sql_string(trim($_POST['zip_code']));
	$address			= sanitize_sql_string(trim($_POST['address']));
	$rt 				= sanitize_sql_string(trim($_POST['rt']));
	$rw					= sanitize_sql_string(trim($_POST['rw']));
	
	// DOMICILE
	$province_id_domicile	= sanitize_int($_POST['province_id_domicile']);
	$regency_id_domicile	= sanitize_int($_POST['city_id_domicile']);
	$district_id_domicile	= sanitize_int($_POST['district_id_domicile']);
	$village_id_domicile	= sanitize_sql_string($_POST['village_id_domicile']);
	$zip_code_domicile		= sanitize_sql_string(trim($_POST['zip_code_domicile']));
	$address_domicile		= sanitize_sql_string(trim($_POST['address_domicile']));
	$rt_domicile 			= sanitize_sql_string(trim($_POST['rt_domicile']));
	$rw_domicile			= sanitize_sql_string(trim($_POST['rw_domicile']));
	$lama_tinggal_tahun		= sanitize_int($_POST['lama_tinggal_tahun']);
	$lama_tinggal_bulan		= sanitize_int($_POST['lama_tinggal_bulan']);
	
	$bank_name				= sanitize_sql_string(trim($_POST['bank_name']));
	$bank_account_number	= sanitize_sql_string(trim($_POST['bank_account_number']));
	$bank_account_holder	= sanitize_sql_string(trim($_POST['bank_account_holder']));
	$bank_account_image		= sanitize_sql_string(trim($_POST['bank_account_image']));
	$bank_branch_name		= sanitize_sql_string(trim($_POST['bank_branch_name']));	
	
	/*if( $id_customer  			== '0' ||
		$agent_name  			== ''  ||
		$id_card_image			== ''  ||
		$branch_id				== ''  || 
		$province_id			== '0' ||
		$regency_id				== '0' ||
		$district_id			== '0' ||
		$village_id				== ''  ||
		$zip_code				== ''  ||
		$address				== ''  ||
		$rt 					== ''  ||
		$rw						== ''  ||
		$bank_name				== ''  ||
		$bank_account_number	== ''  ||
		$bank_account_holder	== ''  ||
		$bank_account_image		== ''  ||
		$bank_branch_name		== '')*/
		if( $id_customer  			== '0' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	if ($email <>'' && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email format';
		
		echo json_encode($api_response);
		exit;
	}
	
	
	if($id_card_number <>'' && strlen($id_card_number) <> 16) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid id number. ID number length must be 16 characters';
		
		echo json_encode($api_response);
		exit;
	}
	
	
	//=================================================================================================================

	$ticket_number = 'AGR'.rand(0000000, 9999999);
		
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from register_agent_history where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'AGR'.rand(0000000, 9999999);
		}
	}
	
	if($id_card_image<>'' && strpos($id_card_image,'http')===FALSE) {
		$idCardImageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $id_card_image); 
	} else {
		$idCardImageFileName = '';
	}
	
	if($npwp_image<>'' && strpos($npwp_image,'http')===FALSE) {
		$npwpImageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $npwp_image);
	} else {
		$npwpImageFileName = '';
	}
	
	if($bank_account_image<>'' && strpos($bank_account_image,'http')===FALSE) {
		$bankAccountImageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $bank_account_image);	
	} else {
		$bankAccountImageFileName = '';
	}
	
	$now = date('Y-m-d H:i:s');
	
	$queryInsert = "INSERT INTO register_agent_history (
					  ticket_number,
					  register_agent_date,
					  id_customer,
					  nik_upline,
					  upline_name, 
					  agent_name,
					  gender,
					  place_of_birth,
					  date_of_birth,
					  id_card_number,
					  id_card_image,
					  area_code, 
					  home_phone_number,
					  phone_number,
					  email,
					  occupation_id,
					  npwp,
					  npwp_image,
					  branch_id, 
					  province_id,
					  regency_id,
					  district_id,
					  village_id,
					  zip_code,
					  address,
					  rt,
					  rw,
					  province_id_domicile,
					  regency_id_domicile,
					  district_id_domicile,
					  village_id_domicile,
					  zip_code_domicile,
					  address_domicile,
					  rt_domicile,
					  rw_domicile,
					  lama_tinggal_tahun,
					  lama_tinggal_bulan,
					  bank_name,
					  bank_account_number,
					  bank_account_holder,
					  bank_account_image,
					  bank_branch_name, 
					  status,
					  status_pernikahan,
					  pendidikan_terakhir,
					  status_rumah_tinggal,
					  kerabat_afi,
					  kerabat_afi_jabatan
					)
					VALUES
					  (
						'$ticket_number',
						'$now',
						'$id_customer',
						'$nik_upline',
						'$upline_name', 
						'$agent_name',
						'$gender',
						'$place_of_birth',
						'$date_of_birth',
						'$id_card_number',
						'$idCardImageFileName',
						'$area_code',
						'$home_phone_number',
						'$phone_number',
						'$email',
						'$occupation_id',
						'$npwp',
						'$npwpImageFileName',
						'$branch_id', 
						'$province_id',
						'$regency_id',
						'$district_id',
						'$village_id',
						'$zip_code',
						'$address',
						'$rt',
						'$rw',
						'$province_id_domicile',
						'$regency_id_domicile',
						'$district_id_domicile',
						'$village_id_domicile',
						'$zip_code_domicile',
						'$address_domicile',
						'$rt_domicile',
						'$rw_domicile',
						'$lama_tinggal_tahun',
						'$lama_tinggal_bulan',
						'$bank_name',
						'$bank_account_number',
						'$bank_account_holder',
						'$bankAccountImageFileName',
						'$bank_branch_name',
						'PENDING',
						'$status_pernikahan',
						'$pendidikan_terakhir',
						'$status_rumah_tinggal',
						'$kerabat_afi',
						'$kerabat_afi_jabatan'
					  )";
	mysqli_query($mysql_connection, $queryInsert); 
	
	$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id = $data['andalan_customer_id'];
	
	//update agent code = 000 selama menunggu approval
	$query = "update customers set agent_code='000' where id_customer='$id_customer' ";
	mysqli_query($mysql_connection, $query);
	
	//jika bukan customer afis sekalian update profile
	if($andalan_customer_id=='' || $andalan_customer_id=='0') {
		
		$query = "update customers set customer_name='$agent_name' ";
		
		if($gender <>'') 				$query = $query." ,gender='$gender' ";
		if($place_of_birth <>'') 		$query = $query." ,place_of_birth='$place_of_birth' ";
		if($date_of_birth <>'') 		$query = $query." ,date_of_birth='$date_of_birth' ";
		if($status_pernikahan <>'')		$query = $query." ,status_pernikahan='$status_pernikahan' ";
		if($pendidikan_terakhir <>'')	$query = $query." ,pendidikan_terakhir='$pendidikan_terakhir' ";
		if($status_rumah_tinggal <>'')	$query = $query." ,status_rumah_tinggal='$status_rumah_tinggal' ";
		if($lama_tinggal_bulan <>'')	$query = $query." ,lama_tinggal_bulan='$lama_tinggal_bulan' ";
		if($lama_tinggal_tahun <>'')	$query = $query." ,lama_tinggal_tahun='$lama_tinggal_tahun' ";
		if($id_card_number <>'') 		$query = $query." ,id_number='$id_card_number' "; 
		if($home_phone_number <>'') 	$query = $query." ,home_phone_number='$home_phone_number' ";
		if($phone_number <>'') 			$query = $query." ,phone_number='$phone_number' ";
		if($email <>'') 				$query = $query." ,email='$email' ";
		if($occupation_id <>'0') 		$query = $query." ,occupation='$occupation_id' ";
		if($npwp <>'') 					$query = $query." ,npwp='$npwp' ";
		if($bank_name <>'') 			$query = $query." ,bank_name='$bank_name' ";
		if($bank_account_number <>'') 	$query = $query." ,bank_account_number='$bank_account_number' ";
		if($bank_account_holder <>'') 	$query = $query." ,bank_account_holder='$bank_account_holder'  ";
		if($kerabat_afi <>'')			$query = $query." ,kerabat_afi='$kerabat_afi' ";
		if($kerabat_afi_jabatan <>'')	$query = $query." ,kerabat_afi_jabatan='$kerabat_afi_jabatan' ";
		
		$query = $query." where id_customer='$id_customer' ";
		mysqli_query($mysql_connection, $query);
		
		//upload gambar ktp
		$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
					VALUES('$id_customer', 'FOTO KTP', '$idCardImageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
		
		//upload gambar npwp
		if($npwpImageFileName<>'') {
			$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
						VALUES('$id_customer', 'FOTO NPWP', '$npwpImageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		//upload gambar rekening
		$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
					VALUES('$id_customer', 'FOTO REK BANK', '$bankAccountImageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
		
		
		//===== LEGAL =====
		if($address <> '') {
			
			$queryCheck  = "select address_id from address_customers 
							where user_id='$id_customer' and is_active='1' and address_type='LEGAL'";
			$resultCheck = mysqli_query($mysql_connection, $queryCheck);
			
			if(mysqli_num_rows($resultCheck) > 0) {

				$dataCheck = mysqli_fetch_array($resultCheck);
				$address_id= $dataCheck['address_id'];
			
				//update alamat yang sudah ada 
				$queryUpdate = "update address set 
								province_id='$province_id', 
								regency_id='$city_id',
								district_id='$district_id', 
								village_id='$village_id', 
								zip_code='$zip_code',
								address='$address', 
								rt='$rt', 
								rw='$rw' 
								where id='$address_id' ";
				mysqli_query($mysql_connection, $queryUpdate); 
			}
			else {
				
				//insert ke tabel address
				$queryInsert = "insert into address(province_id, regency_id,
								district_id, village_id, zip_code,
								address, rt, rw)
								values('$province_id', '$city_id',
								'$district_id', '$village_id', '$zip_code',
								'$address', '$rt', '$rw')";
				mysqli_query($mysql_connection, $queryInsert); 

				//ambil id yang terakhir
				$query = "select id from address where address='$address' 
						  and rt='$rt' and rw='$rw' 
						  and village_id='$village_id' and district_id='$district_id' 
						  and regency_id='$city_id' and province_id='$province_id' 
						  order by id DESC LIMIT 1";
				$result= mysqli_query($mysql_connection, $query);
				$data  = mysqli_fetch_array($result);
				$id_address = $data['id'];

				$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
							VALUES('LEGAL', '$id_customer', '$id_address', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		//===== DOMICILE =====
		if($address_domicile <> '') {
			
			$queryCheck  = "select address_id from address_customers 
							where user_id='$id_customer' and is_active='1' and address_type='DOMICILE'";
			$resultCheck = mysqli_query($mysql_connection, $queryCheck);
			
			if(mysqli_num_rows($resultCheck) > 0) {

				$dataCheck = mysqli_fetch_array($resultCheck);
				$address_id= $dataCheck['address_id'];
			
				//update alamat yang sudah ada 
				$queryUpdate = "update address set 
								province_id='$province_id_domicile', 
								regency_id='$city_id_domicile',
								district_id='$district_id_domicile', 
								village_id='$village_id_domicile', 
								zip_code='$zip_code_domicile',
								address='$address_domicile', 
								rt='$rt_domicile', 
								rw='$rw_domicile' 
								where id='$address_id' ";
				mysqli_query($mysql_connection, $queryUpdate);
			}
			else {
				
				//insert ke tabel address
				$queryInsert = "insert into address(province_id, regency_id,
								district_id, village_id, zip_code,
								address, rt, rw)
								values('$province_id_domicile', '$city_id_domicile',
								'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile',
								'$address_domicile', '$rt_domicile', '$rw_domicile')";
				mysqli_query($mysql_connection, $queryInsert);

				//ambil id yang terakhir
				$query = "select id from address where address='$address_domicile' 
							  and rt='$rt_domicile' and rw='$rw_domicile' 
							  and village_id='$village_id_domicile' and district_id='$district_id_domicile' 
							  and regency_id='$city_id_domicile' and province_id='$province_id_domicile' 
							  order by id DESC LIMIT 1";
				$result= mysqli_query($mysql_connection, $query);
				$data  = mysqli_fetch_array($result);
				$id_address = $data['id'];

				$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
							VALUES('DOMICILE', '$id_customer', '$id_address', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
	}
	
	//=================================================================================================================
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'PENDING';
	$inbox_message				 = 'Terima kasih, pendaftaran Agent anda sedang kami proses';
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'agent registration', 'Pendaftaran Agent', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Agent Registration successful';
	
	echo json_encode($api_response);
	exit;
?>