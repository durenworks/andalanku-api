<?php
	ini_set("display_errors","0");
	error_reporting(0);

	include "inc-db.php";
	include "sanitize.inc.php";

	include "validate_token.php";

	$id_customer	= sanitize_int($_REQUEST["id_customer"]);
	$page 	 		= sanitize_int($_REQUEST["page"]);

	if($page == '0') $page = '1';
	
	//ambil tanggal pendaftaran customer 
	$queryCust = "select registration_date from customers where id_customer='$id_customer'";
	$resultCust= mysqli_query($mysql_connection, $queryCust);
	$dataCust	 = mysqli_fetch_array($resultCust);
	$registration_date = $dataCust['registration_date'];

	$query 			= "select COUNT(*) as num
					   from inbox where (customer_id='$id_customer' or customer_id='-1') 
					   and date>='$registration_date'  ";

	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_assoc($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;
	
	//Note : customer_id = -1 merupakan pesan inbox broadcast untuk semua customer
	$query 	= "select *
			   from inbox where (customer_id='$id_customer' or customer_id='-1') 
			   and date>='$registration_date' 
			   order by date DESC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);

	$inbox_list = array();
	$i = 0;

	while ($data = mysqli_fetch_assoc($result)) {
		
			//khusus untuk pesan broadcast, cek apakah user ini pernah membaca
			if($data['customer_id'] == '-1') {
				
				$queryCheck		= "select id from information_blast_user 
								   where id_inbox='".$data['id']."' and id_customer='".$id_customer."'";
				$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
				if(mysqli_num_rows($resultCheck) > 0) $data['status'] = '1';
				else $data['status'] = '0';
			}
		
			$content = strip_tags($data['content']);
			$content = str_replace("\r\n", " ", $content);	
			$content = str_replace("\t", " ", $content);	
			$content = str_replace("&nbsp;", " ", $content);	
			$content = json_decode($content);
			$message = $content->message;
			$message = substr(strip_tags($message), 0, 180);
			
			$message_date	= $data['date'];
			$tempArray		= explode(" ", $message_date);
			$message_date	= $tempArray[0];
			$today			= date("Y-m-d");
			$yesterday		= date("Y-m-d", strtotime("-1 days", strtotime($today)));
			$two_days_ago	= date("Y-m-d", strtotime("-2 days", strtotime($today)));
			
			if($message_date == $today) {
				$display_date = 'Hari Ini';
			}
			else if($message_date == $yesterday) {
				$display_date = 'Kemarin';
			}
			else if($message_date == $two_days_ago) {
				$display_date = '2 Hari lalu';
			}
			else {
				$display_date = date('j M', strtotime($message_date));
			}
			
			$data['display_date'] = $display_date;

			if ($data['type'] == 'promo') {
				$data['date'] = '';
				$data['display_date'] = '';
			}

			$loopInboxId = $data["id"];
			$data['message'] = $message;

			$queryMedia  = "select file_name, file_type
					    from inbox_media
					    where inbox_id = '$loopInboxId' ";
			$resultMedia = mysqli_query($mysql_connection, $queryMedia);
			$media_list	   = array();
			$n = 0;
			
			$media_list = array();
			while ($dataMedia = mysqli_fetch_assoc($resultMedia)) {
				$media['url'] = $backend_url."/".$inbox_file_folder."/".$dataMedia['file_name'];
				$media['file_type'] = $dataMedia['file_type'];
				array_push($media_list, $media);
			}

			
			$data['media_list']= $media_list;
			$content = json_decode($data['content']); 
			$content->message = $message;
			$data['content'] = $content;
			$inbox_list[$i] = $data;
			$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['inbox_list'] 	= $inbox_list;
	
	//========================================================================================
	//ambil tanggal pendaftaran customer 
	$queryCust = "select registration_date from customers where id_customer='$id_customer'";
	$resultCust= mysqli_query($mysql_connection, $queryCust);
	$dataCust	 = mysqli_fetch_array($resultCust);
	$registration_date = $dataCust['registration_date'];
	
	//Note : customer_id = -1 merupakan pesan inbox broadcast untuk semua customer
	$query 	= "select id,customer_id from inbox 
			   where (customer_id='$id_customer' or customer_id='-1') 
			   and date>='$registration_date' and status='0'";
	$result = mysqli_query($mysql_connection, $query);

	$unread_count = 0;

	while ($data = mysqli_fetch_assoc($result)) {
		
		//khusus untuk pesan broadcast, cek apakah user ini pernah membaca
		if($data['customer_id'] == '-1') {
			
			$queryCheck		= "select id from information_blast_user 
							   where id_inbox='".$data['id']."' and id_customer='".$id_customer."'";
			$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
			if(mysqli_num_rows($resultCheck) == 0) {
				$unread_count++;
			}
		}
		else {
			$unread_count++;
		}			
	}
	//========================================================================================

	$api_response['unread_count'] 	= $unread_count;
	
	echo json_encode($api_response);
	exit;
?>
