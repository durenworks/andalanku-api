<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$id_customer		= sanitize_int($_POST['id_customer']);
	$prepayment_date	= sanitize_sql_string(trim($_POST['prepayment_date']));
	$agreement_no		= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($prepayment_date=='' || $agreement_no=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$afis_api_url  = $afis_api_url.'/Agreement/SimulasiPelunasan/'.$prepayment_date.'/'.$agreement_no;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$totalPelunasan=  trim($responseArray[0]->TotalPelunasan); 
	$sisaPinjaman  =  trim($responseArray[0]->SisaPinjaman); 
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Prepayment simulation successful';
	$api_response['totalPelunasan'] = number_format($totalPelunasan,0,',','.');
	$api_response['sisaPinjaman']   = number_format($sisaPinjaman,0,',','.');
	
	echo json_encode($api_response);
	exit;
?>