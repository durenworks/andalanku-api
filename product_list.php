<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	include "jwt.php";
	include "afis_call.php";
	
	$category 	= sanitize_sql_string($_REQUEST["category"]);
	$biller 	= sanitize_sql_string($_REQUEST["biller"]);
	$keyword 	= sanitize_sql_string($_REQUEST["keyword"]);
	
	if($category=='') {
		
		$api_response['status']			= 'failed';
		$api_response['message'] 		= 'Input not complete';
		$api_response['product_list'] 	= '';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query 	= "select * from ppob_product 
			   where category='$category' and active='1' 
			   and (name like '%$keyword%') ";
	if($biller<>'') $query  = $query." and biller='$biller' ";	   
	$query  = $query." order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$product_list = array();
	$i = 0;
	
	//dicocokkan dengan data di ayopop
	//cek product list ayopop
	$payload = json_encode(['partnerId' => $ayopop_api_key]);
	$ayopop_token = createJWTToken($ayopop_api_secret, $payload);
	
	$ayopop_api_url = $ayopop_api_url.'partner/products';
	$ayopop_response = json_decode(ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $payload));
	
	if($ayopop_response->success) {
		
		$productList = $ayopop_response->data;
	}
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		//dicocokkan dengan data di ayopop
		foreach($productList as $product) {
				
			$code 		= $product->code;
			$active 	= $product->active;
			
			if($code == $data['code'] && $active) {
				
				if($data['logo']<>'') $data['logo'] = $api_url.'/'.$product_logo_image_folder.'/'.$data['logo'];
				
				$product_list[$i] = $data;
				$i++;
			}
		}
	}
	
	$api_response['status'] 		= 'success';
	$api_response['product_list'] 	= $product_list;
	
	echo json_encode($api_response);
	exit;
?>