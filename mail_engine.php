<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require 'plugins/phpmailer/src/Exception.php';
	require 'plugins/phpmailer/src/PHPMailer.php';
	require 'plugins/phpmailer/src/SMTP.php';

	$mail = new PHPMailer();

	//Server settings
	$mail->isSMTP();                                            // Set mailer to use SMTP

	//$mail->SMTPDebug = 2;                                       // Enable verbose debug output

	/* DEVELOPMENT
	$mail->Host       = 'smtp.mailtrap.io';  					// Specify main and backup SMTP servers
	$mail->Port       = 2525;                                   // TCP port to connect to
	$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	$mail->Username   = '4fc9c4333f756e';                     	// SMTP username
	$mail->Password   = '4c61842ae16acb';                       // SMTP password
	*/

	$mail->Host       = $smtp_url;  							// Specify main and backup SMTP servers
	$mail->Port       = $smtp_port;                             // TCP port to connect to
	$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
	$mail->Username   = $smtp_username;                     	// SMTP username
	$mail->Password   = $smtp_password;                         // SMTP password

	//Recipients
	$mail->setFrom($app_email_from, $app_email_from_name);
	$mail->addAddress($customerEmail, $customerName);

	// Content
	$mail->isHTML(true);                                  		// Set email format to HTML
	$mail->Subject = $emailSubject;
	$mail->Body    = $emailContent;

	try {
		$mail->send();
	} catch (Exception $ex) { }

?>