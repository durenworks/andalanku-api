<?php
	ini_set("display_errors","0"); 
	error_reporting(1); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select * from faq where is_active='TRUE' order by id_faq ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$faq_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$faqID = $data['id_faq'];
		
		//get faq answer
		$faq_answer_list = array();
		$n = 0;
		
		$queryAnswer  = "select * from faq_answers where is_active='TRUE' and id_faq='".$faqID."'";
		$resultAnswer = mysqli_query($mysql_connection, $queryAnswer);
		$numbering = true;
		//echo ("total answer = " . mysqli_num_rows($resultAnswer));
		
		if (mysqli_num_rows($resultAnswer) == 1) {
			$numbering = false;
		}
		while ($dataAnswer = mysqli_fetch_assoc($resultAnswer)) {
			
			if($dataAnswer['image'] <> '')
				$dataAnswer['image'] = $backend_url."/".$faq_image_folder."/".$dataAnswer['image'];
			
			if ($numbering) {
				$dataAnswer['counter'] = $n+1;
			}
			else {
				$dataAnswer['counter'] = "";
			}
			
			$faq_answer_list[$n] = $dataAnswer;
			$n++;
		}
		
		$data['faq_answer_list'] = $faq_answer_list;
		
		$faq_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 	= 'success';
	$api_response['faq_list'] 	= $faq_list;
	
	echo json_encode($api_response);
	exit;
?>