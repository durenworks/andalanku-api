<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_city = sanitize_int($_REQUEST["id_city"]);
	
	$query 	= "select * from districts where regency_id='$id_city' order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$district_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$district_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['district_list'] 	= $district_list;
	
	echo json_encode($api_response);
	exit;
?>