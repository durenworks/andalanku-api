<?php
	include "inc-db.php";
	
	$customerName	= 'Handi Artiawan';
	$customerEmail	= 'hndi.artiawan@gmail.com';
	$email_activation_url = $frontend_url.'/email_verification?c=123456';
	$emailSubject = 'Konfirmasi Email Andalanku';
	$emailContent = 'Pengguna Yth, <br><br>';
	$emailContent = $emailContent.'Terima kasih telah melakukan pendaftaran di aplikasi Andalanku.<br>';
	$emailContent = $emailContent.'Kami memerlukan konfirmasi alamat email Anda benar-benar aktif dan dapat digunakan.<br>';
	$emailContent = $emailContent.'Silahkan klik tautan di bawah ini untuk melakukan konfirmasi alamat email Anda ';
	$emailContent = $emailContent.'(atau copy dan paste di browser Anda jika tautan tidak dapat diklik) : <br>';
	$emailContent = $emailContent.'<a href="'.$email_activation_url.'" target="_blank">'.$email_activation_url.'</a><br><br><br>';
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";
	
	include "mail_engine.php";
	
	if (!$mail->send()) {
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
		echo 'Message sent!';
	}
?>