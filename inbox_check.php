<?php
	ini_set("display_errors","0");
	error_reporting(0);

	include "inc-db.php";
	include "sanitize.inc.php";

	include "validate_token.php";

	$id_customer	= sanitize_int($_REQUEST["id_customer"]);

	//ambil tanggal pendaftaran customer 
	$queryCust = "select registration_date from customers where id_customer='$id_customer'";
	$resultCust= mysqli_query($mysql_connection, $queryCust);
	$dataCust	 = mysqli_fetch_array($resultCust);
	$registration_date = $dataCust['registration_date'];
	
	//Note : customer_id = -1 merupakan pesan inbox broadcast untuk semua customer
	$query 	= "select id,customer_id from inbox 
			   where (customer_id='$id_customer' or customer_id='-1') 
			   and date>='$registration_date' and status='0'";
	$result = mysqli_query($mysql_connection, $query);

	$unread_count = 0;

	while ($data = mysqli_fetch_assoc($result)) {
		
		//khusus untuk pesan broadcast, cek apakah user ini pernah membaca
		if($data['customer_id'] == '-1') {
			
			$queryCheck		= "select id from information_blast_user 
							   where id_inbox='".$data['id']."' and id_customer='".$id_customer."'";
			$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
			if(mysqli_num_rows($resultCheck) == 0) {
				$unread_count++;
			}
		}
		else {
			$unread_count++;
		}			
	}

	$api_response['status'] 		= 'success';
	$api_response['unread_count'] 	= $unread_count;

	echo json_encode($api_response);
	exit;
?>
