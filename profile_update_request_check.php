<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	$id_customer = sanitize_int($_REQUEST['id_customer']);
	
	if($id_customer=='0') {
			
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		$api_response['is_pending'] = '';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query = "select count(id) as jml 
			  from profile_update_request 
			  where id_customer='$id_customer' 
			  status='PROCESS' ";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$jml_pending = $data['jml'];	
		
	$api_response['status'] = 'success';
	
	if($jml_pending == 0) {
		$api_response['message'] 		= 'Pending profile update request not found';
		$api_response['is_pending'] 	= 'N';		
	}
	else if($jml_pending > 0) {
		$api_response['message'] 		= 'Pending profile update request found';
		$api_response['is_pending'] 	= 'Y';
	}
	
	echo json_encode($api_response);
	exit;
?>