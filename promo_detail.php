<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$slug = sanitize_sql_string(trim($_REQUEST["slug"]));
	
	if($slug=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query 	= "select a.*, b.promo_type_name  
			   from promos a 
			   left join promo_type b on a.id_promo_type=b.id_promo_type  
			   where slug='$slug' ";
	$result = mysqli_query($mysql_connection, $query);
	
	if(mysqli_num_rows($result) == 0) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Invalid promo id';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data = mysqli_fetch_assoc($result);
	
	$content = $data['content'];
	$data['content_html']	= $content;
		
	$content = strip_tags($content);
	$data['content']		= $content;
		
	$api_response['status'] 			= 'success';
	$api_response['id_promo'] 			= $data['id_promo'];
	$api_response['promo_date'] 		= $data['promo_date'];
	$api_response['title'] 				= $data['title'];
	$api_response['promo_type_name']	= $data['promo_type_name'];
	$api_response['content'] 			= $data['content'];
	$api_response['content_html'] 		= $data['content_html'];
	$api_response['promo_start_date'] 	= $data['promo_start_date'];
	$api_response['promo_expired_date'] = $data['promo_expired_date'];
	$api_response['image']				= $backend_url."/".$promo_image_folder."/".$data['image'];
	$api_response['slug'] 				= $data['slug'];
	
	echo json_encode($api_response);
	exit;
?>