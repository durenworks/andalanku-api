<?php
	ini_set("display_errors","0");
	ini_set("max_execution_time","0");	
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	echo "<br>Start<br>";

	$query_customer = "SELECT id_customer, andalan_customer_id from customers 
					   where andalan_customer_id is not null 
					   AND andalan_customer_id != '' ";
	$resultCustomer= mysqli_query($mysql_connection, $query_customer);
	
	while ($dataCustomer = mysqli_fetch_assoc($resultCustomer)) {
		
		$id_customer 			= $dataCustomer["id_customer"];
		$andalan_customer_id 	= $dataCustomer["andalan_customer_id"];
		
		echo "ID Customer : ".$id_customer." --- Andalan customer id : ".$andalan_customer_id."<br>";
		
		//$agreement_list = array();		
		
		if($andalan_customer_id<>'' && $andalan_customer_id<>'0') {		
				
			$api_url  	   = $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$andalan_customer_id;
			$afis_response = json_decode(afis_call($api_url, 'GET'));
			
			$responseArray = $afis_response->Response->Data;
			
			$i = 0;
			
			$strQuery = "";
			
			$nowSQL = date("Y-m-d H:i:s");
			
			foreach($responseArray as $key=>$value) {
				
				$agreementNo = trim($value->agreementNo);
				$branch = trim($value->branchfullname);
				$contractstatus = trim($value->contractstatus);
				
				if($agreementNo<>'' && $agreementNo<>'-') {
					
					$tempArray = array();
					
					//sekalian ambil kategori asuransi untuk nomor kontrak ini 
					$api_url  			= $afis_api_url.'/AndalanKu/KlaimAsuransi/'.$agreementNo;
					$afis_response 		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayIns	= $afis_response->Response->Data; 
					$insuranceCategory	= $responseArrayIns[0]->MainCoverage;
					
					//sekalian ambil sisa tagihan
					$now = date("Y-m-d");
					$api_url			= $afis_api_url.'/Agreement/SimulasiPelunasan/'.$now.'/'.$agreementNo;
					$afis_response		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayTag	= $afis_response->Response->Data;
					$sisaPinjaman		= trim($responseArrayTag[0]->SisaPinjaman);
					
					//ambil data tagihan
					$api_url			= $afis_api_url.'/Agreement/detailFinancial/'.$agreementNo;
					$afis_response		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayTag	= $afis_response->Response->Data; 
					
					$jtempo					= trim($responseArrayTag[0]->NextInstallmentDate);
					$tempArray				= explode("T", $jtempo);
					$jatuh_tempo_berikutnya = $tempArray[0];
					$tempArray				= explode("-", $jatuh_tempo_berikutnya);
					$tanggal_jatuh_tempo	= $tempArray[2];
					
					$tutup_kontrak			= trim($responseArrayTag[0]->MaturityDate);
					$tempArray				= explode("T", $tutup_kontrak);
					$tutup_kontrak 			= $tempArray[0];
					$angsuran				= trim($responseArrayTag[0]->InstallmentAmount);
					
					//ambil data kendaraan
					$api_url			= $afis_api_url.'/Agreement/detailUnit/'.$agreementNo;
					$afis_response		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayUnit	= $afis_response->Response->Data;
					$plat_nomor			= trim($responseArrayUnit[0]->LicensePlate);
					
					$strQuery = $strQuery."('$id_customer', '$agreementNo', '$branch', 
											'$insuranceCategory', '$sisaPinjaman', '$nowSQL', '$contractstatus',  
											'$tanggal_jatuh_tempo', '$jatuh_tempo_berikutnya', '$tutup_kontrak',
											'$angsuran', '$plat_nomor'),";
					
					/*
					$tempArray['agreement_no']			= $agreementNo;
					$tempArray['branch']				= $branch;
					$tempArray['insurance_category']	= $insuranceCategory;
					$tempArray['sisa_pinjaman']			= number_format($sisaPinjaman,0,',','.');
					$agreement_list[$i] =  $tempArray;
					*/
					
					$i++;					
				}
			}		
		}
		
		if($i > 0) {
			
			//hapus dulu data yang lama
			$query = "delete from agreement_list where customer_id='$id_customer'";
			mysqli_query($mysql_connection, $query);
			
			$queryInsert = "insert into agreement_list(customer_id, agreement_number, branch, 
							insurance_category, sisa_pinjaman, last_update, contract_status, 
							tanggal_jatuh_tempo, jatuh_tempo_berikutnya, tutup_kontrak, 
							angsuran, plat_nomor) values ";
			$strQuery 	 = substr($strQuery, 0, strlen($strQuery)-1);
			$queryInsert = $queryInsert.$strQuery;
			mysqli_query($mysql_connection, $queryInsert);
		}
		
		echo "Done<br>";
		ob_flush();
		flush();
	}
	
	echo "<br>Finish<br>";
	
	//echo json_encode('success');
	//exit;
?>