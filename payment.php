<?php 
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "afis_call.php";
	
	function generateRandomString($length = 12) {

		$characters 		= '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	= strlen($characters);
		$randomString 		= '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
	
	//parameter yang dikirim dari espay
	/*
		Parameter Name 					Max Length 	Mandatory 	Description
		rq_uuid 						64 			Yes 		Request identifier. Unique id to identify these messages with other messages
		rq_datetime 					19 			Yes 		Date and time in Espay application server when messages sent
		member_id 						20 			No 			Customer code if listed in the Espay application
		comm_code 						10 			Yes 		Merchant code listed in the Espay application
		order_id 						20 			Yes 		Transaction Number / Invoice Id / unique id that identify the order / billing / invoice.
		password 						32 			No 			Password is used for identification and validation mechanisms by merchant to identify requestis completely derived from the Espay application
		ccy 							3 			Yes 		Transaction’s currency code. ex: IDR / USD
		amount 							13.2 		Yes 		Amount that have been paid by the customer. Format : 100000.00
		debit_from 						19 			No 			Source accounts (accounts that are debited)
		debit_from_name 				64 			No 			Source account name
		credit_to 						19 			No 			Destination account (accounts that are credited)
		credit_to_name 					64 			No 			Destination account name
		product_code 					32 			Yes 		Product code payment
		message 						32 			No 			Additional information of the payment
		payment_datetime 				19 			Yes 		Date and time of payment are listed on the Espay application Format : YYYY-MM-DD hh:mm:ss
		payment_ref 					20 			Yes 		Transaction reference number (used as the identity ofthe transaction) that are recorded on the Espay system
		debit_from_bank 				20 			Yes 		Bank code of source account. example :008, 016, etc
		credit_to_bank 					20 			Yes 		Bank code of destination account. example :008,016, etc
		approval_code_full_bca 			4 			No 			Approval code from the BCA if using a bca’s debit cardtransaction
		approval_code_installment_bca 	4 			No 			Approval code from the BCA if using a bca’s credit cardtransaction
		signature 						64 			Yes 		Signature that need to validate by merchant, see page here for more detail
	*/
	
	$rq_uuid 						= sanitize_sql_string($_REQUEST['rq_uuid']);
	$rq_datetime 					= sanitize_sql_string($_REQUEST['rq_datetime']);
	$member_id 						= sanitize_sql_string($_REQUEST['member_id']);
	$comm_code 						= sanitize_sql_string($_REQUEST['comm_code']);
	$transaction_code				= sanitize_sql_string($_REQUEST['order_id']);
	$password 						= sanitize_sql_string($_REQUEST['password']);
	$ccy 							= sanitize_sql_string($_REQUEST['ccy']);
	$amount 						= sanitize_sql_string($_REQUEST['amount']);
	$debit_from 					= sanitize_sql_string($_REQUEST['debit_from']);
	$debit_from_name 				= sanitize_sql_string($_REQUEST['debit_from_name']);
	$credit_to 						= sanitize_sql_string($_REQUEST['credit_to']);
	$credit_to_name 				= sanitize_sql_string($_REQUEST['credit_to_name']);
	$product_code 					= sanitize_sql_string($_REQUEST['product_code']);
	$message 						= sanitize_sql_string($_REQUEST['message']);
	$payment_datetime 				= sanitize_sql_string($_REQUEST['payment_datetime']);
	$payment_ref 					= sanitize_sql_string($_REQUEST['payment_ref']);
	$debit_from_bank 				= sanitize_sql_string($_REQUEST['debit_from_bank']);
	$credit_to_bank 				= sanitize_sql_string($_REQUEST['credit_to_bank']);
	$approval_code_full_bca 		= sanitize_sql_string($_REQUEST['approval_code_full_bca']);
	$approval_code_installment_bca 	= sanitize_sql_string($_REQUEST['approval_code_installment_bca']);
	$espaySignature					= sanitize_sql_string($_REQUEST['signature']);
	
	$str_log_content  = '';
	$str_log_content .= 'Payment request received from espay : '.date('Y-m-d H:i:s').'\r\n';
	$str_log_content .= '===========================================================================\r\n';
	$str_log_content .= 'rq_uuid : '.$rq_uuid.'\r\n';
	$str_log_content .= 'rq_datetime : '.$rq_datetime.'\r\n';
	$str_log_content .= 'member_id : '.$member_id.'\r\n';
	$str_log_content .= 'comm_code : '.$comm_code.'\r\n';
	$str_log_content .= 'transaction_code : '.$transaction_code.'\r\n';
	$str_log_content .= 'password : '.$password.'\r\n';
	$str_log_content .= 'ccy : '.$ccy.'\r\n';
	$str_log_content .= 'amount : '.$amount.'\r\n';
	$str_log_content .= 'debit_from : '.$debit_from.'\r\n';
	$str_log_content .= 'debit_from_name : '.$debit_from_name.'\r\n';
	$str_log_content .= 'credit_to : '.$credit_to.'\r\n';
	$str_log_content .= 'credit_to_name : '.$credit_to_name.'\r\n';
	$str_log_content .= 'product_code : '.$product_code.'\r\n';
	$str_log_content .= 'message : '.$message.'\r\n';
	$str_log_content .= 'payment_datetime : '.$payment_datetime.'\r\n';
	$str_log_content .= 'payment_ref : '.$payment_ref.'\r\n';
	$str_log_content .= 'debit_from_bank : '.$debit_from_bank.'\r\n';
	$str_log_content .= 'credit_to_bank : '.$credit_to_bank.'\r\n';
	$str_log_content .= 'approval_code_full_bca : '.$approval_code_full_bca.'\r\n';
	$str_log_content .= 'approval_code_installment_bca : '.$approval_code_installment_bca.'\r\n';
	$str_log_content .= 'espaySignature : '.$espaySignature.'\r\n';
	$str_log_content .= '===========================================================================\r\n';
	
	$message = str_replace('"','',$message);

	$transCodePrefix = substr($transaction_code, 0, 3);

	if($transCodePrefix == 'INS') {
		
		$query = "select id, total_amount, contract_no, police_number   
					from transaction_installment 
					where transaction_code='$transaction_code' ";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			
			$str_log_content .= 'Transaction code not found \r\n';
			$str_log_content .= 'Andalanku Response : '.'1203,Invalid transaction code,,,'.'\r\n';
			$log_request_date = date('Y-m-d H:i:s');
			$log_request_type = 'Payment Espay';
			include "inc-write-log.php";
			
			echo '1203,Invalid transaction code,,,';
			exit;
		}

		$data = mysqli_fetch_array($result);
	
		$id_payment_history	= $data['id'];
		$reconcile_id 		= generateRandomString(20);
		$contract_no		= $data['contract_no'];
		$police_number		= $data['police_number'];
		
		//cek dulu jika sudah pernah sukses dengan payment_ref yang sama
		//maka langsung direspon sukses dan tidak perlu melanjutkan proses lagi 
		$query = "SELECT a.*  
					FROM espay_payment a
					LEFT JOIN transaction_installment b ON a.payment_reff=b.espay_payment_reff 
					WHERE a.payment_ref='$payment_ref'
					AND b.payment_status='PAYMENT DONE'";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) > 0) {
			
			$dataPrev = mysqli_fetch_array($result);
			
			$str_log_content .= 'Found previous successful payment from espay \r\n';
			$str_log_content .= 'Andalanku Response : '.'0,Success,'.$dataPrev['reconcile_id'].','.$transaction_code.','.$dataPrev['andalanku_transaction_date'].'\r\n';
			$log_request_date = date('Y-m-d H:i:s');
			$log_request_type = 'Payment Espay';
			include "inc-write-log.php";
			
			echo '0,Success,'.$dataPrev['reconcile_id'].','.$transaction_code.','.$dataPrev['andalanku_transaction_date'];
			exit;
		}
	
	}
	else if($transCodePrefix == 'AYO') {
		
		$query = "select a.*, b.email   
					from ppob_transaction a 
					left join customers b on a.customer_id=b.id_customer 
					where order_id='$transaction_code' ";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			
			$str_log_content .= 'Transaction code not found \r\n';
			$str_log_content .= 'Andalanku Response : '.'1203,Invalid transaction code,,,'.'\r\n';
			$log_request_date = date('Y-m-d H:i:s');
			$log_request_type = 'Payment Espay';
			include "inc-write-log.php";
			
			echo '1203,Invalid transaction code,,,';
			exit;
		}
		
		$data = mysqli_fetch_array($result);
	
		$id_payment_history	= $data['id'];
		$reconcile_id 		= generateRandomString(20);
		$amount				= $data['amount']; // + $data['total_admin'] + $data['processing_fee'];
		
		//cek dulu jika sudah pernah sukses dengan payment_ref yang sama
		//maka langsung direspon sukses dan tidak perlu melanjutkan proses lagi 
		$query = "SELECT a.*  
					FROM espay_payment a
					LEFT JOIN ppob_transaction b ON a.payment_reff=b.espay_payment_reff 
					WHERE a.payment_ref='$payment_ref'
					AND b.transaction_status='PAYMENT DONE'";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) > 0) {
			
			$dataPrev = mysqli_fetch_array($result);
			
			$str_log_content .= 'Found previous successful payment from espay \r\n';
			$str_log_content .= 'Andalanku Response : '.'0,Success,'.$dataPrev['reconcile_id'].','.$transaction_code.','.$dataPrev['andalanku_transaction_date'].'\r\n';
			$log_request_date = date('Y-m-d H:i:s');
			$log_request_type = 'Payment Espay';
			include "inc-write-log.php";
			
			echo '0,Success,'.$dataPrev['reconcile_id'].','.$transaction_code.','.$dataPrev['andalanku_transaction_date'];
			exit;
		}
	}	
	
	//reconcile_id tidak boleh ada yg sama
	$reconcile_id_exist = true;
	while($reconcile_id_exist) {
	
		$query = "select id 
				  from espay_payment 
				  where reconcile_id='$reconcile_id' ";
		$result= mysqli_query($mysql_connection, $query);
		
		if(mysqli_num_rows($result) == 0) {
			$reconcile_id_exist = false;
		}
		else {
			$reconcile_id = generateRandomString();
		}
	}	
	
	$trx_date = date("Y-m-d H:i:s");
	
	$queryInsert = "INSERT INTO espay_payment (
					  rq_uuid,
					  rq_datetime,
					  member_id,
					  comm_code,
					  ccy,
					  amount,
					  debit_from,
					  debit_from_name,
					  credit_to,
					  credit_to_name,
					  product_code,
					  message,
					  payment_datetime,
					  payment_ref,
					  debit_from_bank,
					  credit_to_bank,
					  approval_code_full_bca,
					  approval_code_installment_bca,
					  reconcile_id,
					  signature,
					  password,
					  andalanku_transaction_date
					)
					VALUES
					  (
						'$rq_uuid',
						'$rq_datetime',
						'$member_id',
						'$comm_code',
						'$ccy',
						'$amount',
						'$debit_from',
						'$debit_from_name',
						'$credit_to',
						'$credit_to_name',
						'$product_code',
						'$message',
						'$payment_datetime',
						'$payment_ref',
						'$debit_from_bank',
						'$credit_to_bank',
						'$approval_code_full_bca',
						'$approval_code_installment_bca',
						'$reconcile_id',
						'$espaySignature',
						'$password',
						'$trx_date'
					  )";
	mysqli_query($mysql_connection, $queryInsert);
	
	$str_log_content .= 'Executing query : '. str_replace("'","\'",$queryInsert).' \r\n';
	
	//data credential merchant Andalanku
	$signature_key		= $espay_signature_key;
	$passwordAndalanku	= $espay_password;	
	
	//validate signature
	//untuk memastikan request ke file ini datang dari espay
	$uppercase			= strtoupper('##'.$signature_key.'##'.$rq_datetime.'##'.$transaction_code.'##PAYMENTREPORT##');
    $merchantSignature	= hash('sha256', $uppercase);
	
	if($merchantSignature <> $espaySignature) {
		
		$str_log_content .= 'Invalid signature \r\n';
		$str_log_content .= 'Andalanku Response : '.'1203,Invalid transaction code,,,'.'\r\n';
		$log_request_date = date('Y-m-d H:i:s');
		$log_request_type = 'Payment Espay'; 
		include "inc-write-log.php";
		
		echo '1201,Invalid signature,,,';
		exit;
	}
	
	if($password <> $passwordAndalanku) {
		
		$str_log_content .= 'Invalid password \r\n';
		$str_log_content .= 'Andalanku Response : '.'1203,Invalid transaction code,,,'.'\r\n';
		$log_request_date = date('Y-m-d H:i:s');
		$log_request_type = 'Payment Espay';
		include "inc-write-log.php";
		
		echo '1202,Invalid password,,,';
		exit;
	}

	$finalResult =  '0,Success,'.$reconcile_id.','.$transaction_code.','.$trx_date;
	
	if($transCodePrefix == 'INS') {
		
		$str_log_content .= 'Transaction DONE \r\n';
		
		/* SEND INBOX */
		$title		= "Pembayaran Berhasil";
		$inboxMsg	= "Dear Konsumen yang terhormat, \r\n\r\n";
		$inboxMsg  .= "Kami telah menerima pembayaran Anda untuk transaksi nomor ".$transaction_code." \r\n";
		$inboxMsg  .= "Berikut adalah detail transaksi Anda : \r\n\r\n";
		$inboxMsg  .= "Tanggal Pembayaran : ".$rq_datetime."\r\n";
		$inboxMsg  .= "Jumlah Pembayaran : ".$ccy." ".number_format($amount,0,',','.')."\r\n";
		$inboxMsg  .= "Metode Pembayaran : ".$product_code."\r\n";
		$inboxMsg  .= "Nomor Rekening : ".$debit_from."\r\n";
		$inboxMsg  .= "Nama Pemilik Rekening : ".$debit_from_name."\r\n";
		$inboxMsg  .= "Nomor Referensi : ".$reconcile_id."\r\n";
		$content = '{"message": '.$inboxMsg.'}';
		
		$queryInsert = "insert into inbox(sender_id, customer_id, date, type, title, status, content) 
						values('-2', '$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
		mysqli_query($mysql_connection, $queryInsert);
		
		$str_log_content .= 'Andalanku Response : '.$finalResult.'\r\n';
		
		//========================== AFIS CALL ==========================
		$base_url = $afis_payment_api_url;

		$afis_api_url	= $base_url.'/api/Biller/payment';
		$body_params	= array();
		
		$trx_date		= time();
		$product_code	= 'ANDKU01';
		$merchant_code	= '22691';
		$application_key= $afis_application_key;
		
		$token	= hash('sha256','%'.$contract_no.
								'%'.$payment_ref.
								'%'.$trx_date.
								'%'.$product_code.
								'%'.$merchant_code.
								'%'.$amount.
								'%'.$application_key.'%'); 
		$token	= strtoupper($token);
		
		$body_params['contract_no']		= $contract_no;
		$body_params['total_amount']	= $amount;
		$body_params['reff_id']			= $payment_ref;
		$body_params['trx_date']		= $trx_date;
		$body_params['product_code']	= $product_code;
		$body_params['merchant_code']	= $merchant_code;
		$body_params['token']			= $token;
		
		$str_log_content .= 'Calling AFIS payment : \r\n';
		$str_log_content .= 'contract_no :'.$contract_no.' \r\n';
		$str_log_content .= 'total_amount :'.$amount.' \r\n';
		$str_log_content .= 'reff_id :'.$payment_ref.' \r\n';
		$str_log_content .= 'trx_date :'.$trx_date.' \r\n';
		$str_log_content .= 'product_code :'.$product_code.' \r\n';
		$str_log_content .= 'merchant_code :'.$merchant_code.' \r\n';
		$str_log_content .= 'token :'.$token.' \r\n';
		
		$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_params, true));
		$afis_response_code	= $afis_response->Response_Code;
		$afis_response_desc	= $afis_response->Response_Desc;
		$afis_reference_no	= $afis_response->Reference_No;
		
		$str_log_content .= 'AFIS response: \r\n';
		$str_log_content .= 'Response_Code :'.$afis_response_code.' \r\n';
		$str_log_content .= 'Response_Desc :'.$afis_response_desc.' \r\n';
		$str_log_content .= 'Reference_No :'.$afis_reference_no.' \r\n';
		
		$log_request_date = date('Y-m-d H:i:s');
		$log_request_type = 'Payment Espay';
		include "inc-write-log.php";

		//update status menjadi PAYMENT
		$queryUpdate = "update transaction_installment set 
							payment_status='PAYMENT DONE', 
							espay_payment_reff='$payment_ref',
							afis_response_code='$afis_response_code', 
							afis_response_desc='$afis_response_desc', 
							afis_reference_no='$afis_reference_no'
						where transaction_code='$transaction_code' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
	}	
	else if($transCodePrefix == 'AYO') {
		
		$str_log_content .= 'Transaction DONE \r\n';
		
		/* SEND INBOX */
		$title		= "Pembayaran Berhasil";
		$inboxMsg	= "Dear Konsumen yang terhormat, \r\n\r\n";
		$inboxMsg  .= "Kami telah menerima pembayaran Anda untuk transaksi nomor ".$transaction_code." \r\n";
		$inboxMsg  .= "Berikut adalah detail transaksi Anda : \r\n\r\n";
		$inboxMsg  .= "Tanggal Pembayaran : ".$rq_datetime."\r\n";
		$inboxMsg  .= "Jumlah Pembayaran : ".$ccy." ".number_format($amount,0,',','.')."\r\n";
		$inboxMsg  .= "Metode Pembayaran : ".$product_code."\r\n";
		$inboxMsg  .= "Nomor Rekening : ".$debit_from."\r\n";
		$inboxMsg  .= "Nama Pemilik Rekening : ".$debit_from_name."\r\n";
		$inboxMsg  .= "Nomor Referensi : ".$reconcile_id."\r\n";
		$content = '{"message": '.$inboxMsg.'}';
		
		$queryInsert = "insert into inbox(sender_id, customer_id, date, type, title, status, content) 
						values('-2', '$id_customer', '$now', 'agent registration', '$title', '0', '$content')";
		mysqli_query($mysql_connection, $queryInsert);
		
		$str_log_content .= 'Andalanku Response : '.$finalResult.'\r\n';
		
		$log_request_date = date('Y-m-d H:i:s');
		$log_request_type = 'Payment Espay';
		include "inc-write-log.php";

		//update status menjadi PAYMENT
		$queryUpdate = "update ppob_transaction set 
						transaction_status='PAYMENT DONE', 
						espay_payment_reff='$payment_ref' 
						where order_id='$transaction_code' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
		//kirim request payment ke ayopop
		$arrayBillId  = array();
		$buyerDetails = ([
									'buyerEmail' => $data['email'],
									'publicBuyerId' => $data['customer_id']
							  ]);
		$arrayCallbackURL = array($api_url.'/ppob_finish');
		$payload = json_encode([
									'partnerId' => $ayopop_api_key,
									'accountNumber' => $data['account_number'],
									'productCode' => $data['product_code'],
									'inquiryId' => intval($data['inquiry_id']),
									'amount' => intval($amount),
									'refNumber' => $data['order_id'], 
									'buyerDetails' => $buyerDetails,
									'callbackUrls' => $arrayCallbackURL
							  ],JSON_UNESCAPED_SLASHES);
		//echo $payload.' --- ';					  
		$ayopop_token = createJWTToken($ayopop_api_secret, $payload); //echo $ayopop_token.' --- ';
		
		$ayopop_api_url = $ayopop_api_url.'bill/payment';
		$ayopop_response = json_decode(ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $payload));
	}
	
	echo $finalResult;
	exit;
?>