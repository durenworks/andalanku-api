<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";

	$api_response['status'] = 'success';
	
	$query 	= "select * from settings where setting_key='Andriod Application Version' or setting_key='iOS Application Version' ";
	$result = mysqli_query($mysql_connection, $query);
	while($data = mysqli_fetch_assoc($result)) {
		$api_response[$data['setting_key']]	= $data['setting_value'];
	}
	
	echo json_encode($api_response);
	exit;
?>