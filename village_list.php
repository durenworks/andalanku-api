<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_district = sanitize_int($_REQUEST["id_district"]);
	
	$query 	= "select * from villages where district_id='$id_district' order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$village_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$village_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['village_list'] 	= $village_list;
	
	echo json_encode($api_response);
	exit;
?>