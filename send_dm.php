<?php
    ini_set("display_errors","1"); 
	error_reporting(1); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	function base64_to_image($backend_folder, $media_image_folder, $base64_string) {
				
		$extension = 'png';
		$folder = $backend_folder."/".$media_image_folder;
		
		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;
		
		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = ''; 
		}

		fclose( $ifp ); 

		return $newnamefile; 
	}
	
	

	$destination_id = sanitize_sql_string($_POST['destination_id']);
	$sender_id		= sanitize_int($_POST['sender_id']);
	$title 			= sanitize_sql_string($_POST['title']);
	$content		= sanitize_sql_string($_POST['content']);
	$inbox_message	= json_encode($content);
	$media			= $_POST['media'];
	
	if($destination_id =='' || $sender_id =='' || $title =='' || $content =='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$now = date("Y-m-d H:i:s");

	$content					 = array();
	$content['ticket_number'] 	 = '';
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = '';
	//$inbox_message				 = $content;
	$content['message']			 = substr($inbox_message, 1, -1);
	$content = json_encode($content);
		
	$queryInsert = "insert into inbox(customer_id, sender_id, 
					date, type, title, content, status) 
					values('$destination_id', '$sender_id', '$now',
					'direct message', '$title', '$content', '0')";
	mysqli_query($mysql_connection, $queryInsert);

	//var_dump($queryInsert);die();
	$arrayMediaID = array();
	
	//upload media
	$i = 0;
	$arrayMediaURL = array();

	//ambil id yang terakhir
	$query = "select id from inbox where customer_id='$destination_id' 
				and sender_id = '$sender_id' 
				and date = '$now'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$id_inbox = $data['id'];

	foreach($media as $key=>$mediaFile) {
		
		$imageFileName = base64_to_image($backend_folder, $inbox_file_folder, $mediaFile);
		if($imageFileName <> '') {
			$queryInsert = "insert into inbox_media (inbox_id, file_name, file_type)
								 values('$id_inbox','$imageFileName', 'image')";
			mysqli_query($mysql_connection, $queryInsert);
		}
	}
		
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Message Sent';
	
	echo json_encode($api_response);
	exit;
?>