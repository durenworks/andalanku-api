<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	$id_customer 		= sanitize_int($_REQUEST["id_customer"]);
	$occupation_id		= sanitize_int($_POST['occupation_id']);
	$monthly_income		= sanitize_int($_POST['monthly_income']);
	$additional_income	= sanitize_int($_POST['additional_income']);
	$side_job			= sanitize_sql_string(trim($_POST['side_job']));
	$total_income		= $monthly_income + $additional_income;
	$plafond			= (($total_income / 3) * 36 ) * 0.75;
	
	$ticket_number = 'PFS'.rand(0000000, 9999999);
		
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from plafond_simulations where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'PFS'.rand(0000000, 9999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
	
	$query = "insert into plafond_simulations(ticket_number, 
			  customer_id, simulation_date, 
			  occupation_id, monthly_income, additional_income, 
			  side_job, plafond_amount, status)
			  values ('$ticket_number', 
			  '$id_customer', '$now',  
			  '$occupation_id', '$monthly_income', '$additional_income', 
			  '$side_job', '$plafond', 'SUBMITTED')";
	$result= mysqli_query($mysql_connection, $query);
	
	//update tabel customer 
	$query = "update customers set occupation='$occupation_id', 
			  monthly_income='$monthly_income', additional_income='$additional_income', 
			  side_job='$side_job' where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);	
	
	$api_response['status'] 	= 'success';
	$api_response['message'] 	= 'Plafond simulation successfully';
	$api_response['plafond']	= number_format($plafond, 0, ',', '.');
	
	echo json_encode($api_response);
	exit;
?>