<?php 
	ini_set("display_errors","0"); 
	error_reporting(0);
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$transaction_code	= sanitize_sql_string(trim($_REQUEST['transaction_code']));
	$bank_code			= sanitize_sql_string(trim($_REQUEST['bank_code']));
	$penalty_pay		= sanitize_sql_string(trim($_REQUEST['penalty_pay']));
	$is_dummy			= sanitize_sql_string(trim($_REQUEST['is_dummy']));
	
	if($penalty_pay == '') $penalty_pay = 'Y';
	if($is_dummy == '') $is_dummy = 'N';
	
	if($transaction_code == '' || $bank_code == '') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//disimpan dulu apakah customer akan membayar penalti atau tidak
	$queryUpdate = "update payment_history set penalty_pay='$penalty_pay' 
					where transaction_code='$transaction_code'";
	mysqli_query($mysql_connection, $queryUpdate);
	
	$query = "select a.*, b.customer_name, b.email, b.phone_number 
			  from payment_history a 
			  left join customers b on a.id_customer=b.id_customer 
			  where transaction_code='$transaction_code' 
			  and payment_status='INQUIRY' ";
	$result= mysqli_query($mysql_connection, $query);
	
	if(mysqli_num_rows($result) == 0) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Invalid transaction code';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data = mysqli_fetch_array($result);
	
	if($penalty_pay == 'Y') $amount = $data['total_amount'];
	else $amount = $data['amount'] + $data['admin_fee'];
	
	//data credential merchant Andalanku
	$rq_datetime 	= date('Y-m-d H:i:s');
	$signature_key	= '457d8b3ybi7pl6gz';	
	$uppercase		= strtoupper('##'.$signature_key.'##'.$rq_datetime.'##'.$transaction_code.'##SENDINVOICE##');
    $signature		= hash('sha256', $uppercase);
	
	$body_params 				= array();	
	$body_params['rq_uuid'] 	= md5($espay_api_key.$transaction_code.$bank_code.microtime());
	$body_params['rq_datetime'] = $rq_datetime;
	$body_params['order_id']	= $transaction_code;
	$body_params['ccy'] 		= 'IDR';
	$body_params['comm_code'] 	= 'SGWANDALANKU';
	$body_params['remark1'] 	= $data['phone_number'];
	$body_params['remark2'] 	= $data['customer_name'];
	$body_params['remark3'] 	= $data['email'];
	$body_params['update'] 		= 'N';
	$body_params['bank_code'] 	= $bank_code;
	$body_params['va_expired'] 	= 360;
	$body_params['amount'] 		= $amount;
	$body_params['signature'] 	= $signature;
	
	/*echo '<br>rq_uuid : '.$body_params['rq_uuid']; 	
	echo '<br>rq_datetime : '.$body_params['rq_datetime']; 
	echo '<br>order_id : '.$body_params['order_id'];	
	echo '<br>ccy : '.$body_params['ccy']; 		
	echo '<br>comm_code : '.$body_params['comm_code']; 	
	echo '<br>remark1 : '.$body_params['remark1']; 	
	echo '<br>remark2 : '.$body_params['remark2']; 	
	echo '<br>remark3 : '.$body_params['remark3']; 	
	echo '<br>update : '.$body_params['update']; 		
	echo '<br>bank_code : '.$body_params['bank_code']; 	
	echo '<br>va_expired : '.$body_params['va_expired']; 	
	echo '<br>amount : '.$body_params['amount']; 		
	echo '<br>signature : '.$body_params['signature']; 	
	echo '<br><br>';*/
	
	$espay_response = json_decode(espay_call($url_send_invoice, $body_params));
	
	/*echo "rq_uuid : ".$espay_response->rq_uuid.'<br>';
	echo "rs_datetime : ".$espay_response->rs_datetime.'<br>';
	echo "error_code : ".$espay_response->error_code.'<br>';
	echo "error_message : ".$espay_response->error_message.'<br>';
	echo "va_number : ".$espay_response->va_number.'<br>';
	echo "expired : ".$espay_response->expired.'<br>';
	echo "description : ".$espay_response->description.'<br>';
	echo "total_amount : ".$espay_response->total_amount.'<br>';
	echo "amount : ".$espay_response->amount.'<br>';
	echo "fee : ".$espay_response->fee.'<br>';*/
	
	if($espay_response->error_code == '0000') { 
	
		$queryUpdate = "update payment_history set 
						espay_va_number='".$espay_response->va_number."', 
						espay_expired='".$espay_response->expired."', 
						espay_description='".$espay_response->description."',
						espay_total_amount='".$espay_response->total_amount."',
						espay_amount='".$espay_response->amount."',
						espay_fee='".$espay_response->fee."' 
						where transaction_code='$transaction_code'";
		mysqli_query($mysql_connection, $queryUpdate);
		
		$api_response['status']				= 'success';
		$api_response['message'] 			= 'VA generated successfully';
		$api_response['transaction_code'] 	= $transaction_code;
		$api_response['va_number'] 			= $espay_response->va_number;
		$api_response['expired'] 			= $espay_response->expired;
		$api_response['total_amount']		= $espay_response->total_amount;
		
		echo json_encode($api_response);
		exit;
	}
	else {
		
		if($is_dummy == 'Y') {
			
			$api_response['status']				= 'success';
			$api_response['message'] 			= 'VA generated successfully';
			$api_response['transaction_code'] 	= 'ABCDE12345';
			$api_response['va_number'] 			= '111222333444555';
			$api_response['expired'] 			= '2022-01-31 09:00:00';
			$api_response['total_amount']		= '1525000';
			
			echo json_encode($api_response);
			exit;
		}
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= $espay_response->error_message;
		
		echo json_encode($api_response);
		exit;
	}
?>