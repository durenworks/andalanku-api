<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	include "common_vars.php";
	
	function generateRandomString($length = 5) {

		$characters 		= '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	= strlen($characters);
		$randomString 		= '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
	
	$customer_name  = sanitize_sql_string(trim($_POST['customer_name']));
	$id_number 		= sanitize_sql_string(trim($_POST['id_number']));
	$email			= sanitize_sql_string(trim($_POST['email']));
	$password		= sanitize_sql_string(trim($_POST['password']));
	$phone_number	= sanitize_sql_string(trim($_POST['phone_number']));
	
	if($customer_name=='' || $id_number=='' || $email=='' || $password=='' || $phone_number=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email format';
		
		echo json_encode($api_response);
		exit;
	}
	
	if(strlen($password) < 8) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid password. Password length must be at least 8 characters';
		
		echo json_encode($api_response);
		exit;
	}
	
	if(preg_match("/[A-Z]/", $password)===0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid password. Password must contain at least one upper case character and one number';
		
		echo json_encode($api_response);
		exit;
	}
	if (!preg_match('/[0-9]|[0-9]/', $password))
	{
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid password. Password must contain at least one upper case character and one number';
		
		echo json_encode($api_response);
		exit;
	}
	
	if(strlen($id_number) <> 16) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid id number. ID number length must be 16 characters';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select id_customer from customers where email='$email'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Email already registered';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select id_customer from customers where phone_number='$phone_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Phone number already registered';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select id_customer from customers where id_number='$id_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'ID number already registered';
		
		echo json_encode($api_response);
		exit;
	}

	
	//=================================================================================================================
	
	$referral_code = generateRandomString(5);

	$referral_code_exist = true;

	while($referral_code_exist) {

		$query = "select id_customer from customers where referral_code='$referral_code'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$referral_code_exist = false;
		}
		else {
			$referral_code = generateRandomString(5);
		}
	}
	
	
	$verification_code = md5('andalanku verification '.$email.microtime());
	$sms_otp = rand(100000,999999); 
	$password = md5($password);
	$now = date('Y-m-d H:i:s');
	
	$queryInsert = "insert into customers(customer_name, id_number, email, password, phone_number, 
					status, verification_code, email_verification, 
					sms_otp, sms_verification, registration_date, 
					referral_code) 
					values('$customer_name', '$id_number', '$email', '$password', '$phone_number', 
					'ACTIVE', '$verification_code', 'NOT VERIFIED', 
					'$sms_otp', 'NOT VERIFIED', '$now', 
					'$referral_code')";
	mysqli_query($mysql_connection, $queryInsert);
	
	//ambil id_customer terbaru
	$queryID 		= "select id_customer from customers where email='$email'";
	$resultID		= mysqli_query($mysql_connection, $queryID);
	$dataID  		= mysqli_fetch_assoc($resultID);
	$id_customer 	= $dataID['id_customer'];
	
	//------- TO-DO : Kirim SMS OTP -------
	
	// Kirim Email Konfirmasi
	$customerName	= $customer_name;
	$customerEmail	= $email;
	$email_activation_url = $frontend_url.'/email_verification?c='.$verification_code;
	$emailSubject = 'Konfirmasi Email Andalanku';
	$emailContent = 'Pengguna Yth, <br><br>';
	$emailContent = $emailContent.'Terima kasih telah melakukan pendaftaran di aplikasi Andalanku.<br>';
	$emailContent = $emailContent.'Kami memerlukan konfirmasi alamat email Anda benar-benar aktif dan dapat digunakan.<br>';
	$emailContent = $emailContent.'Silahkan klik tautan di bawah ini untuk melakukan konfirmasi alamat email Anda ';
	$emailContent = $emailContent.'(atau copy dan paste di browser Anda jika tautan tidak dapat diklik) : <br>';
	$emailContent = $emailContent.'<a href="'.$email_activation_url.'" target="_blank">'.$email_activation_url.'</a><br><br><br>';
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";	
	
	require 'plugins/mailjet/vendor/autoload.php';
	use \Mailjet\Resources;
	
	$mj = new \Mailjet\Client($mailjet_api_key, $mailjet_api_secret, true, ['version' => 'v3.1']);
	$body = [
		'Messages' => [
			[
				'From' => [
					'Email' => "info@andalanku.id",
					'Name'  => "Info Andalanku"
				],
				'To' => [
					[
						'Email' => $customerEmail,
						'Name'  => $customerName
					]
				],
				'Subject'  => $emailSubject,
				'TextPart' => $emailContent,
				'HTMLPart' => $emailContent
			]
		]
	];
	$response = $mj->post(Resources::$Email, ['body' => $body]);
	
	//=================================================================================================================
	
	//cek CIF ID di AFIS
	//jika ada, simpan di database lokal
	$afis_api_url  = $afis_api_url.'/Supplier/Info/'.$id_number.'/'.$phone_number;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$andalan_customer_id =  trim($responseArray[0]->CustomerID); 
	
	$customerGender		= $responseArray[0]->Gender;
	$customerBirthPlace	= $responseArray[0]->BirthPlace;
	$customerBirthDate	= $responseArray[0]->BirthDate;
	$customerMotherName	= $responseArray[0]->MotherName;
	
	if($andalan_customer_id <>'' && $andalan_customer_id <>'0') {
		
		$queryUpdate = "update customers set andalan_customer_id = '$andalan_customer_id',
						gender='$customerGender',  place_of_birth='$customerBirthPlace', date_of_birth='$customerBirthDate', 
						mother_name='$customerMotherName' 
						where id_customer='$id_customer'";
		mysqli_query($mysql_connection, $queryUpdate); 
		
		//simpan alamat
		$cityName	= trim($responseArray[0]->City); 
		$queryCity	= "select * from regencies where name like '%$cityName'";
		$resultCity	= mysqli_query($mysql_connection, $queryCity);
		if(mysqli_num_rows($resultCity) == 1) {
			
			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}
		else if(mysqli_num_rows($resultCity) > 1) {
			
			$queryCity	= "select * from regencies where name = 'KOTA $cityName'";
			$resultCity	= mysqli_query($mysql_connection, $queryCity);
			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}
		
		$districtName	= trim($responseArray[0]->Kecamatan); 
		$queryDistrict	= "select * from districts where name = '$districtName'";
		$resultDistrict	= mysqli_query($mysql_connection, $queryDistrict);
		$dataDistrict	= mysqli_fetch_array($resultDistrict);
		$districtID		= $dataDistrict['id'];
		
		$villageName	= trim($responseArray[0]->Kelurahan); 
		$queryVillage	= "select * from villages where name = '$villageName'";
		$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
		$dataVillage	= mysqli_fetch_array($resultVillage);
		$villageID		= $dataVillage['id'];
		
		if($villageID=='' || $villageID=='0') {
			
			//ambil id kelurahan yang terakhir
			$queryVillage	= "select id from villages where district_id = '$districtID' order by id DESC";
			$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
			$dataVillage	= mysqli_fetch_array($resultVillage);
			$lastVillageID	= $dataVillage['id'];
			$villageID		= $lastVillageID + 1;
			
			$queryVillage	= "insert into villages(id, district_id, name) values('$villageID', '$districtID', '$villageName')";
			mysqli_query($mysql_connection, $queryVillage);
		}
		
		$zipCode	= trim($responseArray[0]->ZipCode);
		$address	= trim($responseArray[0]->Address);
		$rt			= trim($responseArray[0]->RT);
		$rw			= trim($responseArray[0]->RW);
		
		$queryInsert = "insert into address(province_id, regency_id, district_id, village_id,  
						zip_code, address, rt, rw) 
						values('$provinceID', '$cityID', '$districtID', '$villageID', 
						'$zipCode', '$address', '$rt', '$rw')";
		mysqli_query($mysql_connection, $queryInsert);
		
		$queryID 		= "select id from address where address='$address' order by id DESC LIMIT 1";
		$resultID		= mysqli_query($mysql_connection, $queryID);
		$dataID  		= mysqli_fetch_assoc($resultID);
		$id_address 	= $dataID['id'];
		
		$addressType = $responseArray[0]->AddressType;
		
		if($addressType == 'L') $addressType = 'LEGAL';
		else if($addressType == 'D') $addressType = 'DOMICILE';
		else if($addressType == 'O') $addressType = 'OFFICE';
		else $addressType = 'DOMICILE';
		
		$queryInsert = "insert into address_customers(address_type, user_id,  address_id, is_active) 
						values('$addressType', '$id_customer', '$id_address', '1')";
		mysqli_query($mysql_connection, $queryInsert);
	}
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Registration successful';
	$api_response['id_customer'] 	= $id_customer;
	
	echo json_encode($api_response);
	exit;
?>