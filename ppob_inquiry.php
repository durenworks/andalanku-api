<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	include "jwt.php";
	include "afis_call.php";
	
	function parseJSONArray($jsonArray) {
		
		$strDetails = '{';
		foreach($jsonArray as $key=>$tempObj) {
			
			$strDetails .= '"'.$tempObj->key.'"';
			$strDetails .= ':';
			$strDetails .= '"'.$tempObj->value.'"';
			$strDetails .= ',';
		}
		$strDetails  = rtrim($strDetails, ',');
		$strDetails .= '}';
		
		return $strDetails;
	}
	
	$id_customer		= sanitize_int($_REQUEST["id_customer"]);
	$account_number		= sanitize_sql_string($_REQUEST["account_number"]);
	$product_code 		= sanitize_sql_string($_REQUEST["product_code"]);
	$zoneId 			= sanitize_sql_string($_REQUEST["zone_id"]);
	$month 				= sanitize_int($_REQUEST["month"]);
	$transaction_group 	= sanitize_sql_string($_REQUEST["transaction_group"]);
	
	$api_response = array();
	
	if($id_customer=='0' || $account_number=='' || $product_code=='') {
		
		$api_response['status']			= 'failed';
		$api_response['message'] 		= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//inquiry ayopop
	$payload = json_encode([
								'partnerId' => $ayopop_api_key,
								'accountNumber' => $account_number,
								'productCode' => $product_code,
								'zoneId' => $zone_id,
								'month' => $month,
						  ]);
	$ayopop_token = createJWTToken($ayopop_api_secret, $payload);
	
	$ayopop_api_url = $ayopop_api_url.'bill/check';
	$ayopop_response = json_decode(ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $payload));
	
	if($ayopop_response->success) {
		
		//generate order id 
		$now 	= date('Y-m-d');
		$query  = "select count(id) as jml from ppob_transaction where date(transaction_date)='$now'";
		$result = mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$jml 	= $data['jml'];
		$jml	= $jml + 1;
		$order_id = 'AYO'.date('ymd').str_pad($jml,3,'0',STR_PAD_LEFT);
		
		$inquiryData = $ayopop_response->data;
		
		$transaction_date 	= date("Y-m-d H:i:s");
		$payment_expire		= date("Y-m-d H:i:s", strtotime('+1 day', strtotime($transaction_date)));
		$account_number		= $inquiryData->accountNumber;
		$zone_id			= $inquiryData->zone_id;
		$month				= $inquiryData->month;
		$inquiry_id			= $inquiryData->inquiryId;
		$product_code		= $inquiryData->productCode;
		$customer_name		= $inquiryData->customerName;
		$product_name		= $inquiryData->productName;
		$category			= $inquiryData->category;
		$amount				= $inquiryData->amount;
		$total_admin		= $inquiryData->totalAdmin;
		$processing_fee		= $inquiryData->processingFee;
		$validity			= $inquiryData->validity;
		$customer_details	= $inquiryData->customerDetail;
		$bill_details		= $inquiryData->billDetails;
		$product_details	= $inquiryData->productDetails;
		$extra_fields		= $inquiryData->extraFields;
		$transaction_status	= 'INQUIRY';
		
		$strCustomerDetails = parseJSONArray($customer_details);
		$strBillDetails 	= parseJSONArray($bill_details);
		$strProductDetails 	= parseJSONArray($product_details);
		$strExtraFields 	= parseJSONArray($extra_fields);
		
		//simpan data inquiry
		$query  = "insert into ppob_transaction(
					customer_id, 
					transaction_date, 	
					order_id,			
					account_number,		
					zone_id,			
					month,				
					inquiry_id,			
					product_code,		
					customer_name,		
					product_name,		
					category,			
					amount,				
					total_admin,	
					processing_fee,		
					validity,			
					customer_details,	
					bill_details,		
					product_details,	
					extra_fields,		
					transaction_status,
					ayopop_status, 
					payment_expire,
					transaction_group					
				   )
				   values(
					'$id_customer', 
					'$transaction_date', 	
					'$order_id',			
					'$account_number',		
					'$zone_id',			
					'$month',				
					'$inquiry_id',			
					'$product_code',		
					'$customer_name',		
					'$product_name',		
					'$category',			
					'$amount',				
					'$total_admin',	
					'$processing_fee',		
					'$validity',			
					'$strCustomerDetails',	
					'$strBillDetails',		
					'$strProductDetails',	
					'$strExtraFields',		
					'$transaction_status',
					'PENDING',
					'$payment_expire',
					'$transaction_group'
				   )";
		mysqli_query($mysql_connection, $query);
		
		$api_response['status'] 			= 'success';
		$api_response['transaction_date'] 	= $transaction_date;
		$api_response['order_id'] 			= $order_id;
		$api_response['account_number'] 	= $account_number;
		$api_response['zone_id'] 			= $zone_id;
		$api_response['month'] 				= $month;
		$api_response['inquiry_id'] 		= $inquiry_id;
		$api_response['product_code'] 		= $product_code;
		$api_response['customer_name'] 		= $customer_name;
		$api_response['product_name'] 		= $product_name;
		$api_response['category'] 			= $category;
		$api_response['amount'] 			= $amount;
		$api_response['total_admin'] 		= $total_admin;
		$api_response['processing_fee'] 	= $processing_fee;
		$api_response['validity'] 			= $validity;
		$api_response['customer_details'] 	= $customer_details;
		$api_response['bill_details'] 		= $bill_details;
		$api_response['product_details'] 	= $product_details;
		$api_response['extra_fields'] 		= $extra_fields;
		$api_response['payment_url']		= $api_url.'/carspay?type=ppob&transaction_code='.$order_id;
		               		
		echo json_encode($api_response);
		exit;
	}
	else {
	
		$api_response['status']			= 'failed';
		$api_response['message'] 		= $ayopop_response->message->ID;
		
		echo json_encode($api_response);
		exit;
	}
?>