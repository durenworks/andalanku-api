<?php 

	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	include "jwt.php";
	include "afis_call.php";	
	
	$transaction_code = 'AYO201008008';
	
	$query = "select *  
				from ppob_transaction  
				where order_id='$transaction_code' ";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	
	$amount= $data['amount']; // + $data['total_admin'] + $data['processing_fee'];
	
	$arrayBillId  = array();
	$buyerDetails = ([
								'buyerEmail' => 'test@gmail.com',
								'publicBuyerId' => $data['customer_id']
						  ]);
	$arrayCallbackURL = array($api_url.'/ppob_finish');
	$payload = json_encode([
								'partnerId' => $ayopop_api_key,
								'accountNumber' => $data['account_number'],
								'productCode' => $data['product_code'],
								'inquiryId' => intval($data['inquiry_id']),
								'amount' => intval($amount),
								'refNumber' => $data['order_id'], 
								'buyerDetails' => $buyerDetails,
								'callbackUrls' => $arrayCallbackURL
						  ],JSON_UNESCAPED_SLASHES);
	//echo $payload.' --- ';					  
	$ayopop_token = createJWTToken($ayopop_api_secret, $payload); //echo $ayopop_token.' --- ';
	
	$ayopop_api_url = $ayopop_api_url.'bill/payment';
	$ayopop_response = json_decode(ayopop_post_json($ayopop_api_url, $ayopop_api_key, $ayopop_token, $payload));
	//print_r($ayopop_response);	

?>