<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

    function base64_to_image($backend_folder, $media_image_folder, $base64_string) {

        $extension = 'png';
        $folder = $backend_folder."/".$media_image_folder;

        $stringrand = md5(microtime());
        $random = substr($stringrand, 0, 16);
        $newnamefile = 'andalanku_' . $random . '.'.$extension;

        while(file_exists($folder . '/' . $newnamefile)) {
            $stringrand = md5(microtime());
            $random = substr($stringrand, 0, 16);
            $newnamefile = 'andalanku_' . $random . '.'.$extension;
        }

        $ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

        if(!fwrite( $ifp, base64_decode($base64_string))) {
            $newnamefile = '';
        }

        fclose( $ifp );

        return $newnamefile;
    }
	
	$id_customer 		= sanitize_int($_REQUEST["id_customer"]);
	$agreement_number	= sanitize_sql_string($_POST['agreement_number']);
	$claim_type			= sanitize_sql_string(trim($_POST['claim_type'])); 	// TLO, ARK
	$event_case			= sanitize_sql_string(trim($_POST['event_case']));	// ST = Stolen, TLO = Total Lost, A = Accident
	$event_date			= sanitize_sql_string(trim($_POST['event_date']));
	$location			= sanitize_sql_string(trim($_POST['location']));
	$notes				= sanitize_sql_string(trim($_POST['notes']));
	$notes				= strip_tags($notes);
    $media			    = $_POST['media'];
	
	
	if($id_customer=='0' || $agreement_number=='' || $claim_type=='' || $event_case=='' || $event_date=='' || $location=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//=================================================================================================================
	
	$ticket_number = 'ISC'.rand(0000000, 9999999);
	
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from insurance_claims where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'ISC'.rand(0000000, 9999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
		
	$queryInsert = "insert into insurance_claims(ticket_number, customer_id, 
					claim_date, agreement_number, claim_type, 
					event_case, event_date, location, 
					notes, status) 
					values('$ticket_number', '$id_customer', 
					'$now', '$agreement_number', '$claim_type', 
					'$event_case', '$event_date', '$location', 
					'$notes', 'SUBMITTED')";
	mysqli_query($mysql_connection, $queryInsert);

    $arrayMediaID = array();

    //upload media
    $i = 0;
    $arrayMediaURL = array();
    foreach($media as $key=>$mediaFile) {

        $imageFileName = base64_to_image($backend_folder, $media_image_folder, $mediaFile);

        if($imageFileName <> '') {
            $queryInsert = "insert into media(url) values('$imageFileName')";
            mysqli_query($mysql_connection, $queryInsert);

            //ambil id yang terakhir
            $query = "select id_media from media where url='$imageFileName'";
            $result= mysqli_query($mysql_connection, $query);
            $data  = mysqli_fetch_array($result);
            $arrayMediaID[] = $data['id_media'];
            $arrayMediaURL[$i] = $backend_url.'/'.$media_image_folder.'/'.$imageFileName;
            $i++;
        }
    }

    //ambil id yang terakhir
    $query = "select id from insurance_claims where ticket_number='$ticket_number'";
    $result= mysqli_query($mysql_connection, $query);
    $data  = mysqli_fetch_array($result);
    $id_claim = $data['id'];

    foreach($arrayMediaID as $key=>$id_media) {

        $queryInsert = "insert into insurance_claim_media(id_claim, id_media) values('$id_claim', '$id_media')";
        mysqli_query($mysql_connection, $queryInsert);
    }
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'SUBMITTED';
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
	$inbox_message				 .= 'Keluhan Anda akan segera kami tindak lanjuti dalam kurun waktu 2 hari kerja.';
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'insurance claim', 'E-Claim Asuransi', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	
	$afis_api_url	= $afis_api_url.'/AndalanKu/KlaimAsuransi';
	
	$body_param		= array();
	$body_param['InsClaimID']	= $ticket_number;
	$body_param['AgreementNo']	= $agreement_number;
	$body_param['ClaimDate']	= $claim_date;
	$body_param['ClaimType']	= $claim_type;
	$body_param['Case']			= $event_case;
	$body_param['Eventdate']	= $event_date;
	$body_param['Location']		= $location;
	$body_param['Notes']		= $notes;
	
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
	//==================================== AFIS CALL ====================================
		
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Add insurance claim successful';
	$api_response['ticket_number'] 	= $ticket_number;
	
	echo json_encode($api_response);
	exit;
?>