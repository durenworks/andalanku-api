<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_inbox 		= sanitize_int(trim($_REQUEST["id_inbox"]));
	$id_customer 	= sanitize_int(trim($_REQUEST["id_customer"]));
	
	if($id_inbox=='0' || $id_customer=='0') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	// cek dulu apakah merupakan pesan broadcast
	$query 	= "select * from inbox where id='$id_inbox'";
	$result = mysqli_query($mysql_connection, $query);
	$data   = mysqli_fetch_array($result);
	$customer_id = $data['customer_id'];
	
	if($customer_id <> '-1') {
	
		if($customer_id <> $id_customer) {
			
			$api_response['status']		= 'failed';
			$api_response['message'] 	= 'Invalid ID';
			
			echo json_encode($api_response);
			exit;
		}
		
		// update pesan ini menjadi sudah dibaca
		$queryUpdate 	= "update inbox set status='1' where id='$id_inbox' and customer_id='$id_customer'";
		mysqli_query($mysql_connection, $queryUpdate);
	}	
	else {
		
		$queryInsert = "delete from information_blast_user where id_inbox='$id_inbox' and id_customer='$id_customer' ";
		mysqli_query($mysql_connection, $queryInsert);
		
		// jika merupakan pesan broadcast, simpan id customer yang sudah membaca
		$queryInsert = "insert into information_blast_user(id_inbox, id_customer) values ('$id_inbox', '$id_customer')";
		mysqli_query($mysql_connection, $queryInsert);
	}
	
	if($data['content_html'] <> '')
		$content_html = $data['content_html'];
	else { 
		
		$content_html = $data['content'];
	
	
		$content_html = str_replace("\r\n", "<br>", $content_html);	
		$content_html = str_replace("\t", "&Tab;", $content_html);	
		$content_html = json_decode($content_html);
		//$content_html = $content_html->message; 
		
		if($content_html->message <> '') {
			$content_html = $content_html->message; 
		}
		else if($content_html->complain_message <> '') {
			$content_html = $content_html->complain_message; 
		}
		$content_html = str_replace("\r\n", "<br>", $content_html);	
		$content_html = str_replace("\t", "&Tab;", $content_html);	
		$content_html = '<pre>'.$content_html.'</pre>';
	}
	
	$queryMedia  = "select file_name, file_type 
					    from inbox_media a 
					    where a.inbox_id='$id_inbox' ";
	$resultMedia = mysqli_query($mysql_connection, $queryMedia);
	
	$inbox_media	   = array();
	$n = 0;
	
	$media_list = array();
	while ($dataMedia = mysqli_fetch_assoc($resultMedia)) {
		$media['url'] = $backend_url."/".$inbox_file_folder."/".$dataMedia['file_name'];
		$media['file_type'] = $dataMedia['file_type'];
		array_push($media_list, $media);
		$firstMedia = $media['url'];
	}

	//to do bring all attachment
	$data['inbox_media_list']= '';
	//$complain_list[$i] = $data;
	//$i++;
		
	$api_response['status'] 		= 'success';
	$api_response['date'] 			= $data['date'];
	$api_response['title'] 			= $data['title'];
	$api_response['type'] 			= $data['type'];
	$api_response['content'] 		= $data['content'];
	$api_response['media_list']		= $data['media_list'];
	$api_response['first_media']	= $firstMedia;
	$api_response['content_html'] 	= $content_html;
	
	echo json_encode($api_response);
	exit;
?>