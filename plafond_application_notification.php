<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$ticket_number	= sanitize_sql_string(trim($_POST['ticket_number']));
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$queryCheck = "select * from plafond_applications where ticket_number='$ticket_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid ticket number';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data				= mysqli_fetch_assoc($resultCheck);
	$application_date	= $data['application_date'];
	$id_customer		= $data['customer_id'];
	
	$queryUpdate = "update plafond_applications set status='$status' where ticket_number='$ticket_number'";
	$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	
	
	//==================== INBOX ====================
	$inbox_message = 'Konsumen yang terhormat, \r\n';
	if($status == 'Approve') $inbox_message .= 'Pengajuan plafond Anda sudah disetujui. Silahkan cek nilai plafond Anda di menu profile.';
	else if($status == 'Reject') $inbox_message .= 'Mohon maaf, pengajuan plafond Anda belum dapat kami setujui. Terima kasih';
	
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $application_date;
	$content['status'] 		 	 = $status;
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//update tabel inbox
	$now			= date('Y-m-d H:i:s');
	$queryCheck 	= "select id from inbox where content like '%$ticket_number%' ";
	$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck		= mysqli_fetch_array($resultCheck);
	$id_inbox		= $dataCheck['id'];
	$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
	mysqli_query($mysql_connection, $queryInsert);
	
	/*$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'plafond applicaton', 'Pengajuan Plafond', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);*/
	
	//===============================================
	
	// TO-DO : Kirim push notification
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Plafond Application updated successfully';
	
	echo json_encode($api_response);
	exit;
?>