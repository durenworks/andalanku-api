<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$rqUuid =  sanitize_sql_string($_REQUEST['rq_uuid']);
	$rqDatetime =  sanitize_sql_string($_REQUEST['rq_datetime']);
	$receiverId =  sanitize_sql_string($_REQUEST['receiver_id']);
	$password =  sanitize_sql_string($_REQUEST['password']);
	$data =  $_REQUEST['data'];
	$commCode =  sanitize_sql_string($_REQUEST['comm_code']);
	$settlementFee =  sanitize_sql_string($_REQUEST['settlement_fee']);
	$bankRef =  sanitize_sql_string($_REQUEST['bank_ref']);
	$amount =  sanitize_sql_string($_REQUEST['amount']);
	$countTrx =  sanitize_sql_string($_REQUEST['count_trx']);
	$signature =  sanitize_sql_string($_REQUEST['signature']);

	$dataObj = json_decode($data);

	$str_log_content .= 'rq_uuid:'.$rqUuid.' \r\n';
	$str_log_content .= 'rq_datetime:'.$rqDatetime.' \r\n';
	$str_log_content .= 'receiver_id:'.$receiverId.' \r\n';
	$str_log_content .= 'password:'.$rqUpassworduid.' \r\n';
	$str_log_content .= 'data:'.$data.' \r\n';
	$str_log_content .= 'comm_code:'.$commCode.' \r\n';
	$str_log_content .= 'settlement_fee:'.$settlementFee.' \r\n';
	$str_log_content .= 'bank_ref:'.$bankRef.' \r\n';
	$str_log_content .= 'amount:'.$amount.' \r\n';
	$str_log_content .= 'count_trx:'.$countTrx.' \r\n';
	$str_log_content .= 'signature:'.$signature.' \r\n';

	$log_request_date = date('Y-m-d H:i:s');
	$log_request_type = 'Espay Settlement';

	include "inc-write-log.php";

	$settlementFee == '' ? $amount = 0 : $settlementFee;
	$amout == '' ? $amount = 0 : $amount;
	$countTrx == '' ? $countTrx = 0 : $countTrx;

	$queryInsert = "INSERT INTO espay_settlement (
			rq_uuid,
			rq_datetime,
			receiver_id,
			password,
			comm_code,
			settlement_fee,
			bank_ref,
			amount,
			count_trx,
			signature
		)
		VALUES (
			'$rqUuid',
			'$rqDatetime',
			'$receiverId',
			'$password',
			'$commCode',
			$settlementFee,
			'$bankRef',
			$amount,
			$countTrx,
			'$signature'
			)";
	mysqli_query($mysql_connection, $queryInsert);

	foreach($dataObj as $data) {
		$txId = $data->tx_id;
		$paymentId = $data->payment_id;
		$settlementAmount = $data->settlement_amount;
		$settlementDate = $data->settlement_date;
		$settlementRemark = $data->settlement_remark;
		$queryInsertdata = "INSERT INTO espay_settlement_data (
			tx_id,
			rq_uuid,
			payment_id,
			settlement_amount,
			settlement_date,
			settlement_remark
		)
		VALUES (
			'$txId',
			'$rqUuid',
			'$paymentId',
			$settlementAmount,
			'$settlementDate',
			'$settlementRemark'
			)";

			//var_dump($queryInsertdata);die();
		mysqli_query($mysql_connection, $queryInsertdata);
		
		$queryCheck = "SELECT id, amount FROM espay_payment
						WHERE payment_ref = '$txId'";

		$result= mysqli_query($mysql_connection, $queryCheck);
		$data = mysqli_fetch_array($result);

		if(mysqli_num_rows($result) == 0) {
			$str_log_content = $txId;
			$log_request_date = date('Y-m-d H:i:s');
			$log_request_type = 'Espay Settlement, Invalid Payment Ref';

			include "inc-write-log.php";
			//echo $txId." failed, invalid payment ref\n";
		}
		else {
			if ($data['amount'] != $settlementAmount) {
				$str_log_content = $txId;
				$log_request_date = date('Y-m-d H:i:s');
				$log_request_type = 'Espay Settlement, Invalid Amount';

				include "inc-write-log.php";

				$queryUpdate = "UPDATE espay_payment SET settlement = -1
								WHERE payment_ref = '$txId'";
				mysqli_query($mysql_connection, $queryUpdate);
				//echo $txId. " failed, invalid amount\n";
			}
			else {
				$queryUpdate = "UPDATE espay_payment SET settlement = true
								WHERE payment_ref = '$txId'";
				mysqli_query($mysql_connection, $queryUpdate);
				//echo $txId. " success\n";
			}
		}
	}

	echo "0, Success, " . $log_request_date;

?>