<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select *
			   from occupations
 			   where is_active='1' 
			   order by name ASC";
	$result = mysqli_query($mysql_connection, $query);
	
	$occupation_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$occupation_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 			= 'success';
	$api_response['occupation_list'] 	= $occupation_list;
	
	echo json_encode($api_response);
	exit;
?>