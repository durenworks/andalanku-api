<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function generateRandomString($length = 12) {

		$characters 		= '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	= strlen($characters);
		$randomString 		= '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
	
	function getMonthName($month) {
		
		$monthName = '';
		
		if($month=='1' || $month=='01') $monthName = 'Januari';
		if($month=='2' || $month=='02') $monthName = 'Februari';
		if($month=='3' || $month=='03') $monthName = 'Maret';
		if($month=='4' || $month=='04') $monthName = 'April';
		if($month=='5' || $month=='05') $monthName = 'Mei';
		if($month=='6' || $month=='06') $monthName = 'Juni';
		if($month=='7' || $month=='07') $monthName = 'Juli';
		if($month=='8' || $month=='08') $monthName = 'Agustus';
		if($month=='9' || $month=='09') $monthName = 'September';
		if($month=='10') $monthName = 'Oktober';
		if($month=='11') $monthName = 'November';
		if($month=='12') $monthName = 'Desember';
		
		return $monthName;
	}
	
	$id_customer	= sanitize_int($_POST['id_customer']);
	$contract_no	= sanitize_sql_string(trim($_POST['contract_no']));
	
	if($id_customer=='0' || $contract_no=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$inquiry_date	= date("Y-m-d H:i:s");
	$trx_date		= time();
	$product_code	= 'ANDKU01';
	$merchant_code	= '22691';
	$application_key= $afis_application_key;
	
	$year = date("y");
	$month = date("m");
	$day = date("d");
	
	//cek jumlah transaksi hari ini
	$query = "select count(id) as total 
				  from transaction_installment
				  where date(inquiry_date) = CURDATE()";
	$result= mysqli_query($mysql_connection, $query);
	$data = mysqli_fetch_array($result);
	$i = $data['total'] + 1;
	if ($i >= 100) {
		$iterator = $i;
	}
	elseif($i >= 10) {
		$iterator = '0'.$i;
	}
	else {
		$iterator = '00'.$i;
	}

	$transaction_code = 'INS'.$year.$month.$day.$iterator;

	$query = "select asset_description
				  from agreement_list
				  where agreement_number = '$contract_no'";
	$result= mysqli_query($mysql_connection, $query);
	$data = mysqli_fetch_array($result);

	//var_dump($afis_api_url);
	//var_dump(($data['asset_description'])); die();

	if($data['asset_description'] == null) {
		$base_url_afis = $afis_api_url;
		
		$afis_api_url  = $base_url_afis.'/Agreement/detailUnit/'.$contract_no;
		$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
		$responseArray = $afis_response->Response->Data; 

		$asset_description 	= $responseArray[0]->Description;
		$manufacturing_year = $responseArray[0]->ManufacturingYear;
		$colour 			= $responseArray[0]->Colour;
		$license_plate 		= $responseArray[0]->LicensePlate;
		$chasis_no 			= $responseArray[0]->ChasisNo;
		$engine_no		 	= $responseArray[0]->EngineNo;

		//var_dump($afis_api_url);
		//var_dump(($data['asset_description'])); die();

		$query = "update agreement_list set asset_description = '$asset_description'
				  where agreement_number = '$contract_no'";

		mysqli_query($mysql_connection, $query);		
	}
	else {
		$asset_description = $data['asset_description'];
	}

	$token	= hash('sha256','%'.$contract_no.
							'%'.$police_number.
							'%'.$transaction_code.
							'%'.$trx_date.
							'%'.$product_code.
							'%'.$merchant_code.
							'%'.$application_key.'%'); 
	$token	= strtoupper($token);
	
	$base_url_afis = $afis_api_url;
	$base_url = $afis_payment_api_url;

	$afis_api_url	= $base_url.'/api/Biller/inquiry';
	$body_params	= array();
	
	$body_params['contract_no']		= $contract_no;
	$body_params['police_number']	= '';
	$body_params['reff_id']			= $transaction_code;
	$body_params['trx_date']		= $trx_date;
	$body_params['product_code']	= $product_code;
	$body_params['merchant_code']	= $merchant_code;
	$body_params['token']			= $token;
	
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_params, true));
	
	//print_r($afis_response); exit;
	
	//========================== DATA DUMMY ==========================
	/*$afis_response->Response_Code 				= 'SUCCESSFUL';
	$afis_response->data->police_number			= '777888999';
	$afis_response->data->customer				= 'John Doe WR';
	$afis_response->data->installment_number	= '111222333';
	$afis_response->data->tenor				 	= '12';
	$afis_response->data->description			= 'Test Description';
	$afis_response->data->amount				= '499000';
	$afis_response->data->admin_fee				= '1000';
	$afis_response->data->penalty				= '35000';
	$afis_response->data->total_amount			= '535000';
	$afis_response->data->due_date				= '2020-07-31';
	$afis_response->data->periode				= '4';*/	
	//================================================================
	
	if($afis_response->Response_Code == '200') {
		
		$responseData = $afis_response->Data; 
		
		$police_number      = $responseData->Police_Number;
		$customer       	= $responseData->Customer;
		$installment_number	= $responseData->Installment_Number;
		$tenor             	= $responseData->Tenor;
		$description       	= $responseData->Description;
		$amount            	= $responseData->Amount;
		$admin_fee         	= $responseData->Admin_Fee;
		$penalty           	= $responseData->Penalty;
		$total_amount      	= $responseData->Total_Amount;
		$due_date          	= $responseData->Due_Date;
		$periode           	= $responseData->Periode;
		$info_text         	= $responseData->Info_Text;
		
		$newPoliceNumber = '';
		$prevCharType	 = '';
		$currentCharType = '';
		
		for ($i = 0; $i < strlen($police_number); $i++){
		   
			$char = $police_number[$i];
			if (is_numeric($char)) {
				$currentCharType = 'number';
			}
			else {
				$currentCharType = 'letter';
			}
		   
			if($currentCharType <> $prevCharType && $prevCharType <> '') {
				$char = ' '.$char;
			}
			
			$newPoliceNumber = $newPoliceNumber.$char;
			$prevCharType = $currentCharType;
		}
		
		$police_number = $newPoliceNumber;
		
		$tempArray = explode('-', $periode);
		$periode   = getMonthName($tempArray[0]).' '.$tempArray[1];
		
		$tempArray 	= explode('-',$due_date);
		$due_date_db= $tempArray[2].'-'.$tempArray[1].'-'.$tempArray[0];
		
		//$payment_expire = date('Y-m-d H:i:s',strtotime('+23 hour +45 minutes',strtotime($inquiry_date)));
		$payment_expire = date("Y-m-d")." 23:45:00";
		
		//simpan data dari afis 
		$query = "insert into transaction_installment(id_customer, transaction_code, 
					inquiry_date, contract_no, police_number, 
					installment_number, tenor, description, 
					amount, admin_fee, penalty, 
					total_amount, due_date, periode, 
					payment_status, payment_expire)
					values('$id_customer','$transaction_code',
					'$inquiry_date','$contract_no','$police_number',
					'$installment_number','$tenor','$description',
					'$amount','$admin_fee','$penalty',
					'$total_amount','$due_date_db','$periode',
					'INQUIRY', '$payment_expire')";
		mysqli_query($mysql_connection, $query);		
					
		$api_response['status'] 			= 'success';
		$api_response['message'] 			= 'Payment inquiry successful';
		$api_response['transaction_code'] 	= $transaction_code;
		$api_response['contract_no'] 		= $contract_no;
		$api_response['police_number'] 		= $police_number;
		$api_response['asset_description']	= $asset_description;
		$api_response['customer'] 			= $customer;
		$api_response['installment_number'] = $installment_number;
		$api_response['tenor'] 				= $tenor;
		$api_response['description'] 		= $description;
		$api_response['amount'] 			= round($amount,0);
		$api_response['admin_fee'] 			= round($admin_fee, 0);
		$api_response['penalty'] 			= round($penalty, 0);
		$api_response['total_amount'] 		= round($total_amount, 0);
		$api_response['due_date'] 			= $due_date;
		$api_response['periode'] 			= $periode;
		$api_response['Info_text'] 			= $info_text;
		$api_response['payment_url']		= $api_url.'/carspay?transaction_code='.$transaction_code;
	}
	else {
		
		$api_response['status'] 			= 'failed';
		$api_response['message'] 			= $afis_response->Response_Desc;
		$api_response['transaction_code'] 	= '';
		$api_response['contract_no'] 		= '';
		$api_response['police_number'] 		= '';
		$api_response['asset_description']	= '';
		$api_response['customer'] 			= '';
		$api_response['installment_number'] = '';
		$api_response['tenor'] 				= '';
		$api_response['description'] 		= '';
		$api_response['amount'] 			= '';
		$api_response['admin_fee'] 			= '';
		$api_response['penalty'] 			= '';
		$api_response['total_amount'] 		= '';
		$api_response['due_date'] 			= '';
		$api_response['periode'] 			= '';
		$api_response['Info_text'] 			= '';
		$api_response['payment_url']		= '';
	}
	
	
	echo json_encode($api_response);
	exit;
?>