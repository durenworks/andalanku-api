<?php
	ini_set("display_errors","0");
	error_reporting(0);

	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$branch_list = array();

	$str_request = sanitize_sql_string($_REQUEST["id_city"]);
	
	if($latitude<>'' && $longitude<>'') {
		
		$R = 3960;  // earth's mean radius
		$rad = '100';
		// first-cut bounding box (in degrees)
		$maxLat = $latitude + rad2deg($rad/$R);
		$minLat = $latitude - rad2deg($rad/$R);
		// compensate for degrees longitude getting smaller with increasing latitude
		$maxLon = $longitude + rad2deg($rad/$R/cos(deg2rad($latitude)));
		$minLon = $longitude - rad2deg($rad/$R/cos(deg2rad($latitude)));

		$maxLat=number_format((float)$maxLat, 6, '.', '');
		$minLat=number_format((float)$minLat, 6, '.', '');
		$maxLon=number_format((float)$maxLon, 6, '.', '');
		$minLon=number_format((float)$minLon, 6, '.', '');
		
		$result = array();
		
		$queryBranch = "SELECT * FROM branches 
						where longitude is not null or latitude is not null
						ORDER BY (POW((longitude-($longitude)),2) + POW((latitude-($latitude)),2))"; 
		$resultBranch= mysqli_query($mysql_connection, $queryBranch);
		
		while($dataBranch = mysqli_fetch_assoc($resultBranch)) {
			
			$result[] = $dataBranch;
		}
		
	}
	else {
		
		$id_city = 0;
		$str_city = '';
		//var_dump($str_request);
		if (is_numeric($str_request)) {
			$id_city = $str_request;
		}
		else {
			$str_city = str_replace('%20', ' ', $str_request);
		}

		//var_dump($str_city);die();
		$afis_api_url  = $afis_api_url.'/Andalanku/BranchList';
		$afis_response = json_decode(afis_call($afis_api_url, 'GET'));

		$responseArray = $afis_response->Response->Data;

		$i = 0;
		foreach($responseArray as $key=>$value) {
			$branch_list[$i] =  $value;
			$i++;

		}

		$queryCity 		= "select * from regencies where id = '$id_city' or name = '$str_city'";
		$resultCity		= mysqli_query($mysql_connection, $queryCity);
		$dataCity		= mysqli_fetch_array($resultCity);
		$cityName		= $dataCity['name'];
		$cityName 		= str_replace('KOTA ', '', $cityName);
		$id_province  	= $dataCity['province_id'];


		$queryProvince 	= "select * from provinces where id = '$id_province'";
		$resultProvince	= mysqli_query($mysql_connection, $queryProvince);
		$dataProvince	= mysqli_fetch_array($resultProvince);
		$provinceName	= $dataProvince['name'];

		//var_dump($provinceName);

		$branchInCity = array();
		$branchInProvince = array();

		//var_dump($responseArray);die();
		foreach($responseArray as $key=>$value) {
			//echo ($value->City . ' vs ' .$cityName . ' | ');
			if ($value->City === $cityName) {
				$branchInCity[] = $value;
			}

			//echo ($value->Provinsi . ' vs ' .$provinceName . ' | ');
			if ($value->Provinsi === $provinceName) {
				$branchInProvince[] = $value;
			}
		}

		$result =[];
		if (!empty($branchInCity)) { 
			$result = $branchInCity;
		}
		elseif (!empty($branchInProvince)) {
			$result = $branchInProvince;
		}
		else { 
			$result = $responseArray;
		}
	}

	$api_response['status'] 	 = 'success';
	$api_response['branch_list'] = $result;

	echo json_encode($api_response);
	exit;
?>
