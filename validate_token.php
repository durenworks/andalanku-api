<?php
	
	$api_key 		= '';
	$token_number	= '';
	
	$api_response = array();

	foreach (getallheaders() as $name => $value) {
		
		if($name == 'api_key') $api_key = sanitize_sql_string($value);
		if($name == 'token_number') $token_number = sanitize_sql_string($value);
		
		if($name == 'id_customer') $id_customer = sanitize_int($value);
		if($name == 'latitude') $latitude = sanitize_sql_string($value);
		if($name == 'longitude') $longitude = sanitize_sql_string($value);
		
		if($name == 'os_name') $os_name = sanitize_sql_string($value);
		if($name == 'software_version') $software_version = sanitize_sql_string($value);
	}
	
	$now = date("Y-m-d H:i:s");
	//$queryCheck = "select id_token from tokens where api_key='$api_key' and token_number='$token_number' and token_expired>'$now'";
	$queryCheck = "select id_token from tokens where api_key='$api_key' and token_number='$token_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	/*if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid API Key or Token Number';
		
		echo json_encode($api_response);
		exit;
	}*/
	
	//sekalian hapus token yang sudah expired
	//$queryDelete = "delete from tokens where token_expired<'$now'";
	//mysqli_query($mysql_connection, $queryDelete);
	
	if($id_customer<>'0' && $latitude<>'' && $longitude<>'') {
		
		$queryUpdate = "update customers set last_latitude='$latitude', last_longitude='$longitude'  
					    where id_customer='$id_customer'";
		$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	}
	
	if($id_customer<>'0') {
		
		$now = date('Y-m-d H:i:s');
		
		$queryUpdate = "update customers set last_activity='$now', os_name='$os_name', software_version='$software_version' 
					    where id_customer='$id_customer'";
		$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	}
?>