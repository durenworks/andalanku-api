<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	
	$query 	= "select *
			   from partners
			   order by RAND()";
	$result = mysqli_query($mysql_connection, $query);
	
	$partner_list = array();	
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$data['image'] = $backend_url."/".$partner_image_folder."/".$data['image'];
		$partner_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['partner_list'] 	= $partner_list;
	
	echo json_encode($api_response);
	exit;
?>