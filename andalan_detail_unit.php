<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
    include "afis_call.php";

	$agreement_no 	= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($agreement_no=='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}

    $afis_api_url  = $afis_api_url.'/Agreement/detailUnit/'.$agreement_no;
    $afis_response = json_decode(afis_call($afis_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data;

    $asset_description 	= $responseArray[0]->Description;
    $manufacturing_year = $responseArray[0]->ManufacturingYear;
    $colour 			= $responseArray[0]->Colour;
    $license_plate 		= $responseArray[0]->LicensePlate;
    $chasis_no 			= $responseArray[0]->ChasisNo;
    $engine_no		 	= $responseArray[0]->EngineNo;

	$api_response['status'] 				= 'success';
	$api_response['asset_description'] 		= $asset_description;
	$api_response['manufacturing_year'] 	= $manufacturing_year;
	$api_response['colour'] 				= $colour == null ? '-' : $colour;
	$api_response['license_plate'] 			= $license_plate;
	$api_response['chasis_no'] 				= $chasis_no;
	$api_response['engine_no'] 				= $engine_no;
	
	echo json_encode($api_response);
	exit;
?>