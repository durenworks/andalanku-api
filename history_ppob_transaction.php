<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$customer_id = sanitize_int($_REQUEST["customer_id"]);
	$page 		 = sanitize_int($_REQUEST["page"]);
	
	if($page == '0') $page = '1';
	
	$query 			= "select COUNT(a.id) as num
					   from ppob_transaction 
					   where customer_id='$customer_id' ";					   
	$result 		= mysqli_query($mysql_connection, $query);
	$data 			= mysqli_fetch_assoc($result);
	$total_pages 	= $data[num];

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;
	
	$query 	= "select * 
			   from ppob_transaction 
			   where customer_id='$customer_id' 
			   order by id DESC LIMIT $start,$limit";
	$result = mysqli_query($mysql_connection, $query);
	
	$ppob_history_list = array();
	
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {		
		$ppob_history_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 			= 'success';
	$api_response['ppob_history_list'] 	= $ppob_history_list;
	
	echo json_encode($api_response);
	exit;
?>