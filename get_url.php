<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";

	$api_response['status'] = 'success';
	$api_response['web_url']= $web_url;
	$api_response['api_url']= $api_url;
	$api_response['cms_url']= $backend_url;
	
	echo json_encode($api_response);
	exit;
?>