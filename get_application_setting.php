<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$setting_key = sanitize_sql_string(trim($_REQUEST["setting_key"]));
	$setting_key = strtoupper($setting_key);
	
	$query 	= "select * from settings where UPPER(setting_key)='$setting_key' ";
	$result = mysqli_query($mysql_connection, $query);
	
	if(mysqli_num_rows($result) == 0) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Setting key not found';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data = mysqli_fetch_assoc($result);
		
	$api_response['status'] 		= 'success';
	$api_response['setting_value'] 	= $data['setting_value'];
	
	echo json_encode($api_response);
	exit;
?>