<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_customer 	= sanitize_int($_POST['id_customer']);
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$query 	= "select a.*, b.customer_name 
			   from collateral_requests a 
			   left join customers b on a.customer_id=b.id_customer 
 			   where a.customer_id='$id_customer' ";
	if($status == '') $query = $query." order by request_date DESC ";
	else if($status == 'SUBMITTED') $query = $query." and a.status='SUBMITTED' order by request_date DESC ";
	else if($status == 'ON PROCESS') $query = $query." and a.status='ON PROCESS' order by request_date DESC ";
	else if($status == 'SOLVED') $query = $query." and a.status='DONE' order by request_date DESC ";
	
	$result = mysqli_query($mysql_connection, $query); 
	
	$insurance_claim_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
	
		$insurance_claim_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 				= 'success';
	$api_response['insurance_claim_list'] 	= $insurance_claim_list;
	
	echo json_encode($api_response);
	exit;
?>