<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer			= sanitize_int($_REQUEST["id_customer"]);
	$old_password			= sanitize_sql_string(trim($_REQUEST["old_password"]));
	$new_password			= sanitize_sql_string(trim($_REQUEST["new_password"]));
	$confirm_new_password	= sanitize_sql_string(trim($_REQUEST["confirm_new_password"]));
	
	if ($id_customer <> '0' and $old_password <> '' and $new_password <> '' and $confirm_new_password <> '') {
		
		$old_password = md5($old_password);
		$queryCheck = "select id_customer from customers where id_customer='$id_customer' and password='$old_password'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid old password';
			
			echo json_encode($api_response);
			exit;
		}

		if($new_password <> $confirm_new_password) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Password and confirmation password not match';
			
			echo json_encode($api_response);
			exit;
		}

		if(strlen($new_password) < 8) {
		
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid password';
			
			echo json_encode($api_response);
			exit;
		}
		
		$new_password = md5($new_password);
		
		$queryUpdate  = "update customers set password='$new_password' where id_customer='$id_customer'";
		$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
		
		$api_response['status'] 	= 'success';
		$api_response['message'] 	= 'Password saved successfully';
		
		echo json_encode($api_response);
		exit;

	} else {

		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}	
?>