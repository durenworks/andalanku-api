<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	//include "validate_token.php";
	
	function roundUpToNearestMultiple($n, $increment = 1000)
	{
		return (int) ($increment * ceil($n / $increment));
	}
	
	function PMT($rate, $nper, $pv, $fv, $type) {
		
		$PMT = (-$fv - $pv * pow(1 + $rate, $nper)) / (1 + $rate * $type) / ((pow(1 + $rate, $nper) - 1) / $rate);
		
		return $PMT;
	}
	
	$type   		= sanitize_sql_string(trim($_POST['type']));		//(NEW, USED, KMG)
	$otr	 		= sanitize_int(trim($_POST['otr']));				//(harga mobil dalam rupiah)
	$dp				= sanitize_int(trim($_POST['dp']));					//(jumlah dp dalam persen)
	$tenor			= sanitize_sql_string(trim($_POST['tenor']));		//(jumlah tenor dalam bulan)
	$asuransi		= sanitize_sql_string(trim($_POST['asuransi']));	//(TLO, ARK)
	$wilayah		= sanitize_int(trim($_POST['wilayah']));			//(1, 2, 3)
    $car_price      = sanitize_int(trim($_POST['car_price']));          //(harga mobil, KMG)

    if ($dp > 100) {
        $dp = $dp / 100;
    }

    if($asuransi == '1') {
        $asuransi = 'TLO';
    }
    if ($asuransi == '2') {
        $asuransi = 'ARK';
    }
	
	if($type=='' || $otr=='0' || $tenor=='0' || $asuransi=='' || $wilayah=='0') {

		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//dp kurang dari 20%


	if($type == 'NEW' || $type == 'USED') {
		
		if($dp < 20) {
			
			$api_response['status'] 		= 'false';
			$api_response['message'] 		= 'Minimum DP is 20% from OTR';
			$api_response['dp'] 			= '0';
			$api_response['tenor'] 			= '0';
			$api_response['pokok_hutang']	= '0';
			$api_response['angsuran'] 		= '0';
			$api_response['tipe_asuransi'] 	= ' ';
			$api_response['rate_asuransi']	= '0';
			$api_response['premi_asuransi']= '0';
			$api_response['provisi']		= '0';
			$api_response['biaya_admin']	= '0';
			$api_response['bunga_flat'] 	= '0';
			$api_response['total_hutang']	= '0';
			
			echo json_encode($api_response);
			exit;
		}
	}
	
	$pokok_hutang 	= $otr - ($otr * ($dp/100));
	$tenor_tahun 	= ceil($tenor/12);

    $collateral = $otr;

    if ($type == 'KMG') {
        $collateral = $car_price;
    }

    $columnName = 'wilayah_'.$wilayah;
    $queryRate	= "select ".$columnName." from insurance_rates 
                               where asuransi='$asuransi' and otr_range_min<='$collateral' and otr_range_max>='$collateral'";
    $resultRate = mysqli_query($mysql_connection, $queryRate);
    $dataRate	= mysqli_fetch_array($resultRate);
    $rate_asuransi = $dataRate[$columnName];

    $depresiasi = array();
    $depresiasi[1] = '100';
    $depresiasi[2] = '90';
    $depresiasi[3] = '85';
    $depresiasi[4] = '75';
    $depresiasi[5] = '65';

    if ($tenor_tahun < 1) {
        $premi_asuransi = ($rate_asuransi/100) * ($collateral);
    }
    else {
        for ($i = 1; $i <= $tenor_tahun; $i++) {
            $premi_asuransi += ($rate_asuransi/100) * ($collateral * ($depresiasi[$i]/100));
        }
    }

    $premi_asuransi += ($tenor_tahun * 100000);

	if($type == 'NEW') {
		
		$queryRate	= "select rate_new, flat_rate_new,admin from car_rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);

		$flat_rate_new  = $dataRate['flat_rate_new'];
		$rate_new = $dataRate['rate_new'];
		$biaya_admin	= $dataRate['admin'];


		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $flat_rate_new/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($flat_rate_new/100) * $tenor) / 12;
		$biaya_admin = $biaya_admin + 450000;

        $pmt_arrear = PMT(($rate_new/100)/12,$tenor,(-$premi_asuransi-$pokok_hutang),0,0);
        $pmt_advance = PMT(($rate_new/100)/12,$tenor,(-$premi_asuransi-$pokok_hutang),0,1);

        //var_dump(PMT(($rate_new/100)/12,$tenor,(-$premi_asuransi-$pokok_hutang),0,0));

	}
	else if($type == 'USED') {
		
		$queryRate	= "select rate_used, flat_rate_used,admin from car_rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);
		
		$rate_used	= $dataRate['rate_used']; 
		
		$pmt = PMT(($rate_used/100)/12,$tenor,1,0,0)*-1;
		$flat_rate_used = ((($pmt)*$tenor)-1)*12/$tenor; 
		$flat_rate_used = $flat_rate_used * 100;
		
		$biaya_admin	= $dataRate['admin'];
		
		if($rate_used == 0) {
			
			$api_response['status'] 		= 'false';
			$api_response['message'] 		= 'Used car rates is 0';
			$api_response['dp'] 			= '0';
			$api_response['tenor'] 			= '0';
			$api_response['pokok_hutang']	= '0';
			$api_response['angsuran'] 		= '0';
			$api_response['tipe_asuransi'] 	= ' ';
			$api_response['rate_asuransi']	= '0';
			$api_response['premi_asuransi'] = '0';
			$api_response['provisi']		= '0';
			$api_response['biaya_admin']	= '0';
			$api_response['bunga_flat'] 	= '0';
			$api_response['total_hutang']	= '0';
			
			echo json_encode($api_response);
			exit;
		}
		
		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $flat_rate_used/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($flat_rate_used/100) * $tenor) / 12;

        $pmt_arrear = PMT(($rate_used/100)/12,$tenor,(-$premi_asuransi-$pokok_hutang),0,0);
        $pmt_advance = PMT(($rate_used/100)/12,$tenor,(-$premi_asuransi-$pokok_hutang),0,1);
		
	}		
	else if($type == 'KMG') {
		
		$queryRate	= "select rate,admin from rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);
		
		$rate  			= $dataRate['rate'];
		$biaya_admin	= $dataRate['admin'];
		
		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $rate/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($rate/100) * $tenor) / 12;
        $pmt_advance = PMT(($rate/100)/12,$tenor,(-$pokok_hutang),0,1);
        $pmt_arrear = PMT(($rate/100)/12,$tenor,(-$pokok_hutang),0,0);

	}

	$provisi = $pokok_hutang * (2/100);	

	//$total_hutang = $angsuran * $tenor;
	
	$dp_rupiah = $otr * ($dp/100);
	
	//=================================================================================================================
	
	$dataArray					= array();
	
	$tempArray					= array();
	$tempArray['credit_type'] 	= 'ARREAR';
	$tempArray['dp'] 			= number_format($dp_rupiah,0,',','.');
	$tempArray['tenor'] 		= $tenor;
	$tempArray['pokok_hutang']	= number_format($pokok_hutang,0,',','.');
    $tempArray['angsuran'] 		= number_format(roundUpToNearestMultiple($pmt_arrear, 1000), 0,
                            ',', '.');
	$tempArray['tipe_asuransi'] = $asuransi;
	$tempArray['rate_asuransi']	= $rate_asuransi.'%';
	$tempArray['premi_asuransi']= number_format($premi_asuransi,0,',','.');
	$tempArray['provisi']		= number_format($provisi,0,',','.');
	$tempArray['biaya_admin']	= number_format($biaya_admin,0,',','.');
	$tempArray['bunga_flat'] 	= number_format($bunga_flat,0,',','.');
	$tempArray['total_hutang']	= number_format($pmt_arrear * $tenor,0,',','.');
    $tempArray['dana_diterima']	= number_format(roundUpToNearestMultiple($pokok_hutang - $provisi - $biaya_admin -
                                    $premi_asuransi, 1000),
                            0,',','.');
	$dataArray[0]				= $tempArray;
	
	$tempArray					= array();
	$tempArray['credit_type'] 	= 'ADVANCE';
	$tempArray['dp'] 			= number_format($dp_rupiah,0,',','.');
	$tempArray['tenor'] 		= $tenor;
	$tempArray['pokok_hutang']	= number_format($pokok_hutang,0,',','.');
	//$tempArray['angsuran'] 		= number_format($angsuran,0,',','.');
    $tempArray['angsuran'] 		= number_format(roundUpToNearestMultiple($pmt_advance, 1000), '0',
                                    ',', '.');
	$tempArray['tipe_asuransi'] = $asuransi;
	$tempArray['rate_asuransi']	= $rate_asuransi.'%';
	$tempArray['premi_asuransi']= number_format($premi_asuransi,0,',','.');
	$tempArray['provisi']		= number_format($provisi,0,',','.');
	$tempArray['biaya_admin']	= number_format($biaya_admin,0,',','.');
	$tempArray['bunga_flat'] 	= number_format($bunga_flat,0,',','.');
	$tempArray['total_hutang']	= number_format($pmt_advance * $tenor,0,',','.');
    $tempArray['dana_diterima']	= number_format(roundUpToNearestMultiple($pokok_hutang - $provisi - $biaya_admin -
                                    $premi_asuransi - $pmt_advance, 1000),
                            0,',','.');
	$dataArray[1]				= $tempArray;
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Simulationcomplete';
	$api_response['simulation_data']= $dataArray;
	
	echo json_encode($api_response);
	exit;
?>