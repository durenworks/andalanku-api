<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	$verification_code	= sanitize_sql_string(trim($_REQUEST["verification_code"]));
	
	if ($verification_code <> '') {

		$queryCheck = "select id_customer from customers where verification_code='$verification_code'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck) == 0) {
			
			$api_response['status'] 		= 'failed';
			$api_response['message'] 		= 'Invalid email verification code';
			
			echo json_encode($api_response);
			exit;
		}
		else {

			$dataCheck	  = mysqli_fetch_array($resultCheck);
			$id_customer  = $dataCheck['id_customer'];
		
			$queryUpdate  = "update customers set email_verification='VERIFIED', verification_code='' 
							 where id_customer='$id_customer' ";
			$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
			
			$api_response['status'] 		= 'success';
			$api_response['message'] 		= 'Email verification successful';
			
			echo json_encode($api_response);
			exit;
		}
	} else {

		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Invalid email verification code';
		
		echo json_encode($api_response);
		exit;
	}
?>