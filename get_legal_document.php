<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select type, content from legal_documents where is_published='1' ";
	$result = mysqli_query($mysql_connection, $query);
	
	$legal_document_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$legal_document_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 				= 'success';
	$api_response['legal_document_list'] 	= $legal_document_list;
	
	echo json_encode($api_response);
	exit;
?>