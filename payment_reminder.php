<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	
	$h_plus_3 = date('Y-m-d', strtotime("+3 day"));
	$h_plus_7 = date('Y-m-d', strtotime("+7 day"));
	
	$query = "SELECT a.*, b.fcm_token  
			  FROM agreement_list a 
			  left join customers b on a.customer_id=b.id_customer 
			  WHERE contract_status='LIV' 
			  AND (jatuh_tempo_berikutnya='$h_plus_3' or jatuh_tempo_berikutnya='$h_plus_7')";
	$result= mysqli_query($mysql_connection, $query);
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$id_customer	= $data['customer_id'];
		$plat_nomor 	= $data['plat_nomor'];
		$angsuran		= number_format($data['angsuran'],0,',','.');
		$tgl_jatuh_tempo= date('d-m-Y', strtotime($data['jatuh_tempo_berikutnya']));
		$now			= date('Y-m-d H:i:s');
		$fcm_token	= $data['fcm_token'];
		
		//==================== INBOX ====================
		$content				= array();
		$inbox_message			= 'Bpk/Ibu Yth, \r\n\r\n';
		$inbox_message		   .= 'Angsuran '.$plat_nomor.' sebesar Rp. '.$angsuran.' akan jatuh tempo. ';
		$inbox_message		   .= 'Mohon untuk melakukan pembayaran sebelum tgl '.$tgl_jatuh_tempo.'. Abaikan jika sudah membayar.';
		$content['message']		= $inbox_message;
		$content = json_encode($content); echo $inbox_message."<br>";
		
		//insert ke tabel inbox
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'installment payment', 'Pembayaran Angsuran', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================
		
		//==================== FCM PUSH NOTIF ============
		if($fcm_token<>'') {
			
			$json_data = [
				"to" => $fcm_token,
				"notification" => [
					"body" => $inbox_message,
					"title" => 'Pembayaran Angsuran',
					"icon" => "http://devapi.andalanku.id/img/ic_notif.png"
				]
			];
			$data = json_encode($json_data);
			//FCM API end-point
			$url = 'https://fcm.googleapis.com/fcm/send';
			//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
			$server_key = 'AAAAS-tTObY:APA91bEMdHLHqYlKKwbS0B8GN2cy84hJn-PqsPV-Xsr4_SoKFjp96ccTw9BwNBsY48Gz176_Dot409_pGVhtSO9FjNTOKSJLltLvAiaaXRTYmmh21shmOIoXIEAH4j1z6vybmGU6K-is';
			//header with content_type api key
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key='.$server_key
			);
			//CURL request to route notification to FCM connection server (provided by Google)
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			$result = curl_exec($ch);
			//echo $result;
			if ($result === FALSE) {
				echo 'empty';
				die('Oops! FCM Send Error: ' . curl_error($ch));
			}
			curl_close($ch);
		}
		//================================================
	}
?>