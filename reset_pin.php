<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer 	= sanitize_int($_POST['id_customer']);
	$email 			= sanitize_sql_string($_POST['email']);
	
	$queryCheck = "select id_customer from customers where id_customer='$id_customer' and email='$email'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Customer not found or invalid email address';
		
		echo json_encode($api_response);
		exit;
	}
	
	$new_pin_number = rand(000000,999999); 
	
	$enc_new_pin_number = md5($new_pin_number);
	
	$queryUpdate  = "update customers set pin='$enc_new_pin_number' where id_customer='$id_customer'";
	$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	
	//------- TO-DO : Kirim email reset PIN -------
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Change PIN successful';
	$api_response['new_pin_number'] = $new_pin_number;
	
	echo json_encode($api_response);
	exit;
?>