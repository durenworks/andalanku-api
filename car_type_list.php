<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_car_brand = sanitize_int($_REQUEST["id_car_brand"]);

	$query 	= "select * from car_types where id_car_brand='$id_car_brand' and is_active='1' order by name ASC";
	$result = mysqli_query($mysql_connection, $query);
	
	$car_type_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
	
		$id_car_type = $data['id_car_type'];
		
		//cari harga termurah untuk tipe ini
		$queryPrice  = "select price from car_trims where id_car_type='$id_car_type' order by price ASC LIMIT 1";
		$resultPrice = mysqli_query($mysql_connection, $queryPrice);
		$dataPrice	 = mysqli_fetch_array($resultPrice);
		$lowestPrice = $dataPrice['price'];
		
		if(strlen($lowestPrice) >= 7) {
			$priceMillion = substr($lowestPrice, 0, strlen($lowestPrice)-6);
			$lowestPrice  = $priceMillion." juta";
		}
		
		$data['lowestPrice'] = $lowestPrice;
		
		//ambil gambar tipe mobil
		$queryMedia = "select b.url 
					   FROM car_type_media a 
					   Left JOIN media b on a.id_media=b.id_media 
					   WHERE a.id_car_type='$id_car_type'";
		$resultMedia= mysqli_query($mysql_connection, $queryMedia);
		
		$car_type_image = array();
		$n = 0;
		
		while($dataMedia = mysqli_fetch_array($resultMedia)) {
			
			$car_type_image[$n] = $backend_url."/".$media_image_folder."/".$dataMedia['url'];
			$n++;
		}
		
		$data['car_type_image'] = $car_type_image;
		
		$car_type_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['car_type_list']	= $car_type_list;
	
	echo json_encode($api_response);
	exit;
?>