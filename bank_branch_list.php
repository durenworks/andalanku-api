<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$bank_id = sanitize_sql_string($_REQUEST["bank_id"]);
	$keyword = sanitize_sql_string($_REQUEST["keyword"]);
	
	$query 	= "select bank_branch_id, bank_code, bank_branch_name, city 
			   from bank_branch 
			   where bank_id='$bank_id' and (bank_branch_name like '%$keyword%')
			   order by bank_branch_name ASC ";
	$result = mysqli_query($mysql_connection, $query); 
	
	$bank_branch_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) { 
		$bank_branch_list[$i] = $data;
		$i++;
	} 
	
	$api_response['status'] 			= 'success';
	$api_response['bank_branch_list'] 	= $bank_branch_list;
	
	echo json_encode($api_response);
	exit;
?>