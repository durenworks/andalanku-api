<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$year		= sanitize_int($_REQUEST["year"]);
	$keyword	= sanitize_sql_string($_REQUEST["keyword"]);	
	$region		= '901';
			
	$afis_api_url  = $afis_api_url.'/AndalanKu/SimulasiKredit/'.$year.'/'.$region.'/'.$keyword;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$car_price_list= array();
	
	$i = 0;
	foreach($responseArray as $key=>$value) {
		
		$car_price_list[$i] = $value;
		$i++;	
	}		
	
	$api_response['status'] 			= 'success';
	$api_response['car_price_list'] 	= $car_price_list;
	
	echo json_encode($api_response);
	exit;
?>