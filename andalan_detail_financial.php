<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
    include "afis_call.php";

	$agreement_no 	= sanitize_sql_string(trim($_POST['agreement_no']));

	if($agreement_no=='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}

    $afis_api_url  = $afis_api_url.'/Agreement/detailFinancial/'.$agreement_no;
    $afis_response = json_decode(afis_call($afis_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data;

    $otr 					= $responseArray[0]->OTR;
    $down_payment 			= $responseArray[0]->DownPayment;
    $insurance_capitalized 	= $responseArray[0]->InsAssetCapitalized;
    $net_finance 			= $responseArray[0]->NetFinance;
    $agreement_date 		= $responseArray[0]->AgreementDate;
    $effective_date		 	= $responseArray[0]->EffectiveDate;
    $maturity_date 			= $responseArray[0]->MaturityDate;
    $tenor 					= $responseArray[0]->Tenor;
    $installment_amount		= $responseArray[0]->InstallmentAmount;

	$api_response['status'] 				= 'success';
	$api_response['otr'] 					= $otr;
	$api_response['down_payment'] 			= $down_payment;
	$api_response['insurance_capitalized'] 	= $insurance_capitalized;
	$api_response['net_finance'] 			= $net_finance;
	$api_response['agreement_date'] 		= $agreement_date;
	$api_response['effective_date'] 		= $effective_date;
	$api_response['maturity_date'] 			= $maturity_date;
	$api_response['tenor'] 					= $tenor;
	$api_response['installment_amount'] 	= $installment_amount;
	
	echo json_encode($api_response);
	exit;
?>