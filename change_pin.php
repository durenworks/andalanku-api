<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer 		= sanitize_int($_POST['id_customer']);
	$old_pin_number 	= sanitize_sql_string($_POST['old_pin_number']);
	$new_pin_number 	= sanitize_sql_string($_POST['new_pin_number']);
	
	$queryCheck = "select id_customer from customers where id_customer='$id_customer' and (pin='$old_pin_number' or pin is null)";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Incorrect old pin number';
		
		echo json_encode($api_response);
		exit;
	}
	
	if(strlen($new_pin_number) <> 6) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid PIN. PIN number length must be 6 characters';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryUpdate  = "update customers set pin='$new_pin_number' where id_customer='$id_customer'";
	$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	
	$api_response['status'] 	= 'success';
	$api_response['message'] 	= 'Change PIN successful';
	
	echo json_encode($api_response);
	exit;
?>