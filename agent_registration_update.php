<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	
	$id_number	= sanitize_sql_string(trim($_POST['id_number']));
	$agent_code	= sanitize_sql_string(trim($_POST['agent_code']));
	
	if($id_number == '' || $agent_code	== '') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
		
	$queryCheck = "select id_register_agent, id_customer  
				   from register_agent_history 
				   where id_card_number='$id_number' and status='PENDING'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'ID number not found';
		
		echo json_encode($api_response);
		exit;
	}
	
	$dataCheck = mysqli_fetch_array($resultCheck);
	$id_customer = $dataCheck['id_customer'];
	
	$query = "update register_agent_history set status='APPROVED' where id_card_number='$id_number' ";
	mysqli_query($mysql_connection, $query);
	
	//=================================================================================================================
	
	$query = "update customers set agent_code='$agent_code' where id_number='$id_number' ";
	mysqli_query($mysql_connection, $query);
	
	//==================== INBOX ====================
	$now = date("Y-m-d");
	$content					 = array();
	$content['input_date'] 		 = $now;
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, pendaftaran agen telah kami proses. ';
	$inbox_message				 .= 'Berikut adalah kode agen anda : '.$agent_code;
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'agent registration', 'Proses Pendaftaran Agen', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Agent registration updated successfully';
	
	echo json_encode($api_response);
	exit;
?>