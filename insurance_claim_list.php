<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_customer 	= sanitize_int($_POST['id_customer']);
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$query 	= "select a.*, b.customer_name 
			   from insurance_claims a 
			   left join customers b on a.customer_id=b.id_customer 
 			   where a.customer_id='$id_customer' ";
	if($status == '') $query = $query." order by claim_date DESC ";
	else if($status == 'SUBMITTED') $query = $query." and a.status='SUBMITTED' order by claim_date DESC ";
	else if($status == 'ON PROCESS') $query = $query." and a.status='ON PROCESS' order by claim_date DESC ";
	else if($status == 'SOLVED') $query = $query." and a.status='ON HANDLE' order by claim_date DESC ";
	
	$result = mysqli_query($mysql_connection, $query); 
	
	$insurance_claim_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {

        $loopIDClaim = $data['id'];

        //ambil media
        $queryMedia  = "select url 
					    from insurance_claim_media a 
					    left join media b on a.id_media=b.id_media 
					    where a.id_claim='$loopIDClaim' ";
        $resultMedia = mysqli_query($mysql_connection, $queryMedia);

        $media_list	   = array();
        $n = 0;

        while ($dataMedia = mysqli_fetch_assoc($resultMedia)) {
            $media_list[$n] = $backend_url."/".$media_image_folder."/".$dataMedia['url'];
            $n++;
        }

        $data['media_list']= $media_list;

		$insurance_claim_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 				= 'success';
	$api_response['insurance_claim_list'] 	= $insurance_claim_list;
	
	echo json_encode($api_response);
	exit;
?>