<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	//include "afis_call.php";
	
	function base64_to_image($backend_folder, $image_folder, $base64_string) {
				
		$extension = 'png';
		$folder = $backend_folder."/".$image_folder;
		
		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;
		
		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = ''; 
		}

		fclose( $ifp ); 

		return $newnamefile; 
	}
	
	$id_customer 	= sanitize_int($_POST['id_customer']);
	
	if($id_customer=='0') {
			
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}	
	
	$customer_name			= sanitize_sql_string(trim($_POST['customer_name']));
	$gender					= sanitize_sql_string(trim($_POST['gender']));
	$birth_place			= sanitize_sql_string(trim($_POST['birth_place']));
	$birth_date				= sanitize_sql_string(trim($_POST['birth_date']));
	$id_card_number			= sanitize_sql_string(trim($_POST['id_card_number']));
	$nip					= sanitize_sql_string(trim($_POST['nip']));
	$npwp					= sanitize_sql_string(trim($_POST['npwp']));
	$avatar					= sanitize_sql_string(trim($_POST['avatar']));
	$bank_name				= sanitize_sql_string(trim($_POST['bank_name']));
	$bank_account_number	= sanitize_sql_string(trim($_POST['bank_account_number']));
	$bank_account_holder	= sanitize_sql_string(trim($_POST['bank_account_holder']));
	$bank_account_image		= sanitize_sql_string(trim($_POST['bank_account_image']));
	$area_code				= sanitize_sql_string(trim($_POST['area_code']));
	$phone					= sanitize_sql_string(trim($_POST['phone']));
	$mobile_phone			= sanitize_sql_string(trim($_POST['mobile_phone']));
	$id_card_image			= sanitize_sql_string(trim($_POST['id_card_image']));
	$npwp_image				= sanitize_sql_string(trim($_POST['npwp_image']));
	
	//---ALAMAT
	$province_id		= sanitize_int($_POST['province_id']);
	if ($province_id == 0) {
		$province_id = sanitize_sql_string($_POST['province_id']);
		$query = "select id from provinces where name='$province_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id = $data['id'];
	}
	
	$regency_id			= sanitize_int($_POST['regency_id']);
	if ($regency_id == 0) {
		$regency_id = sanitize_sql_string($_POST['regency_id']);
		$query = "select id from regencies where name='$regency_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$regency_id = $data['id'];
	}

	$district_id		= sanitize_int($_POST['district_id']);
	if ($district_id == 0) {
		$district_id = sanitize_sql_string($_POST['district_id']);
		$query = "select id from districts where name='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id = $data['id'];
	}
	
	$village_id			= sanitize_sql_string(trim($_POST['village_id']));
	if ($village_id == 0) {
		$village_id = sanitize_sql_string($_POST['village_id']);
		$query = "select id from villages where name='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id = $data['id'];
	}
	
	$zip_code		= sanitize_sql_string(trim($_POST['zip_code']));
	$address		= sanitize_sql_string(trim($_POST['address']));
	$rt 			= sanitize_sql_string(trim($_POST['rt']));
	$rw				= sanitize_sql_string(trim($_POST['rw']));
	
	//======================================================================================================================
	
	$province_id_domicile		= sanitize_int($_POST['province_id_domicile']);
	if ($province_id_domicile == 0) {
		$province_id_domicile = sanitize_sql_string($_POST['province_id_domicile']);
		$query = "select id from provinces where name='$province_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id_domicile = $data['id'];
	}
	
	$regency_id_domicile		= sanitize_int($_POST['regency_id_domicile']);
	if ($regency_id_domicile == 0) {
		$regency_id_domicile = sanitize_sql_string($_POST['regency_id_domicile']);
		$query = "select id from regencies where name='$regency_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$regency_id_domicile = $data['id'];
	}

	$district_id_domicile		= sanitize_int($_POST['district_id_domicile']);
	if ($district_id_domicile == 0) {
		$district_id_domicile = sanitize_sql_string($_POST['district_id_domicile']);
		$query = "select id from districts where name='$district_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id_domicile = $data['id'];
	}
	
	$village_id_domicile			= sanitize_sql_string(trim($_POST['village_id_domicile']));
	if ($village_id_domicile == 0) {
		$village_id_domicile = sanitize_sql_string($_POST['village_id_domicile']);
		$query = "select id from villages where name='$village_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id_domicile = $data['id'];
	}
	
	$zip_code_domicile		= sanitize_sql_string(trim($_POST['zip_code_domicile']));
	$address_domicile		= sanitize_sql_string(trim($_POST['address_domicile']));
	$rt_domicile 			= sanitize_sql_string(trim($_POST['rt_domicile']));
	$rw_domicile			= sanitize_sql_string(trim($_POST['rw_domicile']));
	
	//======================================================================================================================
	
	$province_id_office		= sanitize_int($_POST['province_id_office']);
	if ($province_id_office == 0) {
		$province_id_office = sanitize_sql_string($_POST['province_id_office']);
		$query = "select id from provinces where name='$province_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id_office = $data['id'];
	}
	
	$regency_id_office		= sanitize_int($_POST['regency_id_office']);
	if ($regency_id_office == 0) {
		$regency_id_office = sanitize_sql_string($_POST['regency_id_office']);
		$query = "select id from regencies where name='$regency_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$regency_id_office = $data['id'];
	}

	$district_id_office		= sanitize_int($_POST['district_id_office']);
	if ($district_id_office == 0) {
		$district_id_office = sanitize_sql_string($_POST['district_id_office']);
		$query = "select id from districts where name='$district_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id_office = $data['id'];
	}
	
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	if ($village_id_office == 0) {
		$village_id_office = sanitize_sql_string($_POST['village_id_office']);
		$query = "select id from villages where name='$village_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id_office = $data['id'];
	}
	
	$zip_code_office	= sanitize_sql_string(trim($_POST['zip_code_office']));
	$address_office		= sanitize_sql_string(trim($_POST['address_office']));
	$rt_office 			= sanitize_sql_string(trim($_POST['rt_office']));
	$rw_office			= sanitize_sql_string(trim($_POST['rw_office']));
	
	//======================================================================================================================
	
	if($avatar <> '') $avatarFileName = base64_to_image($backend_folder, $customer_document_image_folder, $avatar); 
	else $avatarFileName = '';
	
	$query = "select andalan_customer_id, avatar 
			  from customers 
			  where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id = $data['andalan_customer_id'];
	$old_avatar = $data['avatar'];
	
	//jika belum punya andalan_customer_id data tidak perlu lengkap
	/*if($andalan_customer_id=='' || $andalan_customer_id=='0' || $andalan_customer_id==null) {
	
		if($province_id=='0' || $regency_id=='0' || $district_id=='0' || $village_id=='0' ||
		   $zip_code=='' || $address=='' || $rt=='' || $rw=='' ) {
			
			$api_response['status']		= 'failed';
			$api_response['message'] 	= 'input not complete';
			
			echo json_encode($api_response);
			exit;
		}
	}*/
	
	//=================================================================================================================
	
	// jika belum punya andalan_customer_id maka tidak perlu approval 
	if($andalan_customer_id=='' || $andalan_customer_id=='0') {
		
		$imageFileName = '';
		if($bank_account_image <> '') { $imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $bank_account_image); }
				
		$query = "update customers set 
					customer_name		= '$customer_name',	
					gender				= '$gender',
					place_of_birth		= '$birth_place',
					date_of_birth		= '$birth_date',
					id_number			= '$id_card_number',
					nip					= '$nip',
					npwp				= '$npwp',			
					bank_name			= '$bank_name',
					bank_account_number	= '$bank_account_number',
					bank_account_holder	= '$bank_account_holder',				
					home_phone_number	= '$phone',		
					area_code			= '$area_code', 
					phone_number 		= '$mobile_phone' ";	
		
		if($nip <> '') $query = $query." , customer_type='EMPLOYEE' ";
		
		if($imageFileName <> '') $query = $query." , bank_account_image	= '$imageFileName' ";
		
		if($avatarFileName <> '') {
			$query = $query." , avatar='$avatarFileName' ";
			
			if($old_avatar <> '') {
				@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$old_avatar);
			}
		}
		
		$query = $query." where id_customer='$id_customer'";
		mysqli_query($mysql_connection, $query); 
		
		//===== LEGAL =====
		if($address <> '') {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id', '$regency_id', 
							'$district_id', '$village_id', '$zip_code', 
							'$address', '$rt', '$rw', 'LEGAL')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address' and address_type='LEGAL' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//update tabel address_customers
			$queryUpdate = "update address_customers set is_active='0' 
							where address_type='LEGAL' and user_id='$id_customer'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			//insert ke tabel address_customers
			$queryInsert = "insert into address_customers(address_type, user_id, 
							address_id, is_active) 
							values('LEGAL', '$id_customer', 
							'$id_address', '1')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.*,
									 b.address, b.rt, b.rw, b.zip_code,
									 b.village_id, b.district_id, b.regency_id, b.province_id,
									 c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
									 from address_customers a
									 left join address b on a.address_id=b.id
									 left join provinces c on b.province_id=c.id
									 left join regencies d on b.regency_id=d.id
									 left join districts e on b.district_id=e.id
									 left join villages f on b.village_id=f.id
									 where a.user_id='$id_customer'
									 and a.address_type='LEGAL'
									 and a.is_active='1' ";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressLegal  = mysqli_fetch_array($resultAddress);
			
			if ($dataAddressLegal) {
				$api_response['address_legal'] 			= $dataAddressLegal['address'];
				$api_response['rt_legal'] 				= $dataAddressLegal['rt'];
				$api_response['rw_legal'] 				= $dataAddressLegal['rw'];
				$api_response['village_id_legal'] 		= $dataAddressLegal['village_id'];
				$api_response['village_name_legal'] 	= $dataAddressLegal['village_name'];
				$api_response['district_id_legal'] 		= $dataAddressLegal['district_id'];
				$api_response['district_name_legal'] 	= $dataAddressLegal['district_name'];
				$api_response['city_id_legal'] 			= $dataAddressLegal['regency_id'];
				$api_response['city_name_legal'] 		= $dataAddressLegal['city_name'];
				$api_response['province_id_legal'] 		= $dataAddressLegal['province_id'];
				$api_response['province_name_legal'] 	= $dataAddressLegal['province_name'];
				$api_response['zip_code_legal'] 		= $dataAddressLegal['zip_code'];
			}
		}
		
		//===== DOMICILE =====
		if($address_domicile <> '') {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id_domicile', '$regency_id_domicile', 
							'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile', 
							'$address_domicile', '$rt_domicile', '$rw_domicile', 'DOMICILE')";
			mysqli_query($mysql_connection, $queryInsert); 
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_domicile' and address_type='DOMICILE' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//update tabel address_customers
			$queryUpdate = "update address_customers set is_active='0' 
							where address_type='DOMICILE' and user_id='$id_customer'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			//insert ke tabel address_customers
			$queryInsert = "insert into address_customers(address_type, user_id, 
							address_id, is_active) 
							values('DOMICILE', '$id_customer', 
							'$id_address', '1')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.*,
								 b.address, b.rt, b.rw, b.zip_code,
								 b.village_id, b.district_id, b.regency_id, b.province_id,
								 c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
								 from address_customers a
								 left join address b on a.address_id=b.id
								 left join provinces c on b.province_id=c.id
								 left join regencies d on b.regency_id=d.id
								 left join districts e on b.district_id=e.id
								 left join villages f on b.village_id=f.id
								 where a.user_id='$id_customer'
								 and a.address_type='DOMICILE'
								 and a.is_active='1' ";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressDomicile  = mysqli_fetch_array($resultAddress);

			if ($dataAddressDomicile) {
				$api_response['address_domicile'] 			= $dataAddressDomicile['address'];
				$api_response['rt_domicile'] 				= $dataAddressDomicile['rt'];
				$api_response['rw_domicile'] 				= $dataAddressDomicile['rw'];
				$api_response['village_id_domicile'] 		= $dataAddressDomicile['village_id'];
				$api_response['village_name_domicile'] 		= $dataAddressDomicile['village_name'];
				$api_response['district_id_domicile'] 		= $dataAddressDomicile['district_id'];
				$api_response['district_name_domicile'] 	= $dataAddressDomicile['district_name'];
				$api_response['city_id_domicile'] 			= $dataAddressDomicile['regency_id'];
				$api_response['city_name_domicile'] 		= $dataAddressDomicile['city_name'];
				$api_response['province_id_domicile'] 		= $dataAddressDomicile['province_id'];
				$api_response['province_name_domicile'] 	= $dataAddressDomicile['province_name'];
				$api_response['zip_code_domicile'] 			= $dataAddressDomicile['zip_code'];
			}
		}
		
		//===== OFFICE =====
		if($address_office <> '') {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id_office', '$regency_id_office', 
							'$district_id_office', '$village_id_office', '$zip_code_office', 
							'$address_office', '$rt_office', '$rw_office', 'OFFICE')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_office' and address_type='OFFICE' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//update tabel address_customers
			$queryUpdate = "update address_customers set is_active='0' 
							where address_type='OFFICE' and user_id='$id_customer'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			//insert ke tabel address_customers
			$queryInsert = "insert into address_customers(address_type, user_id, 
							address_id, is_active) 
							values('OFFICE', '$id_customer', 
							'$id_address', '1')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.*,
								 b.address, b.rt, b.rw, b.zip_code,
								 b.village_id, b.district_id, b.regency_id, b.province_id,
								 c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name
								 from address_customers a
								 left join address b on a.address_id=b.id
								 left join provinces c on b.province_id=c.id
								 left join regencies d on b.regency_id=d.id
								 left join districts e on b.district_id=e.id
								 left join villages f on b.village_id=f.id
								 where a.user_id='$id_customer'
								 and a.address_type='OFFICE'
								 and a.is_active='1' ";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressOffice  = mysqli_fetch_array($resultAddress);

			if ($dataAddressOffice) {
				$api_response['address_office'] 		= $dataAddressOffice['address'];
				$api_response['rt_office'] 				= $dataAddressOffice['rt'];
				$api_response['rw_office'] 				= $dataAddressOffice['rw'];
				$api_response['village_id_office'] 		= $dataAddressOffice['village_id'];
				$api_response['village_name_office'] 	= $dataAddressOffice['village_name'];
				$api_response['district_id_office'] 	= $dataAddressOffice['district_id'];
				$api_response['district_name_office'] 	= $dataAddressOffice['district_name'];
				$api_response['city_id_office'] 		= $dataAddressOffice['regency_id'];
				$api_response['city_name_office'] 		= $dataAddressOffice['city_name'];
				$api_response['province_id_office'] 	= $dataAddressOffice['province_id'];
				$api_response['province_name_office'] 	= $dataAddressOffice['province_name'];
				$api_response['zip_code_office'] 		= $dataAddressOffice['zip_code'];
			}
		}
		
		//upload gambar id card
		if($id_card_image <> '') {
			
			$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $id_card_image); 
			if($imageFileName <> '') {
				
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO KTP'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO KTP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		//upload gambar npwp
		if($npwp_image <> '') {
			
			$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $npwp_image); 
			if($imageFileName <> '') {
				
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO NPWP'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO NPWP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}			
		
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Profile update request successful';
		$api_response['ticket_number'] 	= '';
		
	} 
	else if($andalan_customer_id<>'' && $andalan_customer_id<>'0') {
		
		$imageFileName = '';
		if($bank_account_image <> '') { $imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $bank_account_image); }
				
		$query = "update customers set 
					nip					= '$nip',
					npwp				= '$npwp',			
					bank_name			= '$bank_name',
					bank_account_number	= '$bank_account_number',
					bank_account_holder	= '$bank_account_holder' ";	
		
		if($nip <> '') $query = $query." , customer_type='EMPLOYEE' ";
		
		if($imageFileName <> '') $query = $query." , bank_account_image	= '$imageFileName' ";
		
		if($avatarFileName <> '') {
			$query = $query." , avatar='$avatarFileName' ";
			
			if($old_avatar <> '') {
				@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$old_avatar);
			}
		}
		
		$query = $query." where id_customer='$id_customer'";
		mysqli_query($mysql_connection, $query); 
		
		$ticket_number = 'UP'.rand(000000000, 999999999);
	
		$ticket_number_exist = true;
		
		while($ticket_number_exist) {
			
			$query = "select id from profile_update_request where ticket_number='$ticket_number'";
			$result= mysqli_query($mysql_connection, $query);
			if(mysqli_num_rows($result) == 0) {
				$ticket_number_exist = false;
			}
			else {
				$ticket_number = 'UP'.rand(000000000, 999999999);
			}
		}
		
		$now = date("Y-m-d H:i:s");
		
		//insert ke tabel profile_update_request
		$queryInsert = "insert into profile_update_request(ticket_number, user_id, 
						request_date, area_code, phone, mobile_phone, id_card_image, 
						status) 
						values('$ticket_number', '$id_customer', 
						'$now', '$area_code', '$phone', '$mobile_phone', '$imageFileName', 
						'PROCESS')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//ambil id yang terakhir
		$query = "select id from profile_update_request where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_profile_update_request = $data['id'];
		
		//===== LEGAL =====
		$updateLegal = false;
		if($address <> '') {
			
			$updateLegal = true;
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id', '$regency_id', 
							'$district_id', '$village_id', '$zip_code', 
							'$address', '$rt', '$rw', 'LEGAL')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address' and address_type='LEGAL' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//insert ke tabel profile_update_request_address
			$queryInsert = "insert into profile_update_request_address(address_type, profile_update_request_id, address_id) 
							values('LEGAL', '$id_profile_update_request', '$id_address')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.address, a.rt, a.rw, a.zip_code,
								 a.village_id, a.district_id, a.regency_id, a.province_id,
								 b.name as province_name, c.name as city_name, d.name as district_name, e.name as village_name
								 from address a 
								 left join provinces b on a.province_id=b.id
								 left join regencies c on a.regency_id=c.id
								 left join districts d on a.district_id=d.id
								 left join villages e on a.village_id=e.id
								 where a.id='$id_address'
								 and a.address_type='LEGAL'";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressLegal  = mysqli_fetch_array($resultAddress);

			if ($dataAddressLegal) {
				$api_response['address_legal'] 			= $dataAddressLegal['address'];
				$api_response['rt_legal'] 				= $dataAddressLegal['rt'];
				$api_response['rw_legal'] 				= $dataAddressLegal['rw'];
				$api_response['village_id_legal'] 		= $dataAddressLegal['village_id'];
				$api_response['village_name_legal'] 	= $dataAddressLegal['village_name'];
				$api_response['district_id_legal'] 		= $dataAddressLegal['district_id'];
				$api_response['district_name_legal'] 	= $dataAddressLegal['district_name'];
				$api_response['city_id_legal'] 			= $dataAddressLegal['regency_id'];
				$api_response['city_name_legal'] 		= $dataAddressLegal['city_name'];
				$api_response['province_id_legal'] 		= $dataAddressLegal['province_id'];
				$api_response['province_name_legal'] 	= $dataAddressLegal['province_name'];
				$api_response['zip_code_legal'] 		= $dataAddressLegal['zip_code'];
			}
		}
		
		//===== DOMICILE =====
		$updateDomicile = false;
		if($address_domicile <> '') {
			
			$updateDomicile = true;
		
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id_domicile', '$regency_id_domicile', 
							'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile', 
							'$address_domicile', '$rt_domicile', '$rw_domicile', 'DOMICILE')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_domicile' and address_type='DOMICILE' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//insert ke tabel profile_update_request_address
			$queryInsert = "insert into profile_update_request_address(address_type, profile_update_request_id, address_id) 
							values('DOMICILE', '$id_profile_update_request', '$id_address')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.address, a.rt, a.rw, a.zip_code,
								 a.village_id, a.district_id, a.regency_id, a.province_id,
								 b.name as province_name, c.name as city_name, d.name as district_name, e.name as village_name
								 from address a 
								 left join provinces b on a.province_id=b.id
								 left join regencies c on a.regency_id=c.id
								 left join districts d on a.district_id=d.id
								 left join villages e on a.village_id=e.id
								 where a.id='$id_address'
								 and a.address_type='DOMICILE'";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressDomicile  = mysqli_fetch_array($resultAddress);

			if ($dataAddressDomicile) {
				$api_response['address_domicile'] 			= $dataAddressDomicile['address'];
				$api_response['rt_domicile'] 				= $dataAddressDomicile['rt'];
				$api_response['rw_domicile'] 				= $dataAddressDomicile['rw'];
				$api_response['village_id_domicile'] 		= $dataAddressDomicile['village_id'];
				$api_response['village_name_domicile'] 		= $dataAddressDomicile['village_name'];
				$api_response['district_id_domicile'] 		= $dataAddressDomicile['district_id'];
				$api_response['district_name_domicile'] 	= $dataAddressDomicile['district_name'];
				$api_response['city_id_domicile'] 			= $dataAddressDomicile['regency_id'];
				$api_response['city_name_domicile'] 		= $dataAddressDomicile['city_name'];
				$api_response['province_id_domicile'] 		= $dataAddressDomicile['province_id'];
				$api_response['province_name_domicile'] 	= $dataAddressDomicile['province_name'];
				$api_response['zip_code_domicile'] 			= $dataAddressDomicile['zip_code'];
			}
		}
		
		//===== OFFICE =====
		$updateOffice = false;
		if($address_office <> '') {
			
			$updateOffice = true;
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id, 
							district_id, village_id, zip_code, 
							address, rt, rw, address_type) 
							values('$province_id_office', '$regency_id_office', 
							'$district_id_office', '$village_id_office', '$zip_code_office', 
							'$address_office', '$rt_office', '$rw_office', 'OFFICE')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id from address where address='$address_office' and address_type='OFFICE' order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];
			
			//insert ke tabel profile_update_request_address
			$queryInsert = "insert into profile_update_request_address(address_type, profile_update_request_id, address_id) 
							values('OFFICE', '$id_profile_update_request', '$id_address')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//===== UNTUK KEPERLUAN RESPON
			$queryAddress = "select a.address, a.rt, a.rw, a.zip_code,
								 a.village_id, a.district_id, a.regency_id, a.province_id,
								 b.name as province_name, c.name as city_name, d.name as district_name, e.name as village_name
								 from address a 
								 left join provinces b on a.province_id=b.id
								 left join regencies c on a.regency_id=c.id
								 left join districts d on a.district_id=d.id
								 left join villages e on a.village_id=e.id
								 where a.id='$id_address'
								 and a.address_type='DOMICILE'";
			$resultAddress= mysqli_query($mysql_connection, $queryAddress);
			$dataAddressOffice  = mysqli_fetch_array($resultAddress);

			if ($dataAddressOffice) {
				$api_response['address_office'] 		= $dataAddressOffice['address'];
				$api_response['rt_office'] 				= $dataAddressOffice['rt'];
				$api_response['rw_office'] 				= $dataAddressOffice['rw'];
				$api_response['village_id_office'] 		= $dataAddressOffice['village_id'];
				$api_response['village_name_office'] 	= $dataAddressOffice['village_name'];
				$api_response['district_id_office'] 	= $dataAddressOffice['district_id'];
				$api_response['district_name_office'] 	= $dataAddressOffice['district_name'];
				$api_response['city_id_office'] 		= $dataAddressOffice['regency_id'];
				$api_response['city_name_office'] 		= $dataAddressOffice['city_name'];
				$api_response['province_id_office'] 	= $dataAddressOffice['province_id'];
				$api_response['province_name_office'] 	= $dataAddressOffice['province_name'];
				$api_response['zip_code_office'] 		= $dataAddressOffice['zip_code'];
			}
		}
		
		//upload gambar id card
		if($id_card_image <> '') {
			
			$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $id_card_image); 
			if($imageFileName <> '') {
				
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO KTP'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO KTP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		//upload gambar npwp
		if($npwp_image <> '') {
			
			$imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $npwp_image); 
			if($imageFileName <> '') {
				
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO NPWP'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO NPWP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}			
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $ticket_number;
		$content['input_date'] 		 = $now;
		$content['status'] 		 	 = 'SUBMITTED';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. ';
		$inbox_message				 .= 'Kami telah menerima permohonan pembaharuan data diri Anda dan akan segera memproses permohonan Anda. ';
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'update profile', 'Update Profile', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================
		
		//==================================== AFIS CALL ====================================
		/* NOTE : Data dikirim ke AFIS ketika admin melakukan approval di cms
		$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$andalan_customer_id = $data['andalan_customer_id'];
		
		//===================
		
		$query = "select name from villages where id='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_name = $data['name'];
		
		$query = "select name from villages where id='$village_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_name_domicile = $data['name'];
		
		$query = "select name from villages where id='$village_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_name_office = $data['name'];
		
		//===================
		
		$query = "select name from districts where id='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_name = $data['name'];
		
		$query = "select name from districts where id='$district_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_name_domicile = $data['name'];
		
		$query = "select name from districts where id='$district_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_name_office = $data['name'];
		
		//===================
		
		$query = "select name from regencies where id='$regency_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_name = $data['name'];
		
		$query = "select name from regencies where id='$regency_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_name_domicile = $data['name'];
		
		$query = "select name from regencies where id='$regency_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_name_office = $data['name'];
		
		$afis_api_url	= $afis_api_url.'/Supplier/pembaharuan';
		
		//===================
		
		if($updateLegal) {
			
			$body_param					= array();
			$body_param['CustomerId']	= $andalan_customer_id;
			$body_param['AddressType']	= 'L';
			$body_param['Address']		= $address;
			$body_param['RT']			= $rt;
			$body_param['RW']			= $rw;
			$body_param['Kelurahan']	= $village_name;
			$body_param['Kecamatan']	= $district_name;
			$body_param['City']			= $city_name;
			$body_param['ZipCode']		= $zip_code;
			$body_param['AreaPhone']	= $area_code;
			$body_param['PhoneNo']		= $phone;
			$body_param['MobileNo']		= $mobile_phone;
			
			$jsonData = json_encode($body_param);
			$queryUpdate = "UPDATE profile_update_request SET log_legal = '$jsonData' WHERE ticket_number = '$ticket_number'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
			$api_response['afis_response'] 	= $afis_response;	
	
			if ($afis_response->StatusCode == 500) {
				
				$queryUpdate = "UPDATE profile_update_request SET status_legal = 0 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			} 
			else {
				$queryUpdate = "UPDATE profile_update_request SET status_legal = 1 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			}
		}
		//===================
		
		if($updateDomicile) {
			
			$body_param					= array();
			$body_param['CustomerId']	= $andalan_customer_id;
			$body_param['AddressType']	= 'D';
			$body_param['Address']		= $address_domicile;
			$body_param['RT']			= $rt_domicile;
			$body_param['RW']			= $rw_domicile;
			$body_param['Kelurahan']	= $village_name_domicile;
			$body_param['Kecamatan']	= $district_name_domicile;
			$body_param['City']			= $city_name_domicile;
			$body_param['ZipCode']		= $zip_code_domicile;
			$body_param['AreaPhone']	= $area_code;
			$body_param['PhoneNo']		= $phone;
			$body_param['MobileNo']		= $mobile_phone;
			
			$jsonData = json_encode($body_param);
			$queryUpdate = "UPDATE profile_update_request SET log_domicile = '$jsonData' WHERE ticket_number = '$ticket_number'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
			$api_response['afis_response'] 	= $afis_response;	
	
			if ($afis_response->StatusCode == 500) {
				
				$queryUpdate = "UPDATE profile_update_request SET status_domicile = 0 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			} 
			else {
				$queryUpdate = "UPDATE profile_update_request SET status_domicile = 1 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			}
		}
		//===================
		
		if($updateOffice) {
			
			$body_param					= array();
			$body_param['CustomerId']	= $andalan_customer_id;
			$body_param['AddressType']	= 'O';
			$body_param['Address']		= $address_office;
			$body_param['RT']			= $rt_office;
			$body_param['RW']			= $rw_office;
			$body_param['Kelurahan']	= $village_name_office;
			$body_param['Kecamatan']	= $district_name_office;
			$body_param['City']			= $city_name_office;
			$body_param['ZipCode']		= $zip_code_office;
			$body_param['AreaPhone']	= $area_code;
			$body_param['PhoneNo']		= $phone;
			$body_param['MobileNo']		= $mobile_phone;
			
			$jsonData = json_encode($body_param);
			$queryUpdate = "UPDATE profile_update_request SET log_office = '$jsonData' WHERE ticket_number = '$ticket_number'";
			mysqli_query($mysql_connection, $queryUpdate);
			
			$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
			$api_response['afis_response'] 	= $afis_response;	
	
			if ($afis_response->StatusCode == 500) {
				
				$queryUpdate = "UPDATE profile_update_request SET status_office = 0 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			} 
			else {
				$queryUpdate = "UPDATE profile_update_request SET status_office = 1 WHERE ticket_number = '$ticket_number'";
				mysqli_query($mysql_connection, $queryUpdate);
			}
		}*/
		//==================================== AFIS CALL ====================================
		
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Profile update request successful';
		$api_response['ticket_number'] 	= $ticket_number;
	}	
	
	echo json_encode($api_response);
	exit;
?>