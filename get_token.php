<?php
	ini_set("display_errors","1");
	error_reporting(1);
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	$api_key  	= sanitize_sql_string(trim($_POST['api_key']));
	$api_secret	= sanitize_sql_string(trim($_POST['api_secret']));
	
	$api_response = array();
	
	$queryCheck = "select id_api_key from api_keys where api_key='$api_key' and api_secret='$api_secret'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck); 
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid API Key or API Secret';
		
		echo json_encode($api_response);
		exit;
	}
	
	$token_number = md5($api_key." -x-x- ".$api_secret." -x-x- ".microtime());
	$token_expired= date("Y-m-d H:i:s", strtotime("+6 hours"));
	
	$queryInsert = "insert into tokens(api_key, token_number, token_expired) 
					values('$api_key', '$token_number', '$token_expired')";
	mysqli_query($mysql_connection, $queryInsert);
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Token number generated';
	$api_response['token_number'] 	= $token_number;
	
	echo json_encode($api_response);
	exit;
?>