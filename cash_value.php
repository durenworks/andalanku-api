<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$id_customer = sanitize_int($_REQUEST["id_customer"]);
	
	$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id = $data['andalan_customer_id'];
	
	$cash_value_list	= array();
	$total_cash_value	= 0;
	
	if($andalan_customer_id<>'' && $andalan_customer_id<>'0') {		
			
		$afis_api_url  = $afis_api_url.'/Agreement/CashValue/'.$andalan_customer_id;
		$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
		$responseArray = $afis_response->Response->Data;
		$total_cash_value	= $responseArray[0]->Total; 
		$cashValueArray		= $responseArray[0]->CashValues;
		
		$i = 0;
		foreach($cashValueArray as $key=>$value) {
			
			$cash_value_list[$i] = $value;
			$i++;	
		}		
	}
	
	$api_response['status'] 			= 'success';
	$api_response['cash_value_list'] 	= $cash_value_list;
	$api_response['total_cash_value'] 	= $total_cash_value;
	
	echo json_encode($api_response);
	exit;
?>