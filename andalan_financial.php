<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
    include "afis_call.php";
	
	function downloadFile($url, $path)
	{
		$newfname = $path;
		$file = fopen ($url, 'rb');
		$file_created = false;
		if ($file) {
			
			$file_created = true;
			
			$newf = fopen ($newfname, 'wb');
			if ($newf) {
				while(!feof($file)) {
					fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
				}
			}
		}
		if ($file) {
			fclose($file);
		}
		if ($newf) {
			fclose($newf);
		}
		
		return $file_created;
	}

	$id_customer	= sanitize_int(trim($_POST['id_customer']));
	$agreement_no 	= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($id_customer=='0' || $agreement_no=='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}

	//=================================== ANDALAN DETAIL UNIT =====================================
    $andalan_detail_unit = array();
	$tempArray			 = array();
	
	$temp_api_url  = $afis_api_url.'/Agreement/detailUnit/'.$agreement_no;
    $afis_response = json_decode(afis_call($temp_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data;

    $asset_description 	= $responseArray[0]->Description;
    $manufacturing_year = $responseArray[0]->ManufacturingYear;
    $colour 			= $responseArray[0]->Colour;
    $license_plate 		= $responseArray[0]->LicensePlate;
    $chasis_no 			= $responseArray[0]->ChasisNo;
    $engine_no		 	= $responseArray[0]->EngineNo;

	
	$tempArray['asset_description'] 	= $asset_description;
	$tempArray['manufacturing_year'] 	= $manufacturing_year;
	$tempArray['colour'] 				= $colour == null ? '-' : $colour;
	$tempArray['license_plate'] 		= $license_plate;
	$tempArray['chasis_no'] 			= $chasis_no;
	$tempArray['engine_no'] 			= $engine_no;
	$andalan_detail_unit[0]				= $tempArray;
	
	
	//=================================== ANDALAN DETAIL FINANCIAL ================================
    $andalan_detail_financial	= array();
	$tempArray			 		= array();
	
	$temp_api_url  = $afis_api_url.'/Agreement/detailFinancial/'.$agreement_no;
    $afis_response = json_decode(afis_call($temp_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data;

    $otr 					= $responseArray[0]->OTR;
    $down_payment 			= $responseArray[0]->DownPayment;
    $insurance_capitalized 	= $responseArray[0]->InsAssetCapitalized;
    $net_finance 			= $responseArray[0]->NetFinance;
    $agreement_date 		= $responseArray[0]->AgreementDate;
    $effective_date		 	= $responseArray[0]->EffectiveDate;
    $maturity_date 			= $responseArray[0]->MaturityDate;
    $tenor 					= $responseArray[0]->Tenor;
    $installment_amount		= $responseArray[0]->InstallmentAmount;
    $next_installment_due_date = $responseArray[0]->NextInstallmentDate;

	$tempArray['otr'] 					= $otr;
	$tempArray['down_payment'] 			= $down_payment;
	$tempArray['net_finance'] 			= $net_finance;
	$tempArray['agreement_date'] 		= $agreement_date;
	$tempArray['effective_date'] 		= $effective_date;
	$tempArray['maturity_date'] 		= $maturity_date;
	$tempArray['tenor'] 				= $tenor;
	$tempArray['installment_amount'] 	= $installment_amount;
    $tempArray['next_installment_due_date'] 	= $next_installment_due_date;

    //=================================== NILAI ASURANSI =========================================

    $temp_api_url  = $afis_api_url.'/AndalanKu/KlaimAsuransi/'.$agreement_no;
    $afis_response = json_decode(afis_call($temp_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data;
    $tempArray['insurance_capitalized'] = $responseArray[0]->MainPremiumToCust;

    //============================================================================================
    $andalan_detail_financial[0]		= $tempArray;
	
	//=================================== ANDALAN DETAIL PAYMENT HISTORY ==========================
    $andalan_detail_payment_history	= array();
	$tempArray			 			= array();
	
	$query = "select andalan_customer_id, customer_name from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id 		= $data['andalan_customer_id'];
	$folder_andalan_customer_id	= str_replace("/","",$andalan_customer_id);
	$customer_name				= $data['customer_name'];
	$customer_name				= str_replace(' ','-',$customer_name);
	
	$temp_api_url  = $afis_api_url.'/Agreement/detailFinancial/'.$agreement_no;
	$afis_response = json_decode(afis_call($temp_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$branchId 	   =  trim($responseArray[0]->BranchId); 
	
	$temp_api_url = $afis_api_url.'/Agreement/detailHistoryPayment/'.$customer_name.'/'.$branchId.'/'.$agreement_no;
	$temp_api_url = preg_replace('/[[:space:]]+/', '-', $temp_api_url);
	$folder 	   = 'files/'.$folder_andalan_customer_id;
	if(!file_exists($folder)) mkdir($folder);
	
	$newFileName   = 'Andalanku_Kartu_Piutang_'.md5($andalan_customer_id.$branchId.$agreement_no).'.pdf';
	
	if(downloadFile($temp_api_url, $folder . '/' . $newFileName)) {
		$tempArray['kartu_piutang'] = $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		
		// jika tidak ada file dari afis maka pakai file yang lama
		if(file_exists($folder . '/' . $newFileName)) {
			$tempArray['kartu_piutang'] = $api_url.'/'.$folder . '/' . $newFileName;
		}
		else {
			$tempArray['detail_history_message'] 	= 'Data belum tersedia';
			//$tempArray['detail_history_message'] 	= $temp_api_url;
			$tempArray['kartu_piutang'] 			= '';
		}
	}
	
	$andalan_detail_payment_history[0]	= $tempArray;
	
	
	//=================================== ANDALAN DETAIL INSURANCE ================================
    $andalan_detail_insurance = array();
	$tempArray				  = array();
	
	$temp_api_url  = $afis_api_url.'/Agreement/detailInsurance/'.$agreement_no;
	
	$folder_agreement_no = str_replace("/","",$agreement_no);
	$folder 	   = 'files/'.$folder_agreement_no;
	if(!file_exists($folder)) mkdir($folder);
	
	$newFileName   = 'Andalanku_Polis_Asuransi_'.md5($agreement_no).'.pdf';
	
	if(downloadFile($temp_api_url, $folder . '/' . $newFileName)) {
		$tempArray['polis'] = $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		
		// jika tidak ada file dari afis maka pakai file yang lama
		if(file_exists($folder . '/' . $newFileName)) {
			$tempArray['polis']	= $api_url.'/'.$folder . '/' . $newFileName;
		}
		else {
			$tempArray['detail_insurance_message'] = 'Data belum tersedia';
			$tempArray['polis'] = '';
		}
	}
	
	$andalan_detail_insurance[0] = $tempArray;	
	
	//=============================================================================================

	//=================================== ANDALAN PERJANJIAN KONTRAK ==============================
    $andalan_detail_insurance = array();
	$tempArray				  = array();
	
	$temp_api_url  = $afis_document_url.$agreement_no;
	$afis_response = json_decode(afis_call($temp_api_url, 'GET'));
	$responseStatus = $afis_response->status;
	
	
	if ($responseStatus == "Success") {
		$folder_agreement_no = str_replace("/","",$agreement_no);
		$folder 	   = 'files/'.$folder_agreement_no;
		if(!file_exists($folder)) mkdir($folder);
		
		$newFileName   = 'Andalanku_Perjanjian_Kontrak'.md5($agreement_no).'.pdf';

		$stringData = $afis_response->data;
		$data = base64_decode($afis_response->data);
		file_put_contents($folder . '/' . $newFileName,$data);
		$tempArray['andalan_agreement_document'] = $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		$tempArray['agreement_document_message'] = 'Data belum tersedia';
			$tempArray['polis'] = '';
	}
	
	
	/*if(downloadFile($temp_api_url, $folder . '/' . $newFileName)) {
		$tempArray['polis'] = $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		
		// jika tidak ada file dari afis maka pakai file yang lama
		if(file_exists($folder . '/' . $newFileName)) {
			$tempArray['agreement_document']	= $api_url.'/'.$folder . '/' . $newFileName;
		}
		else {
			$tempArray['agreement_document_message'] = 'Data belum tersedia';
			$tempArray['polis'] = '';
		}
	}*/
	
	$andalan_agreement_document[0] = $tempArray;	
	
	//=============================================================================================
	
	$api_response['status'] 						= 'success';
	$api_response['andalan_detail_unit']			= $andalan_detail_unit;
	$api_response['andalan_detail_financial']		= $andalan_detail_financial;
	$api_response['andalan_detail_payment_history']	= $andalan_detail_payment_history;
	$api_response['andalan_detail_insurance']		= $andalan_detail_insurance;
	$api_response['andalan_agreement_document']		= $andalan_agreement_document;
	
	echo json_encode($api_response);
	exit;
?>