<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$password_code		= sanitize_sql_string(trim($_REQUEST["password_code"]));
	$password			= sanitize_sql_string(trim($_REQUEST["password"]));
	$password_confirm	= sanitize_sql_string(trim($_REQUEST["password_confirm"]));
	
	if ($password_code <> '' and $password <> '' and $password_confirm <> '') {

		if($password <> $password_confirm) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Password and confirmation password not match';
			
			echo json_encode($api_response);
			exit;
		}

		if(strlen($password) < 8) {
		
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid password';
			
			echo json_encode($api_response);
			exit;
		}
		
		$queryCheck = "select id_customer from customers where password_code='$password_code'";
		$resultCheck= mysqli_query($mysql_connection, $queryCheck);
		if(mysqli_num_rows($resultCheck) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid password code';
			
			echo json_encode($api_response);
			exit;
		}
		
		$password 	  = md5($password);
		$dataCheck	  = mysqli_fetch_array($resultCheck);
		$id_customer  = $dataCheck['id_customer'];
		
		$queryUpdate  = "update customers set password='$password', password_code='' where id_customer='$id_customer'";
		$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
		
		$api_response['status'] 	= 'success';
		$api_response['message'] 	= 'Password reset successful';
		
		echo json_encode($api_response);
		exit;

	} else {

		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}	
?>