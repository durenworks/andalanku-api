<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$id_customer 		= sanitize_int($_REQUEST["id_customer"]);
	$occupation_id		= sanitize_int($_POST['occupation_id']);
	$monthly_income		= sanitize_int($_POST['monthly_income']);
	$additional_income	= sanitize_int($_POST['additional_income']);
	$side_job			= sanitize_sql_string(trim($_POST['side_job']));
	$andalan_branch_id	= sanitize_int($_POST['andalan_branch_id']);
	$survey_date		= sanitize_sql_string(trim($_POST['survey_date']));
	$survey_time		= sanitize_sql_string(trim($_POST['survey_time']));
	$survey_date		= $survey_date." ".$survey_time;
	
	$total_income		= $monthly_income + $additional_income;
	$plafond			= (($total_income / 3) * 36 ) * 0.75;
	
	//---ALAMAT
	$province_id		= sanitize_int($_POST['province_id']);
	if ($province_id == 0) {
		$province_id = sanitize_sql_string($_POST['province_id']);
		$query = "select id from provinces where name='$province_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id = $data['id'];
	}
	$city_id			= sanitize_int($_POST['city_id']);

	if ($city_id == 0) {
		$city_id = sanitize_sql_string($_POST['city_id']);
		$query = "select id from regencies where name='$city_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id = $data['id'];
	}

	$district_id		= sanitize_int($_POST['district_id']);
	if ($district_id == 0) {
		$district_id = sanitize_sql_string($_POST['district_id']);
		$query = "select id from districts where name='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id = $data['id'];
	}
	
	$village_id			= sanitize_sql_string(trim($_POST['village_id']));
	if ($village_id == 0) {
		$village_id = sanitize_sql_string($_POST['village_id']);
		$query = "select id from villages where name='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id = $data['id'];
	}
	
	$address			= sanitize_sql_string(trim($_POST['address']));
	$rt 				= sanitize_sql_string(trim($_POST['rt']));
	$rw					= sanitize_sql_string(trim($_POST['rw']));
	$zip_code			= sanitize_sql_string(trim($_POST['zip_code']));

	$province_id_domicile		= sanitize_int($_POST['province_id_domicile']);
	$city_id_domicile			= sanitize_int($_POST['city_id_domicile']);

	if ($city_id_domicile == 0) {
		$city_id_domicile = sanitize_sql_string($_POST['city_id_domicile']);
		$query = "select id from regencies where name='$city_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_domicile = $data['id'];
	}
	
	$district_id_domicile		= sanitize_int($_POST['district_id_domicile']);
	$village_id_domicile		= sanitize_sql_string(trim($_POST['village_id_domicile']));
	$address_domicile			= sanitize_sql_string(trim($_POST['address_domicile']));
	$rt_domicile 				= sanitize_sql_string(trim($_POST['rt_domicile']));
	$rw_domicile				= sanitize_sql_string(trim($_POST['rw_domicile']));
	$zip_code_domicile			= sanitize_sql_string(trim($_POST['zip_code_domicile']));

	if ($city_id_domicile == '') $city_id_domicile = 0;
	if ($district_id_domicile == '') $district_id_domicile = 0;
	if ($village_id_domicile ==  '') $village_id_domicile = 0;
	if ($province_id_domicile == '') $province_id_domicile = 0;

	$province_id_office		= sanitize_int($_POST['province_id_office']);
	$city_id_office			= sanitize_int($_POST['city_id_office']);

	if ($city_id_office == 0) {
		$city_id_office = sanitize_sql_string($_POST['city_id_office']);
		$query = "select id from regencies where name='$city_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_office = $data['id'];
	}
	
	$district_id_office		= sanitize_int($_POST['district_id_office']);
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$address_office			= sanitize_sql_string(trim($_POST['address_office']));
	$rt_office 				= sanitize_sql_string(trim($_POST['rt_office']));
	$rw_office				= sanitize_sql_string(trim($_POST['rw_office']));
	$zip_code_office		= sanitize_sql_string(trim($_POST['zip_code_office']));

	if ($province_id_office == '') $province_id_office= 0;
	if ($city_id_office == '') $city_id_office =0;
	if ($district_id_office == '') $district_id_office = 0;
	if ($village_id_office == '') $village_id_office = 0;

	
	
	$ticket_number = 'PFA'.rand(0000000, 9999999);
		
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from plafond_applications where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'PFS'.rand(0000000, 9999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
	
	$query = "insert into plafond_applications(ticket_number, 
			  customer_id, application_date, 
			  occupation_id, monthly_income, additional_income, 
			  side_job, plafond_amount, status, 
			  andalan_branch_id, survey_date)
			  values ('$ticket_number', 
			  '$id_customer', '$now',  
			  '$occupation_id', '$monthly_income', '$additional_income', 
			  '$side_job', '$plafond', 'SUBMITTED', 
			  '$andalan_branch_id', '$survey_date')";
	$result= mysqli_query($mysql_connection, $query); 
	
	//update tabel customer 
	$query = "update customers set occupation='$occupation_id', 
			  monthly_income='$monthly_income', additional_income='$additional_income', 
			  side_job='$side_job' where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	
	//=== ALAMAT ======================================================================================================================
	//=================================================================================================================================
	
	// Jika belum punya andalan customer id update semua alamat yang aktif sekarang
	// Jika sudah punya andalan customer id update alamat domisili dan office yang aktif sekarang
	
	$query	= "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$andalan_customer_id 	= $data['andalan_customer_id'];
	
	if($andalan_customer_id == '') {
			
		//===== LEGAL =====
		$queryCheck  = "select address_id from address_customers 
						where user_id='$id_customer' and is_active='1' and address_type='LEGAL'";
		$resultCheck = mysqli_query($mysql_connection, $queryCheck);
		
		if(mysqli_num_rows($resultCheck) > 0) {

			$dataCheck = mysqli_fetch_array($resultCheck);
			$address_id= $dataCheck['address_id'];
		
			//update alamat yang sudah ada 
			$queryUpdate = "update address set 
							province_id='$province_id', 
							regency_id='$city_id',
							district_id='$district_id', 
							village_id='$village_id', 
							zip_code='$zip_code',
							address='$address', 
							rt='$rt', 
							rw='$rw' 
							where id='$address_id' ";
			mysqli_query($mysql_connection, $queryUpdate); 
		}
		else {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id,
							district_id, village_id, zip_code,
							address, rt, rw)
							values('$province_id', '$city_id',
							'$district_id', '$village_id', '$zip_code',
							'$address', '$rt', '$rw')";
			mysqli_query($mysql_connection, $queryInsert); 

			//ambil id yang terakhir
			$query = "select id from address where address='$address' 
					  and rt='$rt' and rw='$rw' 
					  and village_id='$village_id' and district_id='$district_id' 
					  and regency_id='$city_id' and province_id='$province_id' 
					  order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];

			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('LEGAL', '$id_customer', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
	}
		
	//===== DOMICILE =====
	$queryCheck  = "select address_id from address_customers 
					where user_id='$id_customer' and is_active='1' and address_type='DOMICILE'";
	$resultCheck = mysqli_query($mysql_connection, $queryCheck);
	
	if(mysqli_num_rows($resultCheck) > 0) {

		$dataCheck = mysqli_fetch_array($resultCheck);
		$address_id= $dataCheck['address_id'];
	
		//update alamat yang sudah ada 
		$queryUpdate = "update address set 
						province_id='$province_id_domicile', 
						regency_id='$city_id_domicile',
						district_id='$district_id_domicile', 
						village_id='$village_id_domicile', 
						zip_code='$zip_code_domicile',
						address='$address_domicile', 
						rt='$rt_domicile', 
						rw='$rw_domicile' 
						where id='$address_id' ";
		mysqli_query($mysql_connection, $queryUpdate);
	}
	else {
		
		//insert ke tabel address
		$queryInsert = "insert into address(province_id, regency_id,
						district_id, village_id, zip_code,
						address, rt, rw)
						values('$province_id_domicile', '$city_id_domicile',
						'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile',
						'$address_domicile', '$rt_domicile', '$rw_domicile')";
		mysqli_query($mysql_connection, $queryInsert);

		//ambil id yang terakhir
		$query = "select id from address where address='$address_domicile' 
					  and rt='$rt_domicile' and rw='$rw_domicile' 
					  and village_id='$village_id_domicile' and district_id='$district_id_domicile' 
					  and regency_id='$city_id_domicile' and province_id='$province_id_domicile' 
					  order by id DESC LIMIT 1";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_address = $data['id'];

		$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
					VALUES('DOMICILE', '$id_customer', '$id_address', '1') ";
		mysqli_query($mysql_connection, $query);
	}
	
	//===== OFFICE =====
	$queryCheck  = "select address_id from address_customers 
					where user_id='$id_customer' and is_active='1' and address_type='OFFICE'";
	$resultCheck = mysqli_query($mysql_connection, $queryCheck);
	
	if(mysqli_num_rows($resultCheck) > 0) {

		$dataCheck = mysqli_fetch_array($resultCheck);
		$address_id= $dataCheck['address_id'];
	
		//update alamat yang sudah ada 
		$queryUpdate = "update address set 
						province_id='$province_id_office', 
						regency_id='$city_id_office',
						district_id='$district_id_office', 
						village_id='$village_id_office', 
						zip_code='$zip_code_office',
						address='$address_office', 
						rt='$rt_office', 
						rw='$rw_office' 
						where id='$address_id' ";
		mysqli_query($mysql_connection, $queryUpdate);
	}
	else {
		
		//insert ke tabel address
		$queryInsert = "insert into address(province_id, regency_id,
						district_id, village_id, zip_code,
						address, rt, rw)
						values('$province_id_office', '$city_id_office',
						'$district_id_office', '$village_id_office', '$zip_code_office',
						'$address_office', '$rt_office', '$rw_office')";
		mysqli_query($mysql_connection, $queryInsert);

		//ambil id yang terakhir
		$query = "select id from address where address='$address_office' 
					  and rt='$rt_office' and rw='$rw_office' 
					  and village_id='$village_id_office' and district_id='$district_id_office' 
					  and regency_id='$city_id_office' and province_id='$province_id_office' 
					  order by id DESC LIMIT 1";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$id_address = $data['id'];

		$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
					VALUES('OFFICE', '$id_customer', '$id_address', '1') ";
		mysqli_query($mysql_connection, $query);
	}	

	//==================================== AFIS CALL ====================================
	
	$query = "select a.*, b.name as occupation_name  
			  from customers a 
			  left join occupations b on a.occupation=b.id 
			  where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	
	$queryAddress = "select b.*, c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name   
					  from address_customers a  
					  left join address b on a.address_id=b.id 
					  left join provinces c on b.province_id=c.id 
					  left join regencies d on b.regency_id=d.id 
					  left join districts e on b.district_id=e.id 
					  left join villages f on b.village_id=f.id 
					  where a.user_id='$id_customer' and a.address_type='LEGAL' 
					  and a.is_active='1' ";
	$resultAddress= mysqli_query($mysql_connection, $queryAddress);
	$dataAddress  = mysqli_fetch_array($resultAddress);
	
	$afis_api_url	= $afis_api_url.'/andalanku/platfundrequest';
	
	$body_param		= array();
	$body_param['BranchId']				= $andalan_branch_id;
	$body_param['ProspectName']			= $data['customer_name'];
	$body_param['BirthPlace']			= $data['place_of_birth'];
	$body_param['BirthDate']			= $data['date_of_birth'];
	$body_param['IdNumber']				= $data['id_number'];
	$body_param['MotherName']			= $data['mother_name'];
	$body_param['PhoneNo']				= $data['phone_number'];
	$body_param['LegalAddress']			= $dataAddress['address'];
	$body_param['LegalRt']				= $dataAddress['rt'];
	$body_param['LegalRw']				= $dataAddress['rw'];
	$body_param['LegalKeluarahan']		= $dataAddress['village_name'];
	$body_param['LegalKecamatan']		= $dataAddress['district_name'];
	$body_param['LegalCity']			= $dataAddress['city_name'];
	$body_param['LegalZipCode']			= $dataAddress['zip_code'];
	$body_param['JobTypeId']			= $data['occupation_name'];
	$body_param['MonthlyFixeIncome']	= $monthly_income;
	$body_param['MonthlyVariableIncome']= $additional_income;
	$body_param['OtherBusinesName']		= $side_job;
	$body_param['SurveyRequest']		= $survey_date;
	$body_param['ImageKtp']				= '';
	$body_param['ImageKK']				= '';
	$body_param['ImageSlipGaji']		= '';
	$body_param['ImageRekanan']			= '';
	$body_param['ImageOthFinance']		= '';
	
	$jsonData = json_encode($body_param);
	$queryUpdate = "UPDATE plafond_applications SET json_data = '$jsonData' WHERE ticket_number = '$ticket_number'";
	mysqli_query($mysql_connection, $queryUpdate);
	
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param));
	
	if ($afis_response->StatusCode == 500) {
		
		$queryUpdate = "UPDATE plafond_applications SET afis_success = 0 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	} 
	else {
		$queryUpdate = "UPDATE plafond_applications SET afis_success = 1 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	}
	
	//==================================== AFIS CALL ====================================
		
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'SUBMITTED';
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami terima. ';
	$inbox_message				 .= 'Kami akan segera menindaklanjuti permohonan Anda.';
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'plafond application', 'Pengajuan Plafond', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	$api_response['status'] 		= 'success';
	$api_response['ticket_number'] 	= $ticket_number;
	$api_response['message'] 		= 'Plafond application successfully';
	$api_response['plafond']		= number_format($plafond, 0, ',', '.');
	
	echo json_encode($api_response);
	exit;
?>