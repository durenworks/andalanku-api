<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select * from provinces order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$province_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$province_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['province_list'] 	= $province_list;
	
	echo json_encode($api_response);
	exit;
?>