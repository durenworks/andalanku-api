<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	function base64_to_image($backend_folder, $media_image_folder, $base64_string) {
				
		$extension = 'png';
		$folder = $backend_folder."/".$media_image_folder;
		
		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;
		
		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = ''; 
		}

		fclose( $ifp ); 

		return $newnamefile; 
	}
	
	$id_customer 	= sanitize_int($_POST['id_customer']);
	$category		= sanitize_sql_string(trim($_POST['category']));
	$phone			= sanitize_sql_string(trim($_POST['phone']));
	$message		= sanitize_sql_string(trim($_POST['message']));
	$message		= strip_tags($message);
	$media			= $_POST['media'];
	
	if($id_customer=='0' || $category=='' || $phone=='' || $message=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//=================================================================================================================
	
	$ticket_number = 'C'.rand(000000000, 999999999);
	
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id_complain from complains where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'C'.rand(000000000, 999999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
		
	$queryInsert = "insert into complains(ticket_number, category, 
					user_id, phone, message, 
					status, submitted_date) 
					values('$ticket_number', '$category', 
					'$id_customer', '$phone', '$message', 
					'SUBMITTED', '$now')";
	mysqli_query($mysql_connection, $queryInsert);
	
	$arrayMediaID = array();
	
	//upload media
	$i = 0;
	$arrayMediaURL = array();
	foreach($media as $key=>$mediaFile) {
		
		$imageFileName = base64_to_image($backend_folder, $media_image_folder, $mediaFile);
		
		if($imageFileName <> '') {
			$queryInsert = "insert into media(url) values('$imageFileName')";
			mysqli_query($mysql_connection, $queryInsert);
			
			//ambil id yang terakhir
			$query = "select id_media from media where url='$imageFileName'";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$arrayMediaID[] = $data['id_media'];
			$arrayMediaURL[$i] = $backend_url.'/'.$media_image_folder.'/'.$imageFileName;
			$i++;
		}
	}
	
	//ambil id yang terakhir
	$query = "select id_complain from complains where ticket_number='$ticket_number'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$id_complain = $data['id_complain'];
	
	foreach($arrayMediaID as $key=>$id_media) {
		
		$queryInsert = "insert into complain_media(id_complain, id_media) values('$id_complain', '$id_media')";
		mysqli_query($mysql_connection, $queryInsert);
	}
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['category'] 	 	 = $category;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'SUBMITTED';
	$content['message'] 		 = $message;
	$content['processed_status'] = 'PENDING';
	$content['processed_date']   = '';
	$content['solved_status'] 	 = 'PENDING';
	$content['solved_date']   	 = '';
	$content['complain_media']   = $arrayMediaURL;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'e complain', 'E-Complain', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
		
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Add e-complain successful';
	$api_response['ticket_number'] 	= $ticket_number;
	
	echo json_encode($api_response);
	exit;
?>