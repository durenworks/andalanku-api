<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$id_customer 		= sanitize_int($_REQUEST["id_customer"]);
	$agreement_number	= sanitize_sql_string($_POST['agreement_number']);
	$taking_date		= sanitize_sql_string($_POST['taking_date']); 		
	
	if($id_customer=='0' || $agreement_number=='' || $taking_date=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//=================================================================================================================
	
	//Cek dulu di API AFIS apakah jaminan bisa diambil
	$api_base_url  = $afis_api_url;
	$afis_api_url  = $api_base_url.'/Agreement/CollateralRequest/'.$agreement_number;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$afis_status   = $responseArray[0]->STATUS; 
	
	if($afis_status == '1') {
		
		// dicek dulu, 1 nomor kontrak hanya boleh melakukan 1 kali request 
		$query = "select id from collateral_requests where agreement_number='$agreement_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) > 0) {
			
			$api_response['status']		= 'failed';
			$api_response['message'] 	= 'already submit';
			
			echo json_encode($api_response);
			exit;
		}
	
		// dicek apakah sisa tagihan lebih dari nol
		// jika masih ada sisa tagihan maka jaminan tidak bisa diambil
		/*$now = date("Y-m-d");
		$afis_api_url  = $api_base_url.'/Agreement/SimulasiPelunasan/'.$now.'/'.$agreement_number;
		$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
		$responseArray = $afis_response->Response->Data;
		$totalPelunasan=  trim($responseArray[0]->TotalPelunasan); 
		$sisaPinjaman  =  trim($responseArray[0]->SisaPinjaman);
		
		if($sisaPinjaman > 0) {
			
			$api_response['status']		= 'failed';
			$api_response['message'] 	= 'Bill amount greater than zero';
			
			echo json_encode($api_response);
			exit;
		}*/
	
		$ticket_number = 'CRQ'.rand(0000000, 9999999);
		
		$ticket_number_exist = true;
		
		while($ticket_number_exist) {
			
			$query = "select id from collateral_requests where ticket_number='$ticket_number'";
			$result= mysqli_query($mysql_connection, $query);
			if(mysqli_num_rows($result) == 0) {
				$ticket_number_exist = false;
			}
			else {
				$ticket_number = 'CRQ'.rand(0000000, 9999999);
			}
		}
		
		$now = date("Y-m-d H:i:s");
			
		$queryInsert = "insert into collateral_requests(ticket_number, customer_id, 
						request_date, agreement_number, 
						taking_date, status) 
						values('$ticket_number', '$id_customer', 
						'$now', '$agreement_number', 
						'$taking_date', 'INPUT REQUEST')";
		mysqli_query($mysql_connection, $queryInsert);
		
		//==================== AFIS CALL ====================
		
		$query = "select phone_number from customers where id_customer='$id_customer'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$phone_number = $data['phone_number'];
		
		$afis_api_url  = $api_base_url.'/andalanku/RegisterAmbilBPKB/';
		$body_param							= array();
		$body_param['TicketNo']				= $ticket_number;
		$body_param['TglRequest']			= $now;
		$body_param['BranchID']				= '';
		$body_param['ApplicationID']		= '';
		$body_param['AgreementNo']			= $agreement_number;
		$body_param['MobilePhone']			= $phone_number;
		$body_param['TglAmbil']				= $taking_date;
		
		$jsonData = json_encode($body_param);
		$queryUpdate = "UPDATE collateral_requests SET json_data = '$jsonData' WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
		
		$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param, true));
		$api_response['afis_response'] 	= $afis_response;	
		
		if ($afis_response->StatusCode == 500) {
			
			$queryUpdate = "UPDATE collateral_requests SET afis_success = 0 WHERE ticket_number = '$ticket_number'";
			mysqli_query($mysql_connection, $queryUpdate);
		} 
		else {
			$queryUpdate = "UPDATE collateral_requests SET afis_success = 1 WHERE ticket_number = '$ticket_number'";
			mysqli_query($mysql_connection, $queryUpdate);
		}
		
		//==================== INBOX ====================
		$content					 = array();
		$content['ticket_number'] 	 = $ticket_number;
		$content['input_date'] 		 = $now;
		$content['status'] 		 	 = 'INPUT REQUEST';
		$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
		$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami terima. ';
		$inbox_message				 .= 'Untuk jadwal pengambilan adalah maksimal 7 hari setelah pengajuan ini.';
		$content['message']			 = $inbox_message;
		$content = json_encode($content);
		
		//insert ke tabel inbox
		$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
						values('$id_customer', '$now', 'collateral request', 'E-Request Pengambilan Jaminan', '0', 
						'$content')";
		mysqli_query($mysql_connection, $queryInsert);
		//===============================================
		
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Add collateral request successful';
		$api_response['ticket_number'] 	= $ticket_number;
	}
	else {
	
		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Collateral request failed';
		$api_response['ticket_number'] 	= '';
	}
	
	echo json_encode($api_response);
	exit;
?>