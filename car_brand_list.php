<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$query 	= "select * from car_brands order by name ASC";
	$result = mysqli_query($mysql_connection, $query);
	
	$car_brand_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		if($data['image'] <> '')
			$data['image'] = $backend_url."/".$car_brand_image_folder."/".$data['image'];
		
		$car_brand_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['car_brand_list'] = $car_brand_list;
	
	echo json_encode($api_response);
	exit;
?>