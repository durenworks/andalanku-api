<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	function roundUpToNearestMultiple($n, $increment = 1000)
	{
		return (int) ($increment * ceil($n / $increment));
	}
	
	$type   		= sanitize_sql_string(trim($_POST['type']));		//(NEW, USED, KMG)
	$otr	 		= sanitize_int(trim($_POST['otr']));				//(harga mobil dalam rupiah)
	$dp				= sanitize_int(trim($_POST['dp']));					//(jumlah dp dalam persen)
	$tenor			= sanitize_sql_string(trim($_POST['tenor']));		//(jumlah tenor dalam bulan)
	$asuransi		= sanitize_sql_string(trim($_POST['asuransi']));	//(TLO, ARK)
	$wilayah		= sanitize_int(trim($_POST['wilayah']));			//(1, 2, 3)
	
	if($type=='' || $otr=='0' || $tenor=='0' || $asuransi=='' || $wilayah=='0') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	//dp kurang dari 20%
	if($type == 'NEW' || $type == 'USED') {
		
		if($dp < 20) {
			
			$api_response['status'] 		= 'false';
			$api_response['message'] 		= 'Minimum DP is 20% from OTR';
			$api_response['dp'] 			= '0';
			$api_response['tenor'] 			= '0';
			$api_response['pokok_hutang']	= '0';
			$api_response['angsuran'] 		= '0';
			$api_response['tipe_asuransi'] 	= ' ';
			$api_response['rate_asuransi']	= '0';
			$api_response['premi_asuransi']= '0';
			$api_response['provisi']		= '0';
			$api_response['biaya_admin']	= '0';
			$api_response['bunga_flat'] 	= '0';
			$api_response['total_hutang']	= '0';
			
			echo json_encode($api_response);
			exit;
		}
	}
	
	$pokok_hutang 	= $otr - ($otr * ($dp/100));
	$tenor_tahun 	= round($tenor/12);		
		
	if($type == 'NEW') {
		
		$queryRate	= "select flat_rate_new,admin from car_rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);
		
		$flat_rate_new  = $dataRate['flat_rate_new'];
		$biaya_admin	= $dataRate['admin'];
		
		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $flat_rate_new/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($flat_rate_new/100) * $tenor) / 12;
		$biaya_admin = $biaya_admin + 450000;
	}
	else if($type == 'USED') {
		
		$queryRate	= "select rate_used, flat_rate_used,admin from car_rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);
		
		$rate_used		= $dataRate['rate_used'];
		$flat_rate_used	= $dataRate['flat_rate_used'];
		$biaya_admin	= $dataRate['admin'];
		
		if($rate_used == 0) {
			
			$api_response['status'] 		= 'false';
			$api_response['message'] 		= 'Used car rates is 0';
			$api_response['dp'] 			= '0';
			$api_response['tenor'] 			= '0';
			$api_response['pokok_hutang']	= '0';
			$api_response['angsuran'] 		= '0';
			$api_response['tipe_asuransi'] 	= ' ';
			$api_response['rate_asuransi']	= '0';
			$api_response['premi_asuransi']= '0';
			$api_response['provisi']		= '0';
			$api_response['biaya_admin']	= '0';
			$api_response['bunga_flat'] 	= '0';
			$api_response['total_hutang']	= '0';
			
			echo json_encode($api_response);
			exit;
		}
		
		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $flat_rate_used/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($flat_rate_used/100) * $tenor) / 12;
		
	}		
	else if($type == 'KMG') {
		
		$queryRate	= "select rate,admin from rates where tenor='$tenor_tahun'";
		$resultRate = mysqli_query($mysql_connection, $queryRate);
		$dataRate	= mysqli_fetch_array($resultRate);
		
		$rate  			= $dataRate['rate'];
		$biaya_admin	= $dataRate['admin'];
		
		$angsuran = roundUpToNearestMultiple(((($pokok_hutang * $rate/100 )  / 12 * $tenor) + $pokok_hutang) / $tenor, 1000 );
		$bunga_flat = ($pokok_hutang * ($rate/100) * $tenor) / 12;		
	}
	
	$columnName = 'wilayah_'.$wilayah;
	$queryRate	= "select ".$columnName." from insurance_rates 
				   where asuransi='$asuransi' and otr_range_min<='$otr' and otr_range_max>='$otr'";
	$resultRate = mysqli_query($mysql_connection, $queryRate);
	$dataRate	= mysqli_fetch_array($resultRate);
	$rate_asuransi = $dataRate[$columnName]; 		
	
	$depresiasi = array();
	$depresiasi[1] = '100';
	$depresiasi[2] = '90';
	$depresiasi[3] = '85';
	$depresiasi[4] = '75';
	$depresiasi[5] = '65';
	
	for ($i = 1; $i <= $tenor_tahun; $i++) {
		$premi_asuransi += ($rate_asuransi/100) * ($otr * ($depresiasi[$i]/100));
	}
	
	$premi_asuransi += ($tenor_tahun * 100000);
	
	$provisi = $pokok_hutang * (2/100);	

	$total_hutang = $angsuran * $tenor;
	
	$dp_rupiah = $otr * ($dp/100);
	
	//=================================================================================================================
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Simulation complete';
	$api_response['dp'] 			= number_format($dp_rupiah,0,',','.');
	$api_response['tenor'] 			= $tenor;
	$api_response['pokok_hutang']	= number_format($pokok_hutang,0,',','.');
	$api_response['angsuran'] 		= number_format($angsuran,0,',','.');
	$api_response['tipe_asuransi'] 	= $asuransi;
	$api_response['rate_asuransi']	= $rate_asuransi.'%';
	$api_response['premi_asuransi'] = number_format($premi_asuransi,0,',','.');
	$api_response['provisi']		= number_format($provisi,0,',','.');
	$api_response['biaya_admin']	= number_format($biaya_admin,0,',','.');
	$api_response['bunga_flat'] 	= number_format($bunga_flat,0,',','.');
	$api_response['total_hutang']	= number_format($total_hutang,0,',','.');
	
	echo json_encode($api_response);
	exit;
?>