<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$query_customer = "SELECT distinct customer_id from agreement_list where branch is null or branch = ''";
	$resultCustomer= mysqli_query($mysql_connection, $query_customer);
	while ($dataCustomer = mysqli_fetch_assoc($resultCustomer)) {
		$id_customer = $dataCustomer["customer_id"];
		
		$query = "select andalan_customer_id from customers where id_customer='$id_customer'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$andalan_customer_id = $data['andalan_customer_id'];
		
		$agreement_list = array();
		
		
		if($andalan_customer_id<>'' && $andalan_customer_id<>'0') {		
				
			$api_url  	   = $afis_api_url.'/Agreement/ActiveAgrementNumber/'.$andalan_customer_id;
			$afis_response = json_decode(afis_call($api_url, 'GET'));
			
			$responseArray = $afis_response->Response->Data;
			
			$i = 0;
			
			$strQuery = "";
			
			$nowSQL = date("Y-m-d H:i:s");
			
			foreach($responseArray as $key=>$value) {
				
				$agreementNo = trim($value->agreementNo);
				$branch = trim($value->branchfullname);
				
				if($agreementNo<>'' && $agreementNo<>'-') {
					
					$tempArray = array();
					
					//sekalian ambil kategori asuransi untuk nomor kontrak ini 
					$api_url  			= $afis_api_url.'/AndalanKu/KlaimAsuransi/'.$agreementNo;
					$afis_response 		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayIns	= $afis_response->Response->Data; 
					$insuranceCategory	= $responseArrayIns[0]->MainCoverage;
					
					//sekalian ambil sisa tagihan
					$now = date("Y-m-d");
					$api_url			= $afis_api_url.'/Agreement/SimulasiPelunasan/'.$now.'/'.$agreementNo;
					$afis_response		= json_decode(afis_call($api_url, 'GET'));
					$responseArrayTag	= $afis_response->Response->Data;
					$sisaPinjaman		= trim($responseArrayTag[0]->SisaPinjaman);
					
					$strQuery = $strQuery."('$id_customer', '$agreementNo', '$branch', '$insuranceCategory', '$sisaPinjaman', '$nowSQL'),";
					
					$tempArray['agreement_no']			= $agreementNo;
					$tempArray['branch']				= $branch;
					$tempArray['insurance_category']	= $insuranceCategory;
					$tempArray['sisa_pinjaman']			= number_format($sisaPinjaman,0,',','.');
					$agreement_list[$i] =  $tempArray;
					$i++;
				}
			}		
		}
		
		if($i > 0) {
			
			//hapus dulu data yang lama
			$query = "delete from agreement_list where customer_id='$id_customer'";
			mysqli_query($mysql_connection, $query);
			
			$queryInsert = "insert into agreement_list(customer_id, agreement_number, branch, insurance_category, sisa_pinjaman, last_update) values ";
			$strQuery 	 = substr($strQuery, 0, strlen($strQuery)-1);
			$queryInsert = $queryInsert.$strQuery;
			mysqli_query($mysql_connection, $queryInsert);
		}
		else {
			
			//tidak ada respon dari afis, pakai data sebelumnya yang sudah ada di database
			$query = "select * from agreement_list where id_customer='$id_customer'";
			$result= mysqli_query($mysql_connection, $query);
			
			$i = 0;
			
			while($data = mysqli_fetch_array($result)) {
				
				$tempArray = array();
				
				$tempArray['agreement_no']			= $data['agreement_number'];
				$tempArray['insurance_category']	= $data['insurance_category'];
				$tempArray['sisa_pinjaman']			= number_format($data['sisa_pinjaman'],0,',','.');
				$agreement_list[$i] =  $tempArray;
				$i++;
			}
		}
	
	}
	echo json_encode('success');
	exit;
?>