<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_customer 	= sanitize_int($_POST['id_customer']);
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$query 	= "select a.*, b.customer_name 
			   from complains a 
			   left join customers b on a.user_id=b.id_customer 
 			   where a.user_id='$id_customer' ";
	if($status == '') $query = $query." order by submitted_date DESC ";
	else if($status == 'SUBMITTED') $query = $query." and a.status='SUBMITTED' order by submitted_date DESC ";
	else if($status == 'ON PROCESS') $query = $query." and a.status='ON PROCESS' order by on_process_date DESC ";
	else if($status == 'SOLVED') $query = $query." and a.status='SOLVED' order by solved_date DESC ";
	
	$result = mysqli_query($mysql_connection, $query);
	
	$complain_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		//$data['image'] = $backend_url."/".$news_image_folder."/".$data['image'];
		
		$loopIDComplain = $data['id_complain'];
		
		//ambil media 
		$queryMedia  = "select url 
					    from complain_media a 
					    left join media b on a.id_media=b.id_media 
					    where a.id_complain='$loopIDComplain' ";
		$resultMedia = mysqli_query($mysql_connection, $queryMedia);
		
		$media_list	   = array();
		$n = 0;
		
		while ($dataMedia = mysqli_fetch_assoc($resultMedia)) {
			$media_list[$n] = $backend_url."/".$media_image_folder."/".$dataMedia['url'];
			$n++;
		}
		
		$data['media_list']= $media_list;
		$complain_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['complain_list'] 	= $complain_list;
	
	echo json_encode($api_response);
	exit;
?>