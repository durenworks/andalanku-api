<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$ticket_number	= sanitize_sql_string(trim($_POST['ticket_number']));
	$ready_date		= sanitize_sql_string(trim($_POST['ready_date']));
	$form_number	= sanitize_sql_string(trim($_POST['form_number']));
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	if($status == '') $status = 'ON PROCESS';
	
	$queryCheck = "select * from collateral_requests where ticket_number='$ticket_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid ticket number';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data			= mysqli_fetch_assoc($resultCheck);
	$request_date	= $data['request_date'];
	$id_customer	= $data['customer_id'];
	
	$queryUpdate = "update collateral_requests set ready_date='$ready_date', status='$status', form_number='$form_number'  
					where ticket_number='$ticket_number'";
	$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $request_date;
	$content['status'] 		 	 = 'ON PROCESS';
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami proses. ';
	$inbox_message				 .= 'Dokumen jaminan Anda dapat diambil pada tanggal : '.date("d-m-Y", strtotime($ready_date));
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//update tabel inbox
	$now			= date('Y-m-d H:i:s');
	$queryCheck 	= "select id from inbox where content like '%".$ticket_number."%' ";
	$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck		= mysqli_fetch_array($resultCheck);
	$id_inbox		= $dataCheck['id'];
	$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
	mysqli_query($mysql_connection, $queryInsert);	
	//===============================================
	
	// TO-DO : Kirim push notification
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Collateral request updated successfully';
	
	echo json_encode($api_response);
	exit;
?>