<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select * from tutorials where is_active='TRUE' order by id_tutorial ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$tutorial_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$tutorialID = $data['id_tutorial'];
		
		//get tutorial answer
		$tutorial_answer_list = array();
		$n = 0;
		
		$queryAnswer  = "select * from tutorial_answers where is_active='TRUE' and id_tutorial='".$tutorialID."'";
		$resultAnswer = mysqli_query($mysql_connection, $queryAnswer);
		while ($dataAnswer = mysqli_fetch_assoc($resultAnswer)) {
			
			if($dataAnswer['image'] <> '')
				$dataAnswer['image'] = $backend_url."/".$tutorial_image_folder."/".$dataAnswer['image'];
			
			$tutorial_answer_list[$n] = $dataAnswer;
			$n++;
		}
		
		$data['tutorial_answer_list'] = $tutorial_answer_list;
		
		$tutorial_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['tutorial_list'] 	= $tutorial_list;
	
	echo json_encode($api_response);
	exit;
?>