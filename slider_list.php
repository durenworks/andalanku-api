<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$query 	= "select * from sliders
			   order by id_slider DESC";
	$result = mysqli_query($mysql_connection, $query);
	
	$slider_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		if($data['id_promo']<>'0') {
			
			$queryPromo  = 'select title,slug,promo_start_date, promo_expired_date  
							from promos where id_promo="'.$data['id_promo'].'"';
			$resultPromo = mysqli_query($mysql_connection, $queryPromo);
			$dataPromo   = mysqli_fetch_array($resultPromo);
			
			$promoTitle	 		= $dataPromo['title'];
			$promoSlug	 		= $dataPromo['slug'];
			$promoStartDate		= $dataPromo['promo_start_date'];
			$promoExpiredDate	= $dataPromo['promo_expired_date'];
			$now = date("Y-m-d");
			
			//cek dulu apakah promo masih berlaku
			if(strtotime($now)>=strtotime($promoStartDate) && strtotime($now)<=strtotime($promoExpiredDate)) {
				
				$data['image'] 		= $backend_url."/".$slider_image_folder."/".$data['image'];
				$data['promo_title']= $promoTitle;
				$data['promo_slug']	= $promoSlug;
				$slider_list[$i] = $data;
				$i++;
			}
		}
		else {
			
			$data['image'] 		= $backend_url."/".$slider_image_folder."/".$data['image'];
			$data['promo_title']= '';
			$data['promo_slug']	= '';
			$slider_list[$i] = $data;
			$i++;
		}
	}
	
	$api_response['status'] 		= 'success';
	$api_response['slider_list'] 	= $slider_list;
	
	echo json_encode($api_response);
	exit;
?>