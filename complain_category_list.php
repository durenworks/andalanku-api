<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	include "common_vars.php";
	
	$api_response['status'] 					= 'success';
	$api_response['complain_category_list'] 	= $const_complain_category;
	
	echo json_encode($api_response);
	exit;
?>