<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";

	$id_customer = sanitize_int($_REQUEST["id_customer"]);
	$category = sanitize_int($_REQUEST["category"]);
	$status = sanitize_int($_REQUEST["status"]);
	$page 		 = sanitize_int($_REQUEST["page"]);
	
	if($page == '0') $page = '1';


	if($status == 'all'){
		$ppob_status = "and transaction_status <> 'INQUIRY'";
		$install_status ="and payment_status <> 'INQUIRY'";
	}
	else if($status == 'waiting'){
		$ppob_status = "and transaction_status = 'PAYMENT'";
		$install_status = "and payment_status = 'PAYMENT'";
	}
	else if($status == 'done'){
		$ppob_status = "and transaction_status = 'PAYMENT DONE'";
		$install_status = "and and payment_status = PAYMENT DONE'";
	}
	else if($status == 'expired'){
		$ppob_status = "and transaction_status = 'EXPIRED'";
		$install_status = "and and payment_status = 'EXPIRED'";
	}
	else{
		$ppob_status = "";
		$install_status = "";
	}

	$query_ppob		= "select COUNT(id) as num
	   from ppob_transaction  
	   where customer_id = '$id_customer'". $ppob_status;
	$result_ppob	= mysqli_query($mysql_connection, $query_ppob);
	$data_ppob		= mysqli_fetch_assoc($result_ppob);

	$query_install	= "select COUNT(id) as num
	   from transaction_installment  
	   where customer_id = '$id_customer'". $install_status;
	$result_install	= mysqli_query($mysql_connection, $query_install);
	$data_install	= mysqli_fetch_assoc($result_install);
	
	if ($data_ppob[num] > $data_install[num]){ $total_pages	= $data_ppob[num]; }
	else if ($data_ppob[num] < $data_install[num]){ $total_pages	= $data_install[num]; }
	else{ $total_pages	= $data_install[num]; }

	if($page) $start = ($page - 1) * $limit;
	else $start = 0;

	if($category == 'all'){
		$query_ppob = "select *
			from ppob_transaction 
			where customer_id = '$id_customer'".$ppob_status." 
			order by transaction_date DESC LIMIT $start,$limit";
		$result_ppob = mysqli_query($mysql_connection, $query_ppob);

		$query_install = "select * 
			from transaction_installment 
			where id_customer = '$id_customer'".$install_status." 
			order by inquiry_date DESC LIMIT $start,$limit";
		$result_install = mysqli_query($mysql_connection, $query_install);		
	}
	else if($category == 'installment'){
		$query_install = "select * 
			from transaction_installment 
			where id_customer = '$id_customer'".$install_status." 
			order by inquiry_date DESC LIMIT $start,$limit";
		$result_install = mysqli_query($mysql_connection, $query_install);
	}
	else if($category != 'all' && $category != 'installment'){
		$query_ppob = "select *
			from ppob_transaction 
			where category = '$category' and customer_id = '$id_customer'".$ppob_status." 
			order by transaction_date DESC LIMIT $start,$limit";
		$result_ppob = mysqli_query($mysql_connection, $query_ppob);
	}	

	$ppob_history_list = array();
	$install_history_list = array();
	
	$i = 0;
	while ($data_ppob = mysqli_fetch_assoc($result_ppob)){
		$ppob_history_list[$i] = $data_ppob;
		$i++;
	}

	$i = 0;
	
	while ($data_install = mysqli_fetch_assoc($result_install)) {
		
		//tambahkan detailnya
		$queryDetail  = "select payment_datetime, 
						 debit_from_name, debit_from, 
						 credit_to_name, credit_to, 
						 product_code, 
						 amount as payment_amount , ccy as currency
					     from payment_history_detail 
						 where id_payment_history='".$data_install['id']."'";
		$resultDetail = mysqli_query($mysql_connection, $queryDetail);
		$dataDetail   = mysqli_fetch_assoc($resultDetail);
		
		$data_install['payment_datetime'] 	= $dataDetail['payment_datetime'];
		$data_install['debit_from_name'] 	= $dataDetail['debit_from_name'];
		$data_install['debit_from'] 		= $dataDetail['debit_from'];
		$data_install['credit_to_name'] 	= $dataDetail['credit_to_name'];
		$data_install['credit_to'] 			= $dataDetail['credit_to'];
		$data_install['product_code'] 		= $dataDetail['product_code'];
		$data_install['payment_amount'] 	= $dataDetail['payment_amount'];
		$data_install['currency'] 			= $dataDetail['currency'];
		
		$install_history_list[$i] = $data_install;
		$i++;
	}
	
	$api_response['status'] 				= 'success';
	$api_response['ppob_history_list'] 		= $ppob_history_list;
	$api_response['install_history_list'] 	= $install_history_list;
	
	echo json_encode($api_response);
	exit;
?>