<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";	
	include "validate_token.php";
	include "jwt.php";
	include "afis_call.php";
	
	$id_customer = sanitize_int($_REQUEST["id_customer"]);
	
	if($id_customer=='0') {
		
		$api_response['status']			= 'failed';
		$api_response['message'] 		= 'Input not complete';
		$api_response['product_list'] 	= '';
		
		echo json_encode($api_response);
		exit;
	}
	
	// ========================================================================
	
	$query 	= "select b.* 
			   from ppob_transaction a 
			   left join ppob_category b on a.category=b.name  
			   where a.customer_id='$id_customer' 
			   order by a.id DESC limit 0,4";
	$result = mysqli_query($mysql_connection, $query);
	
	$last_purchase = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$last_purchase[$i] = $data;
		$i++;
	}
	
	// ========================================================================
	
	$query 	= "select b.* 
			   from ppob_transaction a 
			   left join ppob_category b on a.category=b.name  
			   where a.customer_id='$id_customer' and b.ppob_group='top up'
			   order by a.id DESC limit 0,4";
	$result = mysqli_query($mysql_connection, $query);
	
	$top_up = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$top_up[$i] = $data;
		$i++;
	}
	
	// ========================================================================
	
	$query 	= "select b.* 
			   from ppob_transaction a 
			   left join ppob_category b on a.category=b.name  
			   where a.customer_id='$id_customer' and b.ppob_group='tagihan'
			   order by a.id DESC limit 0,4";
	$result = mysqli_query($mysql_connection, $query);
	
	$tagihan = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$tagihan[$i] = $data;
		$i++;
	}
	
	// ========================================================================
	
	$query 	= "select b.* 
			   from ppob_transaction a 
			   left join ppob_category b on a.category=b.name  
			   where a.customer_id='$id_customer' and b.ppob_group='hiburan'
			   order by a.id DESC limit 0,4";
	$result = mysqli_query($mysql_connection, $query);
	
	$hiburan = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		
		$hiburan[$i] = $data;
		$i++;
	}
	
	// ========================================================================
	
	$api_response['status'] 		= 'success';
	$api_response['last_purchase'] 	= $last_purchase;
	$api_response['top_up'] 		= $top_up;
	$api_response['tagihan'] 		= $tagihan;
	$api_response['hiburan'] 		= $hiburan;
	
	echo json_encode($api_response);
	exit;
?>