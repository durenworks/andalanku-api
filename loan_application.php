<?php
	ini_set("display_errors","0");
	error_reporting(1);

	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require 'plugins/phpmailer/src/Exception.php';
	require 'plugins/phpmailer/src/PHPMailer.php';
	require 'plugins/phpmailer/src/SMTP.php';

	function base64_to_image($backend_folder, $image_folder, $base64_string) {

		$extension = 'png';
		$folder = $backend_folder."/".$image_folder;

		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;

		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}
		
		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = '';
		}
		
		fclose( $ifp );

		return $newnamefile;
	}

	function generateRandomString($length = 10) {

		$characters 		= '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength 	= strlen($characters);
		$randomString 		= '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}

	// ====== UPPERCASE SEMUA REQUEST ======
	$return_array = array();
	foreach($_POST as $postKey => $postVar){
		
		if(strpos($postKey, 'foto_') !== false)
			$return_array[$postKey] = $postVar;
		else if(strpos($postKey, 'bank_account_image') !== false)
			$return_array[$postKey] = $postVar;
		else 
			$return_array[$postKey] = strtoupper($postVar);
    }
	$_POST = $return_array;
	// =====================================
	
	//loan_applications, loan_application_status
	$id_customer 				= sanitize_int($_POST['id_customer']);
	$product					= sanitize_sql_string(trim($_POST['product'])); 				// NEW CAR, USED CAR, DANA ANDALANKU

    // menyeragamlan nama produk
    if ($product == 'NEW') {
        $product = 'NEW CAR';
    }
    if ($product == 'USED') {
        $product = 'USED CAR';
    }
    if ($product == 'KMG') {
        $product = 'DANA ANDALANKU';
    }

	$otr						= sanitize_int($_POST['otr']);
	$dp_percent					= sanitize_int($_POST['dp_percent']);
    if ($dp_percent > 100) {
        $dp_percent = $dp_percent / 100;
    }
	$dp_amount					= sanitize_int($_POST['dp_amount']);
	$plafond					= sanitize_int($_POST['plafond']);
	$tenor						= sanitize_int($_POST['tenor']);								//(jumlah tenor dalam bulan)
	$insurance					= sanitize_sql_string(trim($_POST['insurance']));				//(TLO, ARK)
	if ($insurance == '1') {
		$insurance = 'TLO';
	}
	elseif ($insurance == '2') {
		$insurance = 'ARK';
	}
	$region						= sanitize_sql_string(trim($_POST['region']));					//(1, 2, 3)
	$collateral_type			= sanitize_sql_string(trim($_POST['collateral_type']));			// BPKB, Sertifikat Tanah
	$collateral					= sanitize_sql_string(trim($_POST['collateral']));				// BPKB : merk/type/model, Sertifikat : jenis/luas/tanggal terbit sertifikat
	$collateral_year			= sanitize_sql_string(trim($_POST['collateral_year']));
	$collateral_owner			= sanitize_sql_string(trim($_POST['collateral_owner']));		// hanya terisi jika product = dana andalanku, lainnya null (nama pemilik)
	$collateral_paper_number	= sanitize_sql_string(trim($_POST['collateral_paper_number']));	// hanya terisi jika product = dana andalanku, lainnya null (nomor polisi di BPKB atau nomor sertifikat)
	$car_year					= sanitize_sql_string(trim($_POST['car_year']));
	$asset_code					= sanitize_sql_string(trim($_POST['asset_code']));
	$regional_mrp				= sanitize_sql_string(trim($_POST['regional_mrp']));
	$branch_id					= sanitize_int($_POST['branch_id']);
	$survey_date				= sanitize_sql_string(trim($_POST['survey_date']));				// YYYY-mm-dd
    $survey_clock				= sanitize_sql_string(trim($_POST['survey_clock']));
	$loan_application_referal	= sanitize_sql_string(trim($_POST['loan_application_referal']));// Diisi Y jika mengajukan kredit untuk orang lain, dan diisi N jika mengajukan kredit untuk diri sendiri
	$installment				= sanitize_int($_POST['installment']);
	$received_funds				= sanitize_int($_POST['recieved_funds']);

	if ($product == 'DANA ANDALANKU') {
		$car_year = $collateral_year;
		
		$first_slash	= stripos($collateral,'/') + 1;
		$last_slash		= strripos($collateral,'/');
		$substr_length	= $last_slash - $first_slash;
		$collateral_car_name = substr($collateral, $first_slash, $substr_length);
	}
	if ($product == 'NEW CAR') {
		$car_year = date("Y");
		$collateral_year = $car_year;
	}

    if ($survey_clock == '') $survey_clock = '00:00';
    $survey_date = $survey_date . ' ' . $survey_clock . ':00';

	//customers
	$customer_name		= sanitize_sql_string(trim($_POST['customer_name']));
	$email				= sanitize_sql_string(trim($_POST['email']));
	$gender 			= sanitize_sql_string(trim($_POST['gender']));
	$place_of_birth 	= sanitize_sql_string(trim($_POST['place_of_birth']));
	$date_of_birth  	= sanitize_sql_string(trim($_POST['date_of_birth']));		// YYYY-mm-dd
	$id_number 			= sanitize_sql_string(trim($_POST['id_number']));
	$mother_name 		= sanitize_sql_string(trim($_POST['mother_name']));
	$home_phone_number 	= sanitize_sql_string(trim($_POST['home_phone_number']));
	$area_code 			= sanitize_sql_string(trim($_POST['area_code']));
	$phone_number 		= sanitize_sql_string(trim($_POST['phone_number']));		//note : tidak disimpan ke tabel customer, karena pada saat registrasi sudah pernah mengisi dan sudah diverifikasi dengan OTP
	$occupation_id		= sanitize_int($_POST['occupation_id']);
	$monthly_income		= sanitize_int($_POST['monthly_income']);
	$additional_income	= sanitize_int($_POST['additional_income']);
	$side_job			= sanitize_sql_string(trim($_POST['side_job']));

	//address, customer_address (note : jika sudah ada alamat LEGAL aktif di tabel address maka tidak perlu menyimpan lagi)
	$province_id		= sanitize_int($_POST['province_id']);
	if ($province_id == null) {
		$province_id = sanitize_sql_string($_POST['province_id']);

		$query = "select id from provinces where name='$province_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id = $data['id'];
	}
	$city_id			= sanitize_int($_POST['city_id']);

	if ($city_id == null) {
		$city_id = sanitize_sql_string($_POST['city_id']);

		$query = "select id from regencies where name='$city_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id = $data['id'];
	}

	$district_id		= sanitize_int($_POST['district_id']);
	if ($district_id == null) {
		$district_id = sanitize_sql_string($_POST['district_id']);

		$query = "select id from districts where name='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id = $data['id'];
	}
	
	$village_id			= sanitize_sql_string(trim($_POST['village_id']));
	if ($village_id == null) {
		$village_id = sanitize_sql_string($_POST['village_id']);

		$query = "select id from villages where name='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id = $data['id'];
	}
	
	$address			= sanitize_sql_string(trim($_POST['address']));
	$rt 				= sanitize_sql_string(trim($_POST['rt']));
	$rw					= sanitize_sql_string(trim($_POST['rw']));
	$zip_code			= sanitize_sql_string(trim($_POST['zip_code']));

	$province_id_domicile		= sanitize_int($_POST['province_id_domicile']);
	$city_id_domicile			= sanitize_int($_POST['city_id_domicile']);

	if ($city_id_domicile == null) {
		$city_id_domicile = sanitize_sql_string($_POST['city_id_domicile']);

		$query = "select id from regencies where name='$city_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_domicile = $data['id'];
	}
	
	$district_id_domicile		= sanitize_int($_POST['district_id_domicile']);
	$village_id_domicile		= sanitize_sql_string(trim($_POST['village_id_domicile']));
	$address_domicile			= sanitize_sql_string(trim($_POST['address_domicile']));
	$rt_domicile 				= sanitize_sql_string(trim($_POST['rt_domicile']));
	$rw_domicile				= sanitize_sql_string(trim($_POST['rw_domicile']));
	$zip_code_domicile			= sanitize_sql_string(trim($_POST['zip_code_domicile']));

	if ($city_id_domicile == '') $city_id_domicile = 0;
	if ($district_id_domicile == '') $district_id_domicile = 0;
	if ($village_id_domicile ==  '') $village_id_domicile = 0;
	if ($province_id_domicile == '') $province_id_domicile = 0;

	$province_id_office		= sanitize_int($_POST['province_id_office']);
	$city_id_office			= sanitize_int($_POST['city_id_office']);

	if ($city_id_office == null) {
		$city_id_office = sanitize_sql_string($_POST['city_id_office']);

		$query = "select id from regencies where name='$city_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_office = $data['id'];
	}
	
	$district_id_office		= sanitize_int($_POST['district_id_office']);
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$address_office			= sanitize_sql_string(trim($_POST['address_office']));
	$rt_office 				= sanitize_sql_string(trim($_POST['rt_office']));
	$rw_office				= sanitize_sql_string(trim($_POST['rw_office']));
	$zip_code_office		= sanitize_sql_string(trim($_POST['zip_code_office']));

	if ($province_id_office == '') $province_id_office= 0;
	if ($city_id_office == '') $city_id_office =0;
	if ($district_id_office == '') $district_id_office = 0;
	if ($village_id_office == '') $village_id_office = 0;

	//customer_documents
	$foto_ktp				= $_POST['foto_ktp'];
	$foto_kk				= $_POST['foto_kk'];
	$foto_slip_gaji			= $_POST['foto_slip_gaji'];
	$foto_rekening_koran	= $_POST['foto_rekening_koran'];
	$foto_keuangan_lainnya	= $_POST['foto_keuangan_lainnya'];
	
	//data rekening bank 
	$bank_name				= sanitize_sql_string(trim($_POST['bank_name']));
	$bank_account_number	= sanitize_sql_string(trim($_POST['bank_account_number']));
	$bank_account_holder	= sanitize_sql_string(trim($_POST['bank_account_holder']));
	$bank_account_image		= $_POST['bank_account_image'];

	if( $id_customer=='0') {

		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';

		echo json_encode($api_response);
		exit;
	}
	/*if( $id_customer=='0' ||
		$product=='' || $otr=='0' || $tenor=='0' || $insurance=='' || $region=='' || $collateral_type=='' || 
		$place_of_birth=='' || $date_of_birth=='' || $id_number=='' || $mother_name=='' || $phone_number=='' || $occupation_id=='' || $monthly_income=='' ||
		$province_id=='' || $city_id=='' || $district_id=='' || $village_id=='' || $address=='' || $rt=='' || $rw=='' || $zip_code=='') {

		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';

		echo json_encode($api_response);
		exit;
	}*/

	//=================================================================================================================================
	//=================================================================================================================================

	$ticket_number = 'PKA'.generateRandomString(7);

	$ticket_number_exist = true;

	while($ticket_number_exist) {

		$query = "select id from loan_applications where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'PKA'.generateRandomString(7);
		}
	}

	$now = date("Y-m-d H:i:s");

	if($product <> 'DANA ANDALANKU') {
		$collateral_owner = '';
		$collateral_paper_number = '';
	}

	//insert ke tabel loan_applications
	$queryInsert = "insert into loan_applications(ticket_number, date,
					customer_id, product, otr,
					dp_percent, dp_amount, plafond, tenor,
					insurance, region,
					collateral_type, collateral, collateral_year,
					collateral_owner, collateral_paper_number,
					loan_application_referal,
					branch_id, survey_date,
					loan_customer_name, loan_email,
					loan_gender, loan_place_of_birth,
					loan_date_of_birth, loan_id_number,
					loan_mother_name, loan_home_phone_number, loan_area_code, 
					loan_phone_number, loan_occupation_id,
					loan_monthly_income, loan_additional_income,
					loan_side_job, loan_address,
					loan_rt, loan_rw,
					loan_zip_code, loan_village_id,
					loan_district_id, loan_city_id,
					loan_province_id,
					loan_address_domicile,
					loan_rt_domicile, loan_rw_domicile,
					loan_zip_code_domicile, loan_village_id_domicile,
					loan_district_id_domicile, loan_city_id_domicile,
					loan_province_id_domicile,
					loan_address_office,
					loan_rt_office, loan_rw_office,
					loan_zip_code_office, loan_village_id_office,
					loan_district_id_office, loan_city_id_office,
					loan_province_id_office,
					status, installment, received_funds,
					is_active
					)
					values('$ticket_number', '$now',
					'$id_customer', '$product', '$otr',
					'$dp_percent', '$dp_amount', '$plafond', '$tenor',
					'$insurance', '$region',
					'$collateral_type', '$collateral', '$collateral_year',
					'$collateral_owner', '$collateral_paper_number',
					'$loan_application_referal',
					'$branch_id', '$survey_date',
					'$customer_name', '$email',
					'$gender', '$place_of_birth',
					'$date_of_birth', '$id_number',
					'$mother_name', '$home_phone_number', '$area_code', 
					'$phone_number', '$occupation_id',
					'$monthly_income', '$additional_income',
					'$side_job', '$address',
					'$rt', '$rw',
					'$zip_code', '$village_id',
					'$district_id', '$city_id',
					'$province_id',
					'$address_domicile',
					'$rt_domicile', '$rw_domicile',
					'$zip_code_domicile', '$village_id_domicile',
					'$district_id_domicile', '$city_id_domicile',
					'$province_id_domicile',
					'$address_office',
					'$rt_office', '$rw_office',
					'$zip_code_office', '$village_id_office',
					'$district_id_office', '$city_id_office',
					'$province_id_office',
					'Pengajuan',
					'$installment', '$received_funds',
					'1'
					)";
	mysqli_query($mysql_connection, $queryInsert);
	//var_dump($queryInsert);

	//ambil id yang terakhir
	$query = "select id from loan_applications where ticket_number='$ticket_number'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$loan_application_id = $data['id'];


	//insert ke tabel loan_applications_status
	$queryInsert = "insert into loan_application_status(loan_application_id, date,
					status, status_afis)
					values('$loan_application_id', '$now',
					'Pengajuan', 'PNJ')";
	mysqli_query($mysql_connection, $queryInsert);

	//=================================================================================================================================

	if($product == 'NEW CAR') $productName = 'Mobil Baru';
	if($product == 'USED CAR') $productName = 'Mobil Bekas';
	if($product == 'DANA ANDALANKU') $productName = 'Dana Andalanku';

	if($product == 'NEW CAR' || $product == 'USED CAR') {
		$mobil = '<span style="min-width:30%; display:inline-block">Mobil</span><span style="min-width:70%; display:inline">: '.$collateral.'</span><br>';
		$str_otr   = '<span style="min-width:30%; display:inline-block">Harga OTR</span><span style="min-width:70%; display:inline">: Rp '.number_format($otr,2,',','.').'</span><br>';
	}
	else if($product == 'DANA ANDALANKU')  {
		$mobil = '';
		$str_otr   = '<span style="min-width:30%; display:inline-block">Harga OTR</span><span style="min-width:70%; display:inline">: Rp '.number_format($otr,2,',','.').'</span><br>';
	}

	// Kirim email pemberitahuan kepada orang yang diajukan
	/*if($loan_application_referal == 'Y') {

		$customerName	= $customer_name;
		$customerEmail	= $email;
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Telah Diterima';
		$emailContent = 'Pengguna Yth, <br><br>';
		$emailContent = $emailContent.'Terima kasih telah menggunakan aplikasi Andalanku.<br>';
		$emailContent = $emailContent.'Kami telah menerima pengajuan kredit dari Anda dan akan segera memproses permohonan Anda.<br><br>';
		$emailContent = $emailContent.'Nomor tiket pengajuan kredit Anda adalah : <b>'.$ticket_number.'</b><br><br>';
		$emailContent = $emailContent.'Customer service kami akan menghubungi Anda untuk proses selanjutnya.<br><br>';
		$emailContent = $emailContent.'Produk : '.$productName.'<br>';
		$emailContent = $emailContent.$mobil;
		$emailContent = $emailContent.$str_otrs;
		$emailContent = $emailContent.'Tenor : '.$tenor.' bulan<br><br><br>';
		$emailContent = $emailContent."Terima kasih, <br><br>";
		$emailContent = $emailContent."Andalan Finance";

		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host       = $smtp_url;
		$mail->Port       = $smtp_port;
		$mail->SMTPAuth   = true;
		$mail->Username   = $smtp_username;
		$mail->Password   = $smtp_password;
		$mail->setFrom($app_email_from, $app_email_from_name);
		$mail->addAddress($customerEmail, $customerName);
		$mail->isHTML(true);
		$mail->Subject = $emailSubject;
		$mail->Body    = $emailContent;
		$mail->send();
	}*/

	// Kirim email pemberitahuan kepada user yang melakukan input
	/*$queryCheck = "select customer_name, email from customers where id_customer='$id_customer'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck	= mysqli_fetch_array($resultCheck);
	$customerName	= $dataCheck['customer_name'];
	$customerEmail	= $dataCheck['email'];
	$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Telah Diterima';
	$emailContent = 'Pengguna Yth, <br><br>';
	$emailContent = $emailContent.'Terima kasih telah menggunakan aplikasi Andalanku.<br>';
	$emailContent = $emailContent.'Kami telah menerima pengajuan kredit dari Anda dan akan segera memproses permohonan Anda.<br><br>';
	$emailContent = $emailContent.'Nomor tiket pengajuan kredit Anda adalah : <b>'.$ticket_number.'</b><br><br>';
	$emailContent = $emailContent.'Customer service kami akan menghubungi Anda untuk proses selanjutnya.<br><br>';
	$emailContent = $emailContent.'Produk : '.$productName.'<br>';
	$emailContent = $emailContent.$mobil;
	$emailContent = $emailContent.$str_otrs;
	$emailContent = $emailContent.'Tenor : '.$tenor.' bulan<br><br><br>';
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";

	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->Host       = $smtp_url;
	$mail->Port       = $smtp_port;
	$mail->SMTPAuth   = true;
	$mail->Username   = $smtp_username;
	$mail->Password   = $smtp_password;
	$mail->setFrom($app_email_from, $app_email_from_name);
	$mail->addAddress($customerEmail, $customerName);
	$mail->isHTML(true);
	$mail->Subject = $emailSubject;
	$mail->Body    = $emailContent;
	$mail->send();*/

	//=================================================================================================================================
	//=================================================================================================================================

	//==================== INBOX ====================
	$tab = "&nbsp;&nbsp;&nbsp;&nbsp;";
	$message_html = 'Dear Konsumen yang terhormat, <br><br>';
	$message_html .= 'Terima kasih telah menggunakan aplikasi Andalanku. ';
	$message_html .= 'Kami telah menerima pengajuan kredit dari Anda dan akan segera memproses permohonan Anda. <br>';
	$message_html .= '<span style="min-width:30%; display:inline-block">Produk</span>';
	$message_html .= '<span style="min-width:70%; display:inline">:' . $productName . '</span><br>';
	$message_html .= $mobil;
	$message_html .= $str_otr;
	$message_html .= '<span style="min-width:30%; display:inline-block">Tenor</span>';
	$message_html .= '<span style="min-width:70%; display:inline">:' . $tenor . ' bulan </span><br>';
	
	if($product == 'DANA ANDALANKU') {
		$message_html .= '<span style="min-width:30%; display:inline-block">Jenis Agunan</span>';
		$message_html .= '<span style="min-width:70%; display:inline">: BPKB </span><br>';
		$message_html .= '<span style="min-width:30%; display:inline-block">Jaminan</span>';
		$message_html .= '<span style="min-width:70%; display:inline">: ' . $collateral_car_name .' '. $collateral_year . '</span><br>';
	}
	else {
		$message_html .= '<span style="min-width:30%; display:inline-block">Asuransi </span>';
		$message_html .= '<span style="min-width:70%; display:inline">: ' . $insurance . '</span><br>';
		$message_html .= '<span style="min-width:30%; display:inline-block">Uang Muka </span>';
		$message_html .= '<span style="min-width:70%; display:inline">: Rp '.number_format($dp_amount,2,',','.').'</span><br>';
	}
	
	$content = array();
	
	$message = $message_html;
	$message = str_replace("<br>", "\r\n", $message);
	$message = str_replace('<span style="min-width:30%; display:inline-block">', " ", $message);
	$message = str_replace('<span style="min-width:70%; display:inline">', " ", $message);
	$message = str_replace('</span>', " ", $message);
	
	$content['message'] 		 = $message;
	$content['ticket_number'] 	 = $ticket_number;
	$content['status'] 			 = 'Pengajuan';
	$content['tanggal_pengajuan']= $now;
	$content = json_encode($content);

	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content, content_html)
					values('$id_customer', '$now', 'credit application', 'Pengajuan Kredit', '0',
					'$content', '$message_html')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================

	$query	= "select andalan_customer_id, phone_number, nip, npwp from customers where id_customer='$id_customer'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$andalan_customer_id 	= $data['andalan_customer_id'];
	$customer_nip  		 	= $data['nip'];
	$customer_npwp		 	= $data['npwp'];
	$customer_phone_number	= $data['phone_number'];

	//update tabel customer
	if($loan_application_referal == 'N') {

		if($andalan_customer_id == '') {
			$queryUpdate = "update customers set
							customer_name 		= '$customer_name', 
							place_of_birth 		= '$place_of_birth',
							date_of_birth  		= '$date_of_birth',
							id_number 			= '$id_number',
							mother_name 		= '$mother_name',
							home_phone_number	= '$home_phone_number',
							area_code			= '$area_code',
							occupation			= '$occupation_id',
							monthly_income		= '$monthly_income',
							additional_income	= '$additional_income',
							side_job			= '$side_job',
							gender				= '$gender' 
							where id_customer   = '$id_customer'";
		}
		else {
			$queryUpdate = "update customers set 
							mother_name 		= '$mother_name', 
							home_phone_number	= '$home_phone_number',
							area_code			= '$area_code',
							occupation			= '$occupation_id',
							monthly_income		= '$monthly_income',
							additional_income	= '$additional_income',
							side_job			= '$side_job' 
							where id_customer   = '$id_customer'";
		}
		mysqli_query($mysql_connection, $queryUpdate);
	}

	if($loan_application_referal == 'Y') {
		
		$imageFileName = '';
		if($bank_account_image <> '') { $imageFileName = base64_to_image($backend_folder, $customer_document_image_folder, $bank_account_image); }
		
		if($imageFileName <> '') {
			
			//hapus dulu gambar yang lama 
			$queryCheck  = "select image from customer_documents 
							where customer_id='$id_customer' and type='FOTO BUKU REKENING BANK'";
			$resultCheck = mysqli_query($mysql_connection, $queryCheck);
			$dataCheck	 = mysqli_fetch_array($resultCheck);
			$oldImage 	 = $dataCheck['image'];
			if($oldImage <> '') {
				@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
			}
		
			$queryDelete = " DELETE FROM customer_documents 
							 where customer_id='$id_customer' and type='FOTO BUKU REKENING BANK' ";
			mysqli_query($mysql_connection, $queryDelete);

			$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active, loan_application_id)
						VALUES('$id_customer', 'FOTO BUKU REKENING BANK', '$imageFileName', '1', $loan_application_id) ";
			mysqli_query($mysql_connection, $query);
		}
		
		$queryUpdate = "update customers set 
						customer_type = 'AGENT' ";
		
		if($bank_name <> '') $queryUpdate = $queryUpdate." , bank_name	= '$bank_name' ";
		if($bank_account_number <> '') $queryUpdate = $queryUpdate." , bank_account_number	= '$bank_account_number' ";
		if($bank_account_holder <> '') $queryUpdate = $queryUpdate." , bank_account_holder	= '$bank_account_holder' ";		
		if($imageFileName <> '') $queryUpdate = $queryUpdate." , bank_account_image	= '$imageFileName' ";
		
		$queryUpdate = $queryUpdate." where id_customer = '$id_customer' ";
		mysqli_query($mysql_connection, $queryUpdate);
	}

	//=== ALAMAT ======================================================================================================================
	//=================================================================================================================================

	if($loan_application_referal == 'N') {
		
		// Jika belum punya andalan customer id update semua alamat yang aktif sekarang
		// Jika sudah punya andalan customer id update alamat domisili dan office yang aktif sekarang
		
		if($andalan_customer_id == '') {
			
			//===== LEGAL =====
			$queryCheck  = "select address_id from address_customers 
							where user_id='$id_customer' and is_active='1' and address_type='LEGAL'";
			$resultCheck = mysqli_query($mysql_connection, $queryCheck);
			
			if(mysqli_num_rows($resultCheck) > 0) {

				$dataCheck = mysqli_fetch_array($resultCheck);
				$address_id= $dataCheck['address_id'];
			
				//update alamat yang sudah ada 
				$queryUpdate = "update address set 
								province_id='$province_id', 
								regency_id='$city_id',
								district_id='$district_id', 
								village_id='$village_id', 
								zip_code='$zip_code',
								address='$address', 
								rt='$rt', 
								rw='$rw' 
								where id='$address_id' ";
				mysqli_query($mysql_connection, $queryUpdate); 
			}
			else {
				
				//insert ke tabel address
				$queryInsert = "insert into address(province_id, regency_id,
								district_id, village_id, zip_code,
								address, rt, rw)
								values('$province_id', '$city_id',
								'$district_id', '$village_id', '$zip_code',
								'$address', '$rt', '$rw')";
				mysqli_query($mysql_connection, $queryInsert); 

				//ambil id yang terakhir
				$query = "select id from address where address='$address' 
						  and rt='$rt' and rw='$rw' 
						  and village_id='$village_id' and district_id='$district_id' 
						  and regency_id='$city_id' and province_id='$province_id' 
						  order by id DESC LIMIT 1";
				$result= mysqli_query($mysql_connection, $query);
				$data  = mysqli_fetch_array($result);
				$id_address = $data['id'];

				$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
							VALUES('LEGAL', '$id_customer', '$id_address', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
			
		//===== DOMICILE =====
		$queryCheck  = "select address_id from address_customers 
						where user_id='$id_customer' and is_active='1' and address_type='DOMICILE'";
		$resultCheck = mysqli_query($mysql_connection, $queryCheck);
		
		if(mysqli_num_rows($resultCheck) > 0) {

			$dataCheck = mysqli_fetch_array($resultCheck);
			$address_id= $dataCheck['address_id'];
		
			//update alamat yang sudah ada 
			$queryUpdate = "update address set 
							province_id='$province_id_domicile', 
							regency_id='$city_id_domicile',
							district_id='$district_id_domicile', 
							village_id='$village_id_domicile', 
							zip_code='$zip_code_domicile',
							address='$address_domicile', 
							rt='$rt_domicile', 
							rw='$rw_domicile' 
							where id='$address_id' ";
			mysqli_query($mysql_connection, $queryUpdate);
		}
		else {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id,
							district_id, village_id, zip_code,
							address, rt, rw)
							values('$province_id_domicile', '$city_id_domicile',
							'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile',
							'$address_domicile', '$rt_domicile', '$rw_domicile')";
			mysqli_query($mysql_connection, $queryInsert);

			//ambil id yang terakhir
			$query = "select id from address where address='$address_domicile' 
						  and rt='$rt_domicile' and rw='$rw_domicile' 
						  and village_id='$village_id_domicile' and district_id='$district_id_domicile' 
						  and regency_id='$city_id_domicile' and province_id='$province_id_domicile' 
						  order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];

			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('DOMICILE', '$id_customer', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		
		//===== OFFICE =====
		$queryCheck  = "select address_id from address_customers 
						where user_id='$id_customer' and is_active='1' and address_type='OFFICE'";
		$resultCheck = mysqli_query($mysql_connection, $queryCheck);
		
		if(mysqli_num_rows($resultCheck) > 0) {

			$dataCheck = mysqli_fetch_array($resultCheck);
			$address_id= $dataCheck['address_id'];
		
			//update alamat yang sudah ada 
			$queryUpdate = "update address set 
							province_id='$province_id_office', 
							regency_id='$city_id_office',
							district_id='$district_id_office', 
							village_id='$village_id_office', 
							zip_code='$zip_code_office',
							address='$address_office', 
							rt='$rt_office', 
							rw='$rw_office' 
							where id='$address_id' ";
			mysqli_query($mysql_connection, $queryUpdate);
		}
		else {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id,
							district_id, village_id, zip_code,
							address, rt, rw)
							values('$province_id_office', '$city_id_office',
							'$district_id_office', '$village_id_office', '$zip_code_office',
							'$address_office', '$rt_office', '$rw_office')";
			mysqli_query($mysql_connection, $queryInsert);

			//ambil id yang terakhir
			$query = "select id from address where address='$address_office' 
						  and rt='$rt_office' and rw='$rw_office' 
						  and village_id='$village_id_office' and district_id='$district_id_office' 
						  and regency_id='$city_id_office' and province_id='$province_id_office' 
						  order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];

			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('OFFICE', '$id_customer', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}	
	}

	//=== FOTO DOKUMEN ================================================================================================================
	//=================================================================================================================================

	//foto_ktp,foto_kk,foto_slip_gaji,foto_rekening_koran,foto_keuangan_lainnya
	if($loan_application_referal == 'N') {
		
		if(substr($foto_ktp,0,4) == 'http') {
			
			$imageFileName = substr($foto_ktp, strrpos($foto_ktp,"/")+1, strlen($foto_ktp)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
						
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KTP', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {
			
			if($foto_ktp <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_ktp); }

			if($imageFileName <> '') {
				
				//hapus dulu gambar yang lama 
				/*$queryCheck  = "select image from customer_documents 
								where customer_id='$id_customer' and type='FOTO KTP'";
				$resultCheck = mysqli_query($mysql_connection, $queryCheck);
				$dataCheck	 = mysqli_fetch_array($resultCheck);
				$oldImage 	 = $dataCheck['image'];
				if($oldImage <> '') {
					@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
				}
			
				$queryDelete = " DELETE FROM customer_documents 
								 where customer_id='$id_customer' and type='FOTO KTP' ";
				mysqli_query($mysql_connection, $queryDelete);*/

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KTP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
				
				copy($backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName);
							
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO KTP'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO KTP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}	

		$imageFileName = '';		
	
		// =====
		
		if(substr($foto_kk,0,4) == 'http') {
			
			$imageFileName = substr($foto_kk, strrpos($foto_kk,"/")+1, strlen($foto_kk)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
						
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KK', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {

			if($foto_kk <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_kk); }

			if($imageFileName <> '') {
				
				//hapus dulu gambar yang lama 
				/*$queryCheck  = "select image from customer_documents 
								where customer_id='$id_customer' and type='FOTO KK'";
				$resultCheck = mysqli_query($mysql_connection, $queryCheck);
				$dataCheck	 = mysqli_fetch_array($resultCheck);
				$oldImage 	 = $dataCheck['image'];
				if($oldImage <> '') {
					@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
				}
			
				$queryDelete = " DELETE FROM customer_documents 
								 where customer_id='$id_customer' and type='FOTO KK' ";
				mysqli_query($mysql_connection, $queryDelete);*/

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KK', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
				
				copy($backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName);
							
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO KK'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO KK', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
			
		$imageFileName = '';
		
		//=====
		
		if(substr($foto_slip_gaji,0,4) == 'http') {
			
			$imageFileName = substr($foto_slip_gaji, strrpos($foto_slip_gaji,"/")+1, strlen($foto_slip_gaji)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
						
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {

			if($foto_slip_gaji <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_slip_gaji); }

			if($imageFileName <> '') {
				
				//hapus dulu gambar yang lama 
				/*$queryCheck  = "select image from customer_documents 
								where customer_id='$id_customer' and type='FOTO SLIP GAJI'";
				$resultCheck = mysqli_query($mysql_connection, $queryCheck);
				$dataCheck	 = mysqli_fetch_array($resultCheck);
				$oldImage 	 = $dataCheck['image'];
				if($oldImage <> '') {
					@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
				}
			
				$queryDelete = " DELETE FROM customer_documents 
								 where customer_id='$id_customer' and type='FOTO SLIP GAJI' ";
				mysqli_query($mysql_connection, $queryDelete);*/

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
				
				copy($backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName);
							
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO SLIP GAJI'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';

		//=====
		
		if(substr($foto_rekening_koran,0,4) == 'http') {
			
			$imageFileName = substr($foto_rekening_koran, strrpos($foto_rekening_koran,"/")+1, strlen($foto_rekening_koran)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
						
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {

			if($foto_rekening_koran <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_rekening_koran); }

			if($imageFileName <> '') {
				
				//hapus dulu gambar yang lama 
				/*$queryCheck  = "select image from customer_documents 
								where customer_id='$id_customer' and type='FOTO REKENING KORAN'";
				$resultCheck = mysqli_query($mysql_connection, $queryCheck);
				$dataCheck	 = mysqli_fetch_array($resultCheck);
				$oldImage 	 = $dataCheck['image'];
				if($oldImage <> '') {
					@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
				}
			
				$queryDelete = " DELETE FROM customer_documents 
								 where customer_id='$id_customer' and type='FOTO REKENING KORAN' ";
				mysqli_query($mysql_connection, $queryDelete);*/

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
				
				copy($backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName);
							
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO REKENING KORAN'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';

		//=====

		if(substr($foto_keuangan_lainnya,0,4) == 'http') {
			
			$imageFileName = substr($foto_keuangan_lainnya, strrpos($foto_keuangan_lainnya,"/")+1, strlen($foto_keuangan_lainnya)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
						
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {
			
			if($foto_keuangan_lainnya <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_keuangan_lainnya); }

			if($imageFileName <> '') {
				
				//hapus dulu gambar yang lama 
				/*$queryCheck  = "select image from customer_documents 
								where customer_id='$id_customer' and type='FOTO KEUANGAN LAINNYA'";
				$resultCheck = mysqli_query($mysql_connection, $queryCheck);
				$dataCheck	 = mysqli_fetch_array($resultCheck);
				$oldImage 	 = $dataCheck['image'];
				if($oldImage <> '') {
					@unlink($backend_folder.'/'.$customer_document_image_folder.'/'.$oldImage);
				}
			
				$queryDelete = " DELETE FROM customer_documents 
								 where customer_id='$id_customer' and type='FOTO KEUANGAN LAINNYA' ";
				mysqli_query($mysql_connection, $queryDelete);*/

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
				
				copy($backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName);
							
				//update yang lama jadi inactive
				$query = "update customer_documents set is_active='0' 
						  where customer_id='$id_customer' 
						  and type='FOTO KEUANGAN LAINNYA'";
				$result= mysqli_query($mysql_connection, $query);
				
				$query = "	INSERT INTO customer_documents(customer_id, type, image, is_active)
							VALUES('$id_customer', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';
	} 
	else if($loan_application_referal == 'Y') {
		
		if(substr($foto_ktp,0,4) == 'http') {
			
			$imageFileName = substr($foto_ktp, strrpos($foto_ktp,"/")+1, strlen($foto_ktp)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
			
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KTP', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {
		
			if($foto_ktp <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_ktp); }

			if($imageFileName <> '') {

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KTP', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';
			
		// =====

		if(substr($foto_kk,0,4) == 'http') {
			
			$imageFileName = substr($foto_kk, strrpos($foto_kk,"/")+1, strlen($foto_kk)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
			
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KK', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {
			
			if($foto_kk <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_kk); }

			if($imageFileName <> '') {

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KK', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';

		//=====

		if(substr($foto_slip_gaji,0,4) == 'http') {
			
			$imageFileName = substr($foto_slip_gaji, strrpos($foto_slip_gaji,"/")+1, strlen($foto_slip_gaji)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
			
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {
			
			if($foto_slip_gaji <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_slip_gaji); }

			if($imageFileName <> '') {

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';

		//=====
		
		if(substr($foto_rekening_koran,0,4) == 'http') {
			
			$imageFileName = substr($foto_rekening_koran, strrpos($foto_rekening_koran,"/")+1, strlen($foto_rekening_koran)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
			
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {

			if($foto_rekening_koran <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_rekening_koran); }

			if($imageFileName <> '') {

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';

		//=====
		
		if(substr($foto_keuangan_lainnya,0,4) == 'http') {
			
			$imageFileName = substr($foto_keuangan_lainnya, strrpos($foto_keuangan_lainnya,"/")+1, strlen($foto_keuangan_lainnya)-1);
			
			copy($backend_folder.'/'.$customer_document_image_folder.'/'.$imageFileName, $backend_folder.'/'.$loan_application_document_image_folder.'/'.$imageFileName);
			
			$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
						VALUES('$loan_application_id', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
			mysqli_query($mysql_connection, $query);
		}
		else {

			if($foto_keuangan_lainnya <> '') { $imageFileName = base64_to_image($backend_folder, $loan_application_document_image_folder, $foto_keuangan_lainnya); }

			if($imageFileName <> '') {

				$query = "	INSERT INTO loan_application_documents(loan_application_id, type, image, is_active)
							VALUES('$loan_application_id', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
		
		$imageFileName = '';
	}

	//==================================== AFIS CALL ====================================

	//$query = "select customer_name, email
	//		  from customers where id_customer='$id_customer'";
	//$result= mysqli_query($mysql_connection, $query);
	//$data  = mysqli_fetch_array($result);
	//$customer_name	= $data['customer_name'];
	//$customer_email	= $data['email'];

	$afis_api_url	= $afis_api_url.'/andalanku/PengajuanKredit';

	$gender = $gender == 'M' ? 'L' : $gender;
    $gender = $gender == 'F' ? 'P' : $gender;

	$body_param		= array();
	$body_param['TicketNo']			= $ticket_number;
	$body_param['NamaProspect']		= $customer_name;
	$body_param['Email']			= $email;
	$body_param['Phone']			= $phone_number;

	$body_param['NoKtp']			= $id_number;
	$body_param['TempatLahir']		= $place_of_birth;
	$body_param['TanggalLahir']		= $date_of_birth;
	$body_param['JenisKelamin']		= $gender;
	$body_param['NamaIbuKandung']	= $mother_name;
	$body_param['TahunKendaraan']	= $car_year;
	$body_param['Kendaraan']		= $asset_code;
	$body_param['RegionalMRP']		= '901';
	$body_param['Tenor']            = $tenor;
	$body_param['OTR']              = $otr;
	$body_param['RateDP']           = $dp_percent;
	$body_param['Asuransi']         = $insurance;

	$query	= "select name from occupations where id='$occupation_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$occupation_name = $data['name'];

	if($occupation_name=='Karyawan Swasta') 			$occupation_name='SWASTA';
	else if($occupation_name=='Pegawai Negeri Sipil') 	$occupation_name='PNS';
	else if($occupation_name=='Anggota Polri') 			$occupation_name='POLRI';
	else if($occupation_name=='Wirausaha') 				$occupation_name='WIRA';
	else if($occupation_name=='Profesional') 			$occupation_name='PROF';
	else if($occupation_name=='Pegawai BUMD') 			$occupation_name='BUMD';
	else if($occupation_name=='Pegawai BUMN') 			$occupation_name='BUMN';
	else if($occupation_name=='Lain-lain') 				$occupation_name='LL';
	else if($occupation_name=='Anggota TNI') 			$occupation_name='TNI';

	$body_param['WilayahAsuransi']		= $region;
	$body_param['NomorTelprumah']		= $home_phone_number;
	$body_param['AreaTelprumah']		= $area_code;
	$body_param['Pekerjaan']			= $occupation_name;
	$body_param['PengasilanPerBulan']	= $monthly_income;
	$body_param['PenghasilanTambahan']	= $additional_income;
	$body_param['SumberPenghasilan']	= $side_job;
	if($loan_application_referal == 'N') {
		$body_param['IsReferal']	= false;
		$body_param['Agentcode']	= "-";
		$body_param['Agentname']	= "-";
	}
	if($loan_application_referal == 'Y') {
		$body_param['IsReferal']	= true;
		$query	= "select customer_name, agent_code from customers where id_customer='$id_customer'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$agentName = $data['customer_name'];
		$agent_code = $data['agent_code'];
		$body_param['Agentcode']	= "-";
		$body_param['Agentname']	= $agentName;
		if ($agent_code != '') {
			$body_param['Agentcode'] = $agent_code;
		}
			
	}

	$query	= "select name from villages where id='$village_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$village_name = $data['name'];

	$query	= "select name from districts where id='$district_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$district_name = $data['name'];

	$query	= "select name from regencies where id='$city_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$city_name = $data['name'];

	$query	= "select name from provinces where id='$province_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$province_name = $data['name'];

	$arrAlamatKtp					= array();
	$arrAlamatKtp['Alamat']			= $address;
	$arrAlamatKtp['Rt']				= $rt;
	$arrAlamatKtp['Rw']				= $rw;
	$arrAlamatKtp['Kelurahan']		= $village_name;
	$arrAlamatKtp['Kecamatan']		= $district_name;
	$arrAlamatKtp['Kota']			= $city_name;
	$arrAlamatKtp['Provinsi']		= $province_name;
	$arrAlamatKtp['Kodepos']		= substr($zip_code,0, 5);
	$body_param['AlamatKtp']		= $arrAlamatKtp;

	$query	= "select name from villages where id='$village_id_domicile'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$village_name = $data['name'];

	$query	= "select name from districts where id='$district_id_domicile'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$district_name = $data['name'];

	$query	= "select name from regencies where id='$city_id_domicile'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$city_name = $data['name'];

	$query	= "select name from provinces where id='$province_id_domicile'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$province_name = $data['name'];

	$arrAlamatDomisili				= array();
	$arrAlamatDomisili['Alamat']	= $address_domicile;
	$arrAlamatDomisili['Rt']		= $rt_domicile;
	$arrAlamatDomisili['Rw']		= $rw_domicile;
	$arrAlamatDomisili['Kelurahan']	= $village_name;
	$arrAlamatDomisili['Kecamatan']	= $district_name;
	$arrAlamatDomisili['Kota']		= $city_name;
	$arrAlamatDomisili['City']		= $city_name;
	$arrAlamatDomisili['Provinsi']	= $province_name;
	$arrAlamatDomisili['Kodepos']	= substr($zip_code_domicile, 0, 5);
	$body_param['AlamatDomisili']	= $arrAlamatDomisili;
	
	if($arrAlamatDomisili['Alamat'] == '') {
		$body_param['AlamatDomisili'] = $arrAlamatKtp;
	}		

	$query	= "select name from villages where id='$village_id_office'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$village_name = $data['name'];

	$query	= "select name from districts where id='$district_id_office'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$district_name = $data['name'];

	$query	= "select name from regencies where id='$city_id_office'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$city_name = $data['name'];

	$query	= "select name from provinces where id='$province_id_office'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$province_name = $data['name'];

	$arrAlamatKantor				= array();
	$arrAlamatKantor['Alamat']		= $address_office;
	$arrAlamatKantor['Rt']			= $rt_office;
	$arrAlamatKantor['Rw']			= $rw_office;
	$arrAlamatKantor['Kelurahan']	= $village_name;
	$arrAlamatKantor['Kecamatan']	= $district_name;
	$arrAlamatKantor['Kota']		= $city_name;
	$arrAlamatKantor['Provinsi']	= $province_name;
	$arrAlamatKantor['Kodepos']		= substr($zip_code_office, 0, 5);
	$body_param['AlamatKantor']		= $arrAlamatKantor;
	
	if($loan_application_referal == 'Y') {
		$body_param['AgentBankID']		= '014';
		$body_param['AgentAccountNo']	= $bank_account_number;
		$body_param['AgentAccountName']	= $bank_account_holder;
		$body_param['AlamatAgent']		= $arrAlamatKtp;
		$body_param['AgentNoAreaTelp1']	= substr($customer_phone_number,0,4);
		$body_param['AgentNoTelp1']		= substr($customer_phone_number,4);
		$body_param['AgentNoTelp2']		= $home_phone_number;
		$body_param['AgentNPWP']		= $customer_npwp;
		$body_param['AgentNoKTP']		= $id_number;
		$body_param['AgentNIK']			= $customer_nip;
	} else {
		$body_param['AgentBankID']		= '-';
		$body_param['AgentAccountNo']	= '-';
		$body_param['AgentAccountName']	= '-';
		$arrAlamatKtpEmpty				= array();
		$arrAlamatKtpEmpty['Alamat']	= '-';
		$arrAlamatKtpEmpty['Rt']		= '-';
		$arrAlamatKtpEmpty['Rw']		= '-';
		$arrAlamatKtpEmpty['Kelurahan']	= '-';
		$arrAlamatKtpEmpty['Kecamatan']	= '-';
		$arrAlamatKtpEmpty['Kota']		= '-';
		$arrAlamatKtpEmpty['Provinsi']	= '-';
		$arrAlamatKtpEmpty['Kodepos']	= '-';
		$body_param['AlamatAgent']		= $arrAlamatKtpEmpty;		
		$body_param['AgentNoAreaTelp1']	= '-';
		$body_param['AgentNoTelp1']		= '-';
		$body_param['AgentNoTelp2']		= '-';
		$body_param['AgentNPWP']		= '-';
		$body_param['AgentNoKTP']		= '-';
		$body_param['AgentNIK']			= '-';
	}
	
	//var_dump($body_param); die();
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param, true));
	$api_response['afis_response'] 	= $afis_response;
	//var_dump($afis_response->StatusCode);
	
	$jsonData = json_encode($body_param);
	$queryUpdateApplication = "UPDATE loan_applications SET json_data = '$jsonData'
												WHERE ticket_number = '$ticket_number'";
	mysqli_query($mysql_connection, $queryUpdateApplication);
	
	if ($afis_response->StatusCode == 500) {
		//var_dump($ticket_number);die();
		
		$queryUpdateApplication = "UPDATE loan_applications SET afis_success = 0
												WHERE ticket_number = '$ticket_number'";
		//var_dump($queryUpdateApplication);die();
		mysqli_query($mysql_connection, $queryUpdateApplication);
		//var_dump($queryUpdateApplication);die();
		
	}
	else {
		$queryUpdateApplication = "UPDATE loan_applications SET afis_success = 1
												WHERE ticket_number = '$ticket_number'";
		//var_dump($queryUpdateApplication);die();
		mysqli_query($mysql_connection, $queryUpdateApplication);
		
		
		//ambil agent code dari AFIS jika customer belum punya agent_code  
		if($loan_application_referal == 'Y' && $agent_code_code == '') {
			
			$agent_code = $afis_response->Response->Data[0]->Agentcode;
			
			if($agent_code != '') {
				
				$queryUpdateCustomer = "UPDATE customers SET agent_code = '$agent_code', customer_type='AGENT' 
										WHERE id_customer = '$id_customer'";
				mysqli_query($mysql_connection, $queryUpdateCustomer);
			}
			
		}
		
	}
	//==================================== AFIS CALL ====================================

	$api_response['message'] 		= 'Loan application successful';
	$api_response['ticket_number'] 	= $ticket_number;

	echo json_encode($api_response);
	exit;
?>
