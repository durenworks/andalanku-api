<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "common_vars.php";
	include "validate_token.php";
	
	$email = sanitize_sql_string($_POST['email']);
	
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email format';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select customer_name, id_customer from customers where email='$email'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid email address';
		
		echo json_encode($api_response);
		exit;
	}
	
	$dataCheck 		= mysqli_fetch_array($resultCheck);
	$customerName	= $dataCheck['customer_name'];
	$customerEmail	= $email;
	
	$new_password   = md5(microtime()." Andalanku ".$email.rand(0000,9999));
	$new_password   = strtoupper(substr($new_password,0,8));
	
	$new_password_enc = md5($new_password);
	
	$queryUpdate  = "update customers set password='$new_password_enc' where email='$email'";
	$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	
	//------- Kirim email password baru -------
	$linkURL 	  = $backend_url."/reset_password?c=".$password_code;
	$emailSubject = 'Permintaan Reset Password Andalanku';
	$emailContent = "Pengguna Yth, <br><br>";
	$emailContent = $emailContent."Anda telah melakukan permintaan reset password pada aplikasi Andalanku.<br>";
	$emailContent = $emailContent."Password baru Anda adalah : ".$new_password."<br><br>";
	$emailContent = $emailContent."<br><br>";
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";
	
	require 'plugins/mailjet/vendor/autoload.php';
	use \Mailjet\Resources;
	
	$mj = new \Mailjet\Client($mailjet_api_key, $mailjet_api_secret, true, ['version' => 'v3.1']);
	$body = [
		'Messages' => [
			[
				'From' => [
					'Email' => "info@andalanku.id",
					'Name'  => "Info Andalanku"
				],
				'To' => [
					[
						'Email' => $customerEmail,
						'Name'  => $customerName
					]
				],
				'Subject'  => $emailSubject,
				'TextPart' => $emailContent,
				'HTMLPart' => $emailContent
			]
		]
	];
	$response = $mj->post(Resources::$Email, ['body' => $body]);
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Password reset successful. Please check your email inbox.';
	
	echo json_encode($api_response);
	exit;
?>