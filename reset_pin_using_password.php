<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer 	= sanitize_int($_POST['id_customer']);
	$email 			= sanitize_sql_string($_POST['email']);
	$password		= sanitize_sql_string(trim($_POST['password']));
	$pinNumber		= sanitize_sql_string(trim($_POST['pin_number']));
	$password 		= md5($password);
	
	$queryCheck = "select id_customer from customers where id_customer='$id_customer' and password='$password'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Wrong Password';
		
		echo json_encode($api_response);
		exit;
	}
	if(strlen($pinNumber) <> 6) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid PIN. PIN number length must be 6 characters';
		
		echo json_encode($api_response);
		exit;
	}
	
	//$new_pin_number = rand(000000,999999); 
	
	$queryUpdate  = "update customers set pin='$pinNumber' where id_customer='$id_customer'";
	$resultUpdate = mysqli_query($mysql_connection, $queryUpdate);
	
	//------- TO-DO : Kirim email reset PIN -------
	
	//------- Kirim email password baru -------
	/*
	$linkURL 	  = $backend_url."/reset_password?c=".$password_code;
	$emailSubject = 'Permintaan Reset PIN Andalanku';
	$emailContent = "Pengguna Yth, <br><br>";
	$emailContent = $emailContent."Anda telah melakukan permintaan reset PIN pada aplikasi Andalanku.<br>";
	$emailContent = $emailContent."PIN baru Anda adalah : ".$new_pin_number."<br><br>";
	$emailContent = $emailContent."<br><br>";
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";
	
	include "mail_engine.php";
	*/
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Change PIN successful';
	//$api_response['new_pin_number'] = $new_pin_number;
	
	echo json_encode($api_response);
	exit;
?>