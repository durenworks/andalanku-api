<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";

	$nip = sanitize_sql_string($_REQUEST["nip"]);
	
	if($nip=='0' || $nip=='') {
		
		$api_response['status']			= 'failed';
		$api_response['message'] 		= 'Input not complete';
		$api_response['employeenik'] 	= '';
		$api_response['employeename'] 	= '';
		
		echo json_encode($api_response);
		exit;
	}

	$afis_api_url  = $afis_api_url.'/Andalanku/ValidasiUplineAgent/'.$nip;
    $afis_response = json_decode(afis_call($afis_api_url, 'GET'));
    $responseArray = $afis_response->Response->Data[0];

	if($responseArray->errorMessage == '') {
		$api_response['status'] 		= 'success';
		$api_response['message'] 		= 'Employee found';
		$api_response['employeenik'] 	= $responseArray->employeenik;
		$api_response['employeename'] 	= $responseArray->employeename;
	}
	else {
		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Employee not found';
		$api_response['employeenik'] 	= '';
		$api_response['employeename'] 	= '';
	}

	echo json_encode($api_response);
	exit;
?>
