<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function base64_to_image($backend_folder, $customer_document_image_folder, $base64_string) {

		$extension = 'png';
		$folder = $backend_folder."/".$customer_document_image_folder;

		$stringrand = md5(microtime());
		$random = substr($stringrand, 0, 16);
		$newnamefile = 'andalanku_' . $random . '.'.$extension;

		while(file_exists($folder . '/' . $newnamefile)) {
			$stringrand = md5(microtime());
			$random = substr($stringrand, 0, 16);
			$newnamefile = 'andalanku_' . $random . '.'.$extension;
		}

		$ifp = fopen( $folder . '/' . $newnamefile, 'wb' );

		if(!fwrite( $ifp, base64_decode($base64_string))) {
			$newnamefile = '';
		}

		fclose( $ifp );

		return $newnamefile;
	}
	
	// ====== UPPERCASE SEMUA REQUEST ======
	$return_array = array();
	foreach($_POST as $postKey => $postVar){
		
		if(strpos($postKey, 'foto_') !== false)
			$return_array[$postKey] = $postVar;
		else 
			$return_array[$postKey] = strtoupper($postVar);
    }
	$_POST = $return_array;
	// =====================================
	
	$id_customer 		= sanitize_int($_POST["id_customer"]);
	$occupation_id		= sanitize_int($_POST['occupation_id']);
	$monthly_income		= sanitize_int($_POST['monthly_income']);
	$additional_income	= sanitize_int($_POST['additional_income']);
	$side_job			= sanitize_sql_string(trim($_POST['side_job']));
	$andalan_branch_id	= sanitize_int($_POST['andalan_branch_id']);
	$survey_date		= sanitize_sql_string(trim($_POST['survey_date']));
	$survey_time		= sanitize_sql_string(trim($_POST['survey_time']));
	$survey_date		= $survey_date." ".$survey_time;
	
	$total_income		= $monthly_income + $additional_income;
	$plafond			= (($total_income / 3) * 36 ) * 0.75;
	
	if($id_customer == '0') {
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Input not complete';
		echo json_encode($api_response);
		exit;
	}
	
	//---ALAMAT
	$province_id		= sanitize_int($_POST['province_id']);
	if ($province_id == 0) {
		$province_id = sanitize_sql_string($_POST['province_id']);
		$query = "select id from provinces where name='$province_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$province_id = $data['id'];
	}
	$city_id			= sanitize_int($_POST['city_id']);

	if ($city_id == 0) {
		$city_id = sanitize_sql_string($_POST['city_id']);
		$query = "select id from regencies where name='$city_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id = $data['id'];
	}

	$district_id		= sanitize_int($_POST['district_id']);
	if ($district_id == 0) {
		$district_id = sanitize_sql_string($_POST['district_id']);
		$query = "select id from districts where name='$district_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$district_id = $data['id'];
	}
	
	$village_id			= sanitize_sql_string(trim($_POST['village_id']));
	if ($village_id == 0) {
		$village_id = sanitize_sql_string($_POST['village_id']);
		$query = "select id from villages where name='$village_id'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$village_id = $data['id'];
	}
	
	$address			= sanitize_sql_string(trim($_POST['address']));
	$rt 				= sanitize_sql_string(trim($_POST['rt']));
	$rw					= sanitize_sql_string(trim($_POST['rw']));
	$zip_code			= sanitize_sql_string(trim($_POST['zip_code']));

	$province_id_domicile		= sanitize_int($_POST['province_id_domicile']);
	$city_id_domicile			= sanitize_int($_POST['city_id_domicile']);

	if ($city_id_domicile == 0) {
		$city_id_domicile = sanitize_sql_string($_POST['city_id_domicile']);
		$query = "select id from regencies where name='$city_id_domicile'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_domicile = $data['id'];
	}
	
	$district_id_domicile		= sanitize_int($_POST['district_id_domicile']);
	$village_id_domicile		= sanitize_sql_string(trim($_POST['village_id_domicile']));
	$address_domicile			= sanitize_sql_string(trim($_POST['address_domicile']));
	$rt_domicile 				= sanitize_sql_string(trim($_POST['rt_domicile']));
	$rw_domicile				= sanitize_sql_string(trim($_POST['rw_domicile']));
	$zip_code_domicile			= sanitize_sql_string(trim($_POST['zip_code_domicile']));

	if ($city_id_domicile == '') $city_id_domicile = 0;
	if ($district_id_domicile == '') $district_id_domicile = 0;
	if ($village_id_domicile ==  '') $village_id_domicile = 0;
	if ($province_id_domicile == '') $province_id_domicile = 0;

	$province_id_office		= sanitize_int($_POST['province_id_office']);
	$city_id_office			= sanitize_int($_POST['city_id_office']);

	if ($city_id_office == 0) {
		$city_id_office = sanitize_sql_string($_POST['city_id_office']);
		$query = "select id from regencies where name='$city_id_office'";
		$result= mysqli_query($mysql_connection, $query);
		$data  = mysqli_fetch_array($result);
		$city_id_office = $data['id'];
	}
	
	$district_id_office		= sanitize_int($_POST['district_id_office']);
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$village_id_office		= sanitize_sql_string(trim($_POST['village_id_office']));
	$address_office			= sanitize_sql_string(trim($_POST['address_office']));
	$rt_office 				= sanitize_sql_string(trim($_POST['rt_office']));
	$rw_office				= sanitize_sql_string(trim($_POST['rw_office']));
	$zip_code_office		= sanitize_sql_string(trim($_POST['zip_code_office']));
	
	//customer_documents
	$foto_ktp				= $_POST['foto_ktp'];
	$foto_kk				= $_POST['foto_kk'];
	$foto_slip_gaji			= $_POST['foto_slip_gaji'];
	$foto_rekening_koran	= $_POST['foto_rekening_koran'];
	$foto_keuangan_lainnya	= $_POST['foto_keuangan_lainnya'];

	if ($province_id_office == '') $province_id_office= 0;
	if ($city_id_office == '') $city_id_office =0;
	if ($district_id_office == '') $district_id_office = 0;
	if ($village_id_office == '') $village_id_office = 0;
	
	//=== VALIDASI ALAMAT ===
	if($address == '') {
		
		$queryAddress = "select b.* 
						from address_customers a 
						left join address b on b.id=a.address_id 
						where user_id='$id_customer' 
						and address_type='LEGAL' and is_active='1'"; 
		$resultAddress= mysqli_query($mysql_connection, $queryAddress);
		if(mysqli_num_rows($resultAddress) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Legal address must be filled';
			
			echo json_encode($api_response);
			exit;
		}
	}
	if($address_domicile == '') {
		
		$queryAddress = "select b.* 
						from address_customers a 
						left join address b on b.id=a.address_id 
						where user_id='$id_customer' 
						and address_type='DOMICILE' and is_active='1'"; 
		$resultAddress= mysqli_query($mysql_connection, $queryAddress);
		if(mysqli_num_rows($resultAddress) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Domicile address must be filled';
			
			echo json_encode($api_response);
			exit;
		}
	}
	if($address_office == '') {
		
		$queryAddress = "select b.* 
						from address_customers a 
						left join address b on b.id=a.address_id 
						where user_id='$id_customer' 
						and address_type='OFFICE' and is_active='1'"; 
		$resultAddress= mysqli_query($mysql_connection, $queryAddress);
		if(mysqli_num_rows($resultAddress) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Office address must be filled';
			
			echo json_encode($api_response);
			exit;
		}
	}
	//=======================
	
	$ticket_number = 'PFA'.rand(0000000, 9999999);
		
	$ticket_number_exist = true;
	
	while($ticket_number_exist) {
		
		$query = "select id from plafond_applications where ticket_number='$ticket_number'";
		$result= mysqli_query($mysql_connection, $query);
		if(mysqli_num_rows($result) == 0) {
			$ticket_number_exist = false;
		}
		else {
			$ticket_number = 'PFS'.rand(0000000, 9999999);
		}
	}
	
	$now = date("Y-m-d H:i:s");
	
	$query = "insert into plafond_applications(ticket_number, 
			  customer_id, application_date, 
			  occupation_id, monthly_income, additional_income, 
			  side_job, plafond_amount, status, 
			  andalan_branch_id, survey_date)
			  values ('$ticket_number', 
			  '$id_customer', '$now',  
			  '$occupation_id', '$monthly_income', '$additional_income', 
			  '$side_job', '$plafond', 'SUBMITTED', 
			  '$andalan_branch_id', '$survey_date')";
	$result= mysqli_query($mysql_connection, $query); 
	
	//ambil id terakhir 
	$queryID = "select id from plafond_applications where ticket_number='$ticket_number'";
	$resultID= mysqli_query($mysql_connection, $queryID);
	$dataID	 = mysqli_fetch_array($resultID);
	$new_plafond_application_id = $dataID['id'];
	
	//update tabel customer 
	$query = "update customers set occupation='$occupation_id', 
			  monthly_income='$monthly_income', additional_income='$additional_income', 
			  side_job='$side_job' where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	
	//=== ALAMAT ======================================================================================================================
	//=================================================================================================================================
	
	// Jika belum punya andalan customer id update semua alamat yang aktif sekarang
	// Jika sudah punya andalan customer id update alamat domisili dan office yang aktif sekarang
	
	$query	= "select andalan_customer_id from customers where id_customer='$id_customer'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$andalan_customer_id 	= $data['andalan_customer_id'];
	
	if($andalan_customer_id == '') {
			
		//===== LEGAL =====
		if($address <> '') {
			
			$queryCheck  = "select address_id from address_customers 
							where user_id='$id_customer' and is_active='1' and address_type='LEGAL'";
			$resultCheck = mysqli_query($mysql_connection, $queryCheck);
			
			if(mysqli_num_rows($resultCheck) > 0) {

				$dataCheck = mysqli_fetch_array($resultCheck);
				$address_id= $dataCheck['address_id'];
			
				//update alamat yang sudah ada 
				$queryUpdate = "update address set 
								province_id='$province_id', 
								regency_id='$city_id',
								district_id='$district_id', 
								village_id='$village_id', 
								zip_code='$zip_code',
								address='$address', 
								rt='$rt', 
								rw='$rw' 
								where id='$address_id' ";
				mysqli_query($mysql_connection, $queryUpdate); 
			}
			else {
				
				//insert ke tabel address
				$queryInsert = "insert into address(province_id, regency_id,
								district_id, village_id, zip_code,
								address, rt, rw)
								values('$province_id', '$city_id',
								'$district_id', '$village_id', '$zip_code',
								'$address', '$rt', '$rw')";
				mysqli_query($mysql_connection, $queryInsert); 

				//ambil id yang terakhir
				$query = "select id from address where address='$address' 
						  and rt='$rt' and rw='$rw' 
						  and village_id='$village_id' and district_id='$district_id' 
						  and regency_id='$city_id' and province_id='$province_id' 
						  order by id DESC LIMIT 1";
				$result= mysqli_query($mysql_connection, $query);
				$data  = mysqli_fetch_array($result);
				$id_address = $data['id'];

				$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
							VALUES('LEGAL', '$id_customer', '$id_address', '1') ";
				mysqli_query($mysql_connection, $query);
			}
		}
	}
		
	//===== DOMICILE =====
	if($address_domicile <> '') {
		
		$queryCheck  = "select address_id from address_customers 
						where user_id='$id_customer' and is_active='1' and address_type='DOMICILE'";
		$resultCheck = mysqli_query($mysql_connection, $queryCheck);
		
		if(mysqli_num_rows($resultCheck) > 0) {

			$dataCheck = mysqli_fetch_array($resultCheck);
			$address_id= $dataCheck['address_id'];
		
			//update alamat yang sudah ada 
			$queryUpdate = "update address set 
							province_id='$province_id_domicile', 
							regency_id='$city_id_domicile',
							district_id='$district_id_domicile', 
							village_id='$village_id_domicile', 
							zip_code='$zip_code_domicile',
							address='$address_domicile', 
							rt='$rt_domicile', 
							rw='$rw_domicile' 
							where id='$address_id' ";
			mysqli_query($mysql_connection, $queryUpdate);
		}
		else {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id,
							district_id, village_id, zip_code,
							address, rt, rw)
							values('$province_id_domicile', '$city_id_domicile',
							'$district_id_domicile', '$village_id_domicile', '$zip_code_domicile',
							'$address_domicile', '$rt_domicile', '$rw_domicile')";
			mysqli_query($mysql_connection, $queryInsert);

			//ambil id yang terakhir
			$query = "select id from address where address='$address_domicile' 
						  and rt='$rt_domicile' and rw='$rw_domicile' 
						  and village_id='$village_id_domicile' and district_id='$district_id_domicile' 
						  and regency_id='$city_id_domicile' and province_id='$province_id_domicile' 
						  order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];

			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('DOMICILE', '$id_customer', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}
	}
	
	//===== OFFICE =====
	if($address_office <> '') {
		
		$queryCheck  = "select address_id from address_customers 
						where user_id='$id_customer' and is_active='1' and address_type='OFFICE'";
		$resultCheck = mysqli_query($mysql_connection, $queryCheck);
		
		if(mysqli_num_rows($resultCheck) > 0) {

			$dataCheck = mysqli_fetch_array($resultCheck);
			$address_id= $dataCheck['address_id'];
		
			//update alamat yang sudah ada 
			$queryUpdate = "update address set 
							province_id='$province_id_office', 
							regency_id='$city_id_office',
							district_id='$district_id_office', 
							village_id='$village_id_office', 
							zip_code='$zip_code_office',
							address='$address_office', 
							rt='$rt_office', 
							rw='$rw_office' 
							where id='$address_id' ";
			mysqli_query($mysql_connection, $queryUpdate);
		}
		else {
			
			//insert ke tabel address
			$queryInsert = "insert into address(province_id, regency_id,
							district_id, village_id, zip_code,
							address, rt, rw)
							values('$province_id_office', '$city_id_office',
							'$district_id_office', '$village_id_office', '$zip_code_office',
							'$address_office', '$rt_office', '$rw_office')";
			mysqli_query($mysql_connection, $queryInsert);

			//ambil id yang terakhir
			$query = "select id from address where address='$address_office' 
						  and rt='$rt_office' and rw='$rw_office' 
						  and village_id='$village_id_office' and district_id='$district_id_office' 
						  and regency_id='$city_id_office' and province_id='$province_id_office' 
						  order by id DESC LIMIT 1";
			$result= mysqli_query($mysql_connection, $query);
			$data  = mysqli_fetch_array($result);
			$id_address = $data['id'];

			$query = "	INSERT INTO address_customers(address_type, user_id, address_id, is_active)
						VALUES('OFFICE', '$id_customer', '$id_address', '1') ";
			mysqli_query($mysql_connection, $query);
		}	
	}
	
	//=== FOTO DOKUMEN ================================================================================================================
	//=================================================================================================================================	
		
	if($foto_ktp <> '') { $imageFileName = base64_to_image($backend_folder, $plafond_application_document_image_folder, $foto_ktp); }

	if($imageFileName <> '') {

		$query = "	INSERT INTO plafond_application_documents(plafond_application_id, type, image, is_active)
					VALUES('$new_plafond_application_id', 'FOTO KTP', '$imageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
	}
	
	$imageFileName = '';

	// =====

	if($foto_kk <> '') { $imageFileName = base64_to_image($backend_folder, $plafond_application_document_image_folder, $foto_kk); }

	if($imageFileName <> '') {
		
		$query = "	INSERT INTO plafond_application_documents(plafond_application_id, type, image, is_active)
					VALUES('$new_plafond_application_id', 'FOTO KK', '$imageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
	}

	$imageFileName = '';
	
	//=====

	if($foto_slip_gaji <> '') { $imageFileName = base64_to_image($backend_folder, $plafond_application_document_image_folder, $foto_slip_gaji); }

	if($imageFileName <> '') {
		
		$query = "	INSERT INTO plafond_application_documents(plafond_application_id, type, image, is_active)
					VALUES('$new_plafond_application_id', 'FOTO SLIP GAJI', '$imageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
	}
	
	$imageFileName = '';

	//=====

	if($foto_rekening_koran <> '') { $imageFileName = base64_to_image($backend_folder, $plafond_application_document_image_folder, $foto_rekening_koran); }

	if($imageFileName <> '') {
		
		$query = "	INSERT INTO plafond_application_documents(plafond_application_id, type, image, is_active)
					VALUES('$new_plafond_application_id', 'FOTO REKENING KORAN', '$imageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
	}
	
	$imageFileName = '';

	//=====

	if($foto_keuangan_lainnya <> '') { $imageFileName = base64_to_image($backend_folder, $plafond_application_document_image_folder, $foto_keuangan_lainnya); }

	if($imageFileName <> '') {
		
		$query = "	INSERT INTO plafond_application_documents(plafond_application_id, type, image, is_active)
					VALUES('$new_plafond_application_id', 'FOTO KEUANGAN LAINNYA', '$imageFileName', '1') ";
		mysqli_query($mysql_connection, $query);
	}
	
	$imageFileName = '';
	

	//==================================== AFIS CALL ====================================
	
	$query = "select a.*, b.name as occupation_name  
			  from customers a 
			  left join occupations b on a.occupation=b.id 
			  where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	
	$queryAddress = "select b.*, c.name as province_name, d.name as city_name, e.name as district_name, f.name as village_name   
					  from address_customers a  
					  left join address b on a.address_id=b.id 
					  left join provinces c on b.province_id=c.id 
					  left join regencies d on b.regency_id=d.id 
					  left join districts e on b.district_id=e.id 
					  left join villages f on b.village_id=f.id 
					  where a.user_id='$id_customer' and a.address_type='LEGAL' 
					  and a.is_active='1' ";
	$resultAddress= mysqli_query($mysql_connection, $queryAddress);
	$dataAddress  = mysqli_fetch_array($resultAddress);
	
	$afis_api_url	= $afis_api_url.'/andalanku/PlafondRequest';
	
	$body_param		= array();
	$body_param['BranchId']				= $andalan_branch_id;
	$body_param['Name']					= $data['customer_name'];
	$body_param['BirthPlace']			= $data['place_of_birth'];
	$body_param['BirthDate']			= $data['date_of_birth'];
	$body_param['IdNumber']				= $data['id_number'];
	$body_param['MotherName']			= $data['mother_name'];
	$body_param['PhoneArea']			= $data['area_code'];
	$body_param['PhoneNo']				= $data['home_phone_number'];
	$body_param['MobileNo']				= $data['phone_number'];
	
	$occupation_name = $data['occupation_name'];
	
	if($occupation_name=='Karyawan Swasta') 			$occupation_name='KARY';
	else if($occupation_name=='Pegawai Negeri Sipil') 	$occupation_name='PNS';
	else if($occupation_name=='Anggota Polri') 			$occupation_name='POLRI';
	else if($occupation_name=='Wirausaha') 				$occupation_name='WIRA';
	else if($occupation_name=='Profesional') 			$occupation_name='PROF';
	else if($occupation_name=='Pegawai BUMD') 			$occupation_name='BUMD';
	else if($occupation_name=='Pegawai BUMN') 			$occupation_name='BUMN';
	else if($occupation_name=='Lain-lain') 				$occupation_name='LL';
	else if($occupation_name=='Anggota TNI') 			$occupation_name='TNI';
	
	$body_param['JobTypeId'] = $occupation_name;
	
	// ===== ALAMAT KTP
	$query	= "select name from villages where id='$village_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$village_name = $data['name'];

	$query	= "select name from districts where id='$district_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$district_name = $data['name'];

	$query	= "select name from regencies where id='$city_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$city_name = $data['name'];

	$query	= "select name from provinces where id='$province_id'";
	$result	= mysqli_query($mysql_connection, $query);
	$data	= mysqli_fetch_array($result);
	$province_name = $data['name'];

	$arrAlamatKtp					= array();
	$arrAlamatKtp['Alamat']			= $address;
	$arrAlamatKtp['Rt']				= $rt;
	$arrAlamatKtp['Rw']				= $rw;
	$arrAlamatKtp['Kelurahan']		= $village_name;
	$arrAlamatKtp['Kecamatan']		= $district_name;
	$arrAlamatKtp['Kota']			= $city_name;
	$arrAlamatKtp['Provinsi']		= $province_name;
	$arrAlamatKtp['Kodepos']		= substr($zip_code,0, 5);
	$body_param['LegalAddress']		= $arrAlamatKtp;
	
	// ===== ALAMAT DOMISILI	
	// JIKA PARAMETER ALAMAT DOMISILI KOSONG MAKA DIAMBIL DARI ALAMAT YANG SUDAH ADA
	// JIKA BELUM ADA DATA YANG TERSIMPAN, GUNAKAN PARAMETER ALAMAT LEGAL
	if($address_domicile == '') {
		
		$queryAddress = "select b.* 
						from address_customers a 
						left join address b on b.id=a.address_id 
						where user_id='$id_customer' 
						and address_type='DOMICILE' and is_active='1'"; 
		$resultAddress= mysqli_query($mysql_connection, $queryAddress);
		if(mysqli_num_rows($resultAddress) > 0) {
			
			$dataAddress = mysqli_fetch_array($resultAddress);
			
			// ===== ALAMAT DOMISILI	
			$query	= "select name from villages where id='".$dataAddress['village_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$village_name = $data['name'];

			$query	= "select name from districts where id='".$dataAddress['district_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$district_name = $data['name'];

			$query	= "select name from regencies where id='".$dataAddress['regency_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$city_name = $data['name'];

			$query	= "select name from provinces where id='".$dataAddress['province_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$province_name = $data['name'];

			$arrAlamatDomisili				= array();
			$arrAlamatDomisili['Alamat']	= $dataAddress['address'];
			$arrAlamatDomisili['Rt']		= $dataAddress['rt'];
			$arrAlamatDomisili['Rw']		= $dataAddress['rw'];
			$arrAlamatDomisili['Kelurahan']	= $village_name;
			$arrAlamatDomisili['Kecamatan']	= $district_name;
			$arrAlamatDomisili['Kota']		= $city_name;
			$arrAlamatDomisili['City']		= $city_name;
			$arrAlamatDomisili['Provinsi']	= $province_name;
			$arrAlamatDomisili['Kodepos']	= substr($dataAddress['zip_code'], 0, 5);
			$body_param['ResidenceAddress']	= $arrAlamatDomisili;
		}
		else {
			$body_param['ResidenceAddress'] = $arrAlamatKtp;
		}
	}
	else {
		
		$query	= "select name from villages where id='$village_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$village_name = $data['name'];

		$query	= "select name from districts where id='$district_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$district_name = $data['name'];

		$query	= "select name from regencies where id='$city_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$city_name = $data['name'];

		$query	= "select name from provinces where id='$province_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$province_name = $data['name'];

		$arrAlamatDomisili				= array();
		$arrAlamatDomisili['Alamat']	= $address_domicile;
		$arrAlamatDomisili['Rt']		= $rt_domicile;
		$arrAlamatDomisili['Rw']		= $rw_domicile;
		$arrAlamatDomisili['Kelurahan']	= $village_name;
		$arrAlamatDomisili['Kecamatan']	= $district_name;
		$arrAlamatDomisili['Kota']		= $city_name;
		$arrAlamatDomisili['City']		= $city_name;
		$arrAlamatDomisili['Provinsi']	= $province_name;
		$arrAlamatDomisili['Kodepos']	= substr($zip_code_domicile, 0, 5);
		$body_param['ResidenceAddress']	= $arrAlamatDomisili;
		
		if($arrAlamatDomisili['Alamat'] == '') {
			$body_param['ResidenceAddress'] = $arrAlamatKtp;
		}		
	}
	
	// ===== ALAMAT OFFICE	
	// JIKA PARAMETER ALAMAT OFFICE KOSONG MAKA DIAMBIL DARI ALAMAT YANG SUDAH ADA
	// JIKA BELUM ADA DATA YANG TERSIMPAN, GUNAKAN PARAMETER ALAMAT LEGAL
	if($address_domicile == '') {
		
		$queryAddress = "select b.* 
						from address_customers a 
						left join address b on b.id=a.address_id 
						where user_id='$id_customer' 
						and address_type='OFFICE' and is_active='1'"; 
		$resultAddress= mysqli_query($mysql_connection, $queryAddress);
		if(mysqli_num_rows($resultAddress) > 0) {
			
			$dataAddress = mysqli_fetch_array($resultAddress);
			
			// ===== ALAMAT OFFICE	
			$query	= "select name from villages where id='".$dataAddress['village_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$village_name = $data['name'];

			$query	= "select name from districts where id='".$dataAddress['district_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$district_name = $data['name'];

			$query	= "select name from regencies where id='".$dataAddress['regency_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$city_name = $data['name'];

			$query	= "select name from provinces where id='".$dataAddress['province_id']."'";
			$result	= mysqli_query($mysql_connection, $query);
			$data	= mysqli_fetch_array($result);
			$province_name = $data['name'];

			$arrAlamatKantor				= array();
			$arrAlamatKantor['Alamat']		= $dataAddress['address'];
			$arrAlamatKantor['Rt']			= $dataAddress['rt'];
			$arrAlamatKantor['Rw']			= $dataAddress['rw'];
			$arrAlamatKantor['Kelurahan']	= $village_name;
			$arrAlamatKantor['Kecamatan']	= $district_name;
			$arrAlamatKantor['Kota']		= $city_name;
			$arrAlamatKantor['City']		= $city_name;
			$arrAlamatKantor['Provinsi']	= $province_name;
			$arrAlamatKantor['Kodepos']		= substr($dataAddress['zip_code'], 0, 5);
			$body_param['CompanyAddress']	= $arrAlamatKantor;
		}
		else {
			$body_param['CompanyAddress'] = $arrAlamatKtp;
		}
	}
	else {
		
		$query	= "select name from villages where id='$village_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$village_name = $data['name'];

		$query	= "select name from districts where id='$district_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$district_name = $data['name'];

		$query	= "select name from regencies where id='$city_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$city_name = $data['name'];

		$query	= "select name from provinces where id='$province_id_domicile'";
		$result	= mysqli_query($mysql_connection, $query);
		$data	= mysqli_fetch_array($result);
		$province_name = $data['name'];

		$arrAlamatKantor				= array();
		$arrAlamatKantor['Alamat']		= $address_office;
		$arrAlamatKantor['Rt']			= $rt_office;
		$arrAlamatKantor['Rw']			= $rw_office;
		$arrAlamatKantor['Kelurahan']	= $village_name;
		$arrAlamatKantor['Kecamatan']	= $district_name;
		$arrAlamatKantor['Kota']		= $city_name;
		$arrAlamatKantor['Provinsi']	= $province_name;
		$arrAlamatKantor['Kodepos']		= substr($zip_code_office, 0, 5);
		$body_param['CompanyAddress']	= $arrAlamatKantor;
		
		if($arrAlamatKantor['Alamat'] == '') {
			$body_param['CompanyAddress'] = $arrAlamatKtp;
		}		
	}
	
	$body_param['MonthlyIncome']		= $monthly_income;
	$body_param['OtherIncome']			= $additional_income;
	$body_param['SourceOfIncome']		= $side_job;
	$body_param['SurveyDate']			= $survey_date;
	$body_param['ImageKtp']				= '';
	$body_param['ImageKK']				= '';
	$body_param['ImageSlipGaji']		= '';
	$body_param['ImageRekanan']			= '';
	$body_param['ImageOthFinance']		= '';
	$body_param['TicketNo']				= $ticket_number;
	
	$jsonData = json_encode($body_param);
	$queryUpdate = "UPDATE plafond_applications SET json_data = '$jsonData' WHERE ticket_number = '$ticket_number'";
	mysqli_query($mysql_connection, $queryUpdate);
	
	$afis_response	= json_decode(afis_call($afis_api_url, 'POST', $body_param, true));
	$api_response['afis_response'] 	= $afis_response;	
	
	if ($afis_response->StatusCode == 500) {
		
		$queryUpdate = "UPDATE plafond_applications SET afis_success = 0 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	} 
	else {
		$queryUpdate = "UPDATE plafond_applications SET afis_success = 1 WHERE ticket_number = '$ticket_number'";
		mysqli_query($mysql_connection, $queryUpdate);
	}
	
	//==================================== AFIS CALL ====================================
		
	
	//==================== INBOX ====================
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $now;
	$content['status'] 		 	 = 'SUBMITTED';
	$inbox_message				 = 'Dear Konsumen, \r\n\r\n';
	$inbox_message				 .= 'Konsumen yang terhormat, pengajuan Anda sudah kami terima. ';
	$inbox_message				 .= 'Kami akan segera menindaklanjuti permohonan Anda.';
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//insert ke tabel inbox
	$queryInsert = "insert into inbox(customer_id, date, type, title, status, content) 
					values('$id_customer', '$now', 'plafond application', 'Pengajuan Plafond', '0', 
					'$content')";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	$api_response['status'] 		= 'success';
	$api_response['ticket_number'] 	= $ticket_number;
	$api_response['message'] 		= 'Plafond application successfully';
	$api_response['plafond']		= number_format($plafond, 0, ',', '.');
	
	echo json_encode($api_response);
	exit;
?>