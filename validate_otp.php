<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer	= sanitize_sql_string(trim($_POST['id_customer']));
	$otp_code		= sanitize_sql_string(trim($_POST['otp_code']));
	
	$queryCheck = "select id_customer from customers where sms_otp='$otp_code' and id_customer='$id_customer'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	if(!$skip_otp) {
		
		if(mysqli_num_rows($resultCheck) == 0) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid customer id or otp code';
			
			echo json_encode($api_response);
			exit;
		}
	}
	
	//sukses
	$queryCheck = "update customers set sms_verification='VERIFIED' where id_customer='$id_customer'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'OTP validation successful';
	
	echo json_encode($api_response);
	exit;
?>