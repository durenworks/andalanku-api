<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	function downloadFile($url, $path)
	{
		$newfname = $path;
		$file = fopen ($url, 'rb');
		$file_created = false;
		if ($file) {
			
			$file_created = true;
			
			$newf = fopen ($newfname, 'wb');
			if ($newf) {
				while(!feof($file)) {
					fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
				}
			}
		}
		if ($file) {
			fclose($file);
		}
		if ($newf) {
			fclose($newf);
		}
		
		return $file_created;
	}

	$id_customer	= sanitize_int(trim($_POST['id_customer']));
	$agreement_no	= sanitize_sql_string(trim($_POST['agreement_no']));
	
	if($id_customer=='0' || $agreement_no=='' ) {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}	
		
	$query = "select andalan_customer_id, customer_name from customers where id_customer='$id_customer'";
	$result= mysqli_query($mysql_connection, $query);
	$data  = mysqli_fetch_array($result);
	$andalan_customer_id 		= $data['andalan_customer_id'];
	$folder_andalan_customer_id	= str_replace("/","",$andalan_customer_id);
	$customer_name				= $data['customer_name'];
	$customer_name				= str_replace(' ','-',$customer_name);
	
	$base_url = $afis_api_url;
	
	$afis_api_url  = $base_url.'/Agreement/detailFinancial/'.$agreement_no;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$branchId 	   =  trim($responseArray[0]->BranchId); 
	
	$afis_api_url  = $base_url.'/Agreement/detailHistoryPayment/'.$customer_name.'/'.$branchId.'/'.$agreement_no;
	
	$folder 	   = 'files/'.$folder_andalan_customer_id;
	if(!file_exists($folder)) mkdir($folder);
	
	$newFileName   = 'Andalanku_Kartu_Piutang_'.md5($andalan_customer_id.$branchId.$agreement_no).'.pdf';
	
	if(downloadFile($afis_api_url, $folder . '/' . $newFileName)) {
		$api_response['status'] 		= 'success';
		$api_response['kartu_piutang'] 	= $api_url.'/'.$folder . '/' . $newFileName;
	}
	else {
		
		// jika tidak ada file dari afis maka pakai file yang lama
		if(file_exists($folder . '/' . $newFileName)) {
			$api_response['status'] 		= 'success';
			$api_response['kartu_piutang'] 	= $api_url.'/'.$folder . '/' . $newFileName;
		}
		else {
			$api_response['status'] 		= 'failed';
			$api_response['message'] 		= 'Data belum tersedia';
			$api_response['kartu_piutang'] 	= '';
		}
	}
	
	echo json_encode($api_response);
	exit;
?>