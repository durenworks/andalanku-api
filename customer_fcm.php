<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer 	= sanitize_int($_REQUEST['id_customer']);
	$fcm_token		= sanitize_sql_string(trim($_REQUEST['fcm_token']));
	
	if($id_customer=='0' || $fcm_token=='') {
			
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$query = "update customers set fcm_token='$fcm_token' where id_customer='$id_customer'";
	mysqli_query($mysql_connection, $query);
	
	
	$api_response['status'] 	= 'success';
	$api_response['message'] 	= 'FCM token updated successfully';
	
	echo json_encode($api_response);
	exit;
?>