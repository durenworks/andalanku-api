<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_province = sanitize_int($_REQUEST["id_province"]);
	
	$query 	= "select * from regencies where province_id='$id_province' order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$city_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$city_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 	= 'success';
	$api_response['city_list'] 	= $city_list;
	
	echo json_encode($api_response);
	exit;
?>