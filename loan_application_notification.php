<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require 'plugins/phpmailer/src/Exception.php';
	require 'plugins/phpmailer/src/PHPMailer.php';
	require 'plugins/phpmailer/src/SMTP.php';
	
	$ticket_number	= sanitize_sql_string(trim($_POST['ticket_number']));
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$queryCheck = "select * from loan_applications where ticket_number='$ticket_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid ticket number';
		
		echo json_encode($api_response);
		exit;
	}
	
	/*
		NAP = Input Aplikasi
		CST = Analisa Kredit
		APR = Approval Kredit
		GLV = Approved
		RJC = Reject
		CAN = Canceled
	*/
	
	if($status == 'NAP') {
		$status			= 'Input Aplikasi';
		$status_afis	= 'NAP';
	}
	else if($status == 'CST') {
		$status			= 'Analisa Kredit';
		$status_afis	= 'CST';
	}
	else if($status == 'APR') {
		$status			= 'Approval Kredit';
		$status_afis	= 'APR';
	}
	else if($status == 'GLV') {
		$status			= 'Approved';
		$status_afis	= 'GLV';
	}
	else if($status == 'RJC') {
		$status			= 'Reject';
		$status_afis	= 'RJC';
	}
	else if($status == 'CAN') {
		$status			= 'Canceled';
		$status_afis	= 'CAN';
	}
	else {
		
		$api_response['status'] 		= 'failed';
		$api_response['message'] 		= 'Unknown status code';
		
		echo json_encode($api_response);
		exit;
	}
	
	
	
	$data				 = mysqli_fetch_assoc($resultCheck);
	$loan_application_id = $data['id'];
	$customer_id 		 = $data['customer_id'];
	$loan_date 			 = date("d-m-Y", strtotime($data['date']));
	$now				 = date('Y-m-d H:i:s');
	$loan_application_referal 	= $data['loan_application_referal'];
	$customer_name				= $data['loan_customer_name'];
	$email						= $data['loan_email'];
	$product					= $data['product'];
	$collateral					= $data['collateral'];
	$collateral_year			= $data['collateral_year'];
	$otr						= $data['otr'];
	$tenor						= $data['tenor'];
	
	if($product == 'NEW CAR') $productName = 'Mobil Baru';
	if($product == 'USED CAR') $productName = 'Mobil Bekas';
	if($product == 'DANA ANDALANKU') $productName = 'Dana Andalanku';
	
	if($product == 'NEW CAR' || $product == 'USED CAR') {
		$mobil = 'Mobil \t\t : '.$collateral.'<br>';
		$str_otr   = 'Harga OTR &nbsp; : Rp '.number_format($otr,2,',','.').'<br>';
	}
	else if($product == 'DANA ANDALANKU')  {
		$mobil = '';
		$str_otr   = 'Harga OTR &nbsp; : Rp '.number_format($otr,2,',','.').'<br>';
	}
	
	if ($product == 'DANA ANDALANKU') {
		$car_year = $collateral_year;
		
		$first_slash	= stripos($collateral,'/') + 1;
		$last_slash		= strripos($collateral,'/');
		$substr_length	= $last_slash - $first_slash;
		$collateral_car_name = substr($collateral, $first_slash, $substr_length);
	}
	if ($product == 'NEW CAR') {
		$car_year = date("Y");
		$collateral_year = $car_year;
	}
	
	$queryUpdate = "update loan_applications set status='$status' where ticket_number='$ticket_number'";
	$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	
	//insert ke tabel loan_applications_status
	$queryInsert = "insert into loan_application_status(loan_application_id, date, 
					status, status_afis) 
					values('$loan_application_id', '$now', 
					'$status', '$status_afis')";
	mysqli_query($mysql_connection, $queryInsert);
	
	//==================== INBOX dan EMAIL ====================
	$message = 'Dear Konsumen yang terhormat, \r\n\r\n';
	
	$emailContent = 'Pengguna Yth, <br><br>';
	$emailContent = $emailContent.'Terima kasih telah menggunakan aplikasi Andalanku.<br>';
	$emailContent = $emailContent.'Kami telah menerima pengajuan kredit dari Anda dan telah memproses permohonan Anda.<br><br>';
	$emailContent = $emailContent.'Nomor tiket pengajuan kredit Anda adalah : <b>'.$ticket_number.'</b><br><br>';
	
	if($status == 'Input Aplikasi') {
		$message .= 'Pengajuan kredit anda saat ini dalam proses input aplikasi. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' - Input Aplikasi';
		$emailContent = $emailContent.'Kami menginformasikan bahwa pengajuan tersebut saat ini dalam proses input aplikasi.';
	}
	else if($status == 'Analisa Kredit') {
		$message .= 'Pengajuan kredit anda saat ini memasuki tahap analisa kredit. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Sedang Dianalisa';
		$emailContent = $emailContent.'Kami menginformasikan bahwa untuk pengajuan tersebut sedang kami lakukan proses analisa kredit.';
	}
	else if($status == 'Approval Kredit') {
		$message .= 'Pengajuan kredit anda saat ini memasuki tahap approval kredit. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Approval Kredit Pengajuan Kredit Nomor '.$ticket_number;
		$emailContent = $emailContent.'Kami menginformasikan bahwa pengajuan tersebut saat ini memasuki tahap approval kredit.';
	}
	else if($status == 'Approved') {
		$message .= 'Pengajuan kredit anda sudah disetujui. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Telah Disetujui';
		$emailContent = $emailContent.'Kami menginformasikan bahwa pengajuan tersebut telah kami setujui.';
	}
	else if($status == 'Reject') {
		$message .= 'Pengajuan kredit anda belum dapat kami setujui. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Ditolak';
		$emailContent = $emailContent.'Kami menginformasikan bahwa pengajuan tersebut belum dapat kami terima.';
	}
	else if($status == 'Canceled') {
		$message .= 'Pengajuan kredit anda telah dibatalkan. Untuk informasi lebih lanjut silahkan hubungi Customer Service kami. \r\n';
		$emailSubject = 'Pengajuan Kredit Nomor '.$ticket_number.' Telah Dibatalkan';
		$emailContent = $emailContent.'Kami menginformasikan bahwa pengajuan tersebut belum dapat kami terima dan telah kami batalkan.';
	}
	
	$message .= 'Produk &emsp;&emsp; : ' . $productName . '\r\n';
	$message .= str_replace('<br>', '\r\n', $mobil);
	$message .= str_replace('<br>', '\r\n', $str_otr);
	$message .= 'Tenor &emsp;&emsp;&nbsp;&nbsp;&nbsp;: ' . $tenor . ' bulan \r\n';
	
	if($product == 'DANA ANDALANKU') {
		$message .= 'Jenis Agunan: BPKB \r\n';
		$message .= 'Jaminan  &emsp;&nbsp;&nbsp; : ' . $collateral_car_name .' '. $collateral_year . '\r\n';
	}
	else {
		$message .= 'Asuransi '.$tab.' : ' . $insurance . '\r\n';
		$message .= 'Uang Muka '.$tab.' : Rp '.number_format($dp_amount,2,',','.').'\r\n';
	}
	
	$emailContent = $emailContent.'Produk : '.$productName.'<br>';
	$emailContent = $emailContent.$mobil;
	$emailContent = $emailContent.$otr;
	$emailContent = $emailContent.'Tenor : '.$tenor.' bulan<br><br><br>';
	$emailContent = $emailContent.'Customer service kami akan menghubungi Anda untuk proses selanjutnya.<br><br>';
	$emailContent = $emailContent."Terima kasih, <br><br>";
	$emailContent = $emailContent."Andalan Finance";
	
	$content = array();
	$content['message'] 		 = $message;
	$content['ticket_number'] 	 = $ticket_number;
	$content['status'] 			 = $status;
	$content['tangal_pengajuan'] = $loan_date;
	$content = json_encode($content);
	
	//update tabel inbox
	$queryCheck = "select id from inbox where content like '%$ticket_number%' ";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck	= mysqli_fetch_array($resultCheck);
	$id_inbox	= $dataCheck['id'];
	$queryInsert = "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
	mysqli_query($mysql_connection, $queryInsert);
	//===============================================
	
	// Kirim email pemberitahuan kepada orang yang diajukan	
	/*if($loan_application_referal == 'Y') {
		
		$customerName	= $customer_name;
		$customerEmail	= $email;		
		$mail = new PHPMailer();
		$mail->isSMTP();  
		$mail->Host       = $smtp_url;  							
		$mail->Port       = $smtp_port;                             
		$mail->SMTPAuth   = true;                                   
		$mail->Username   = $smtp_username;                     	
		$mail->Password   = $smtp_password;                         
		$mail->setFrom($app_email_from, $app_email_from_name);
		$mail->addAddress($customerEmail, $customerName);
		$mail->isHTML(true);                                  		
		$mail->Subject = $emailSubject;
		$mail->Body    = $emailContent;
		$mail->send();
	}
	
	// Kirim email pemberitahuan kepada user yang melakukan input
	$queryCheck = "select customer_name, email from customers where id_customer='$id_customer'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck	= mysqli_fetch_array($resultCheck);
	$customerName	= $dataCheck['customer_name'];
	$customerEmail	= $dataCheck['email'];
	$mail = new PHPMailer();
	$mail->isSMTP();  
	$mail->Host       = $smtp_url;  							
	$mail->Port       = $smtp_port;                             
	$mail->SMTPAuth   = true;                                   
	$mail->Username   = $smtp_username;                     	
	$mail->Password   = $smtp_password;                         
	$mail->setFrom($app_email_from, $app_email_from_name);
	$mail->addAddress($customerEmail, $customerName);
	$mail->isHTML(true);                                  		
	$mail->Subject = $emailSubject;
	$mail->Body    = $emailContent;
	$mail->send();*/
	
	//=================================================================================================================================
	//=================================================================================================================================
	
	// khusus untuk status GLV (Approved), jika user baru menjadi user AFIS,
	// simpan andalan customer id dan data lainnya dari AFIS
	if($status == 'Approved') { 
		
		$queryCustomer = "select * from customers where id_customer='$customer_id'";
		$resultCustomer= mysqli_query($mysql_connection, $queryCustomer);
		$dataCustomer  = mysqli_fetch_assoc($resultCustomer);
		
		if($dataCustomer['andalan_customer_id']=='') {
			
			$id_number		= $dataCustomer['id_number'];
			$phone_number	= $dataCustomer['phone_number'];
			
			$afis_api_url  = $afis_api_url.'/Supplier/Info/'.$id_number.'/'.$phone_number;
			$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
			$responseArray = $afis_response->Response->Data;
			$andalan_customer_id =  trim($responseArray[0]->CustomerID); 
			
			$customerGender		= $responseArray[0]->Gender;
			$customerBirthPlace	= $responseArray[0]->BirthPlace;
			$customerBirthDate	= $responseArray[0]->BirthDate;
			$customerMotherName	= $responseArray[0]->MotherName;
			
			$queryUpdate = "update customers set andalan_customer_id='$andalan_customer_id', 
							gender='$customerGender',  
							place_of_birth='$customerBirthPlace', date_of_birth='$customerBirthDate', 
							mother_name='$customerMotherName' 
							where id_customer='$customer_id'";
			mysqli_query($mysql_connection, $queryUpdate); 
			
			//simpan alamat
			$cityName	= trim($responseArray[0]->City); 
			$queryCity	= "select * from regencies where name like '%$cityName'";
			$resultCity	= mysqli_query($mysql_connection, $queryCity);
			if(mysqli_num_rows($resultCity) == 1) {
				
				$dataCity	= mysqli_fetch_array($resultCity);
				$cityID		= $dataCity['id'];
				$provinceID	= $dataCity['province_id'];
			}
			else if(mysqli_num_rows($resultCity) > 1) {
				
				$queryCity	= "select * from regencies where name = 'KOTA $cityName'";
				$resultCity	= mysqli_query($mysql_connection, $queryCity);
				$dataCity	= mysqli_fetch_array($resultCity);
				$cityID		= $dataCity['id'];
				$provinceID	= $dataCity['province_id'];
			}
			
			$districtName	= trim($responseArray[0]->Kecamatan); 
			$queryDistrict	= "select * from districts where name = '$districtName'";
			$resultDistrict	= mysqli_query($mysql_connection, $queryDistrict);
			$dataDistrict	= mysqli_fetch_array($resultDistrict);
			$districtID		= $dataDistrict['id'];
			
			$villageName	= trim($responseArray[0]->Kelurahan); 
			$queryVillage	= "select * from villages where name = '$villageName'";
			$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
			$dataVillage	= mysqli_fetch_array($resultVillage);
			$villageID		= $dataVillage['id'];
			
			if($villageID=='' || $villageID=='0') {
			
				//ambil id kelurahan yang terakhir
				$queryVillage	= "select id from villages where district_id = '$districtID' order by id DESC";
				$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
				$dataVillage	= mysqli_fetch_array($resultVillage);
				$lastVillageID	= $dataVillage['id'];
				$villageID		= $lastVillageID + 1;
				
				$queryVillage	= "insert into villages(id, district_id, name) values('$villageID', '$districtID', '$villageName')";
				mysqli_query($mysql_connection, $queryVillage);
			}
			
			$zipCode	= trim($responseArray[0]->ZipCode);
			$address	= trim($responseArray[0]->Address);
			$rt			= trim($responseArray[0]->RT);
			$rw			= trim($responseArray[0]->RW);
			
			$queryInsert = "insert into address(province_id, regency_id, district_id, village_id,  
							zip_code, address, rt, rw) 
							values('$provinceID', '$cityID', '$districtID', '$villageID', 
							'$zipCode', '$address', '$rt', '$rw')";
			mysqli_query($mysql_connection, $queryInsert);
			
			$queryID 		= "select id from address where address='$address' order by id DESC LIMIT 1";
			$resultID		= mysqli_query($mysql_connection, $queryID);
			$dataID  		= mysqli_fetch_assoc($resultID);
			$id_address 	= $dataID['id'];
			
			$addressType = $responseArray[0]->AddressType;
			
			if($addressType == 'L') $addressType = 'LEGAL';
			else if($addressType == 'D') $addressType = 'DOMICILE';
			else if($addressType == 'O') $addressType = 'OFFICE';
			else $addressType = 'DOMICILE';
			
			//update dulu yang lain menjadi inactive
			$queryUpdate = "update address_customers set is_active='0' where user_id='$customer_id' and address_type='$addressType' ";
			mysqli_query($mysql_connection, $queryUpdate);
			
			$queryInsert = "insert into address_customers(address_type, user_id,  address_id, is_active) 
							values('$addressType', '$customer_id', '$id_address', '1')";
			mysqli_query($mysql_connection, $queryInsert);
		}
	}
	
	
	// TO-DO : Kirim push notification
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Loan application updated successfully';
	
	echo json_encode($api_response);
	exit;
?>