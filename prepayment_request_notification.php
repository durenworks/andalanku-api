<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$ticket_number	= sanitize_sql_string(trim($_POST['ticket_number']));
	$status			= sanitize_sql_string(trim($_POST['status']));
	
	$queryCheck = "select * from prepayment_requests where ticket_number='$ticket_number'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Invalid ticket number';
		
		echo json_encode($api_response);
		exit;
	}
	
	$data			= mysqli_fetch_assoc($resultCheck);
	$request_date	= $data['request_date'];
	$id_customer	= $data['customer_id'];
	
	$queryUpdate = "update prepayment_requests set status='$status' where ticket_number='$ticket_number'";
	$resultUpdate= mysqli_query($mysql_connection, $queryUpdate);
	
	//==================== INBOX ====================
	$inbox_message = 'Konsumen yang terhormat, \r\n';
	if($status == 'APPROVED') $inbox_message .= 'Pengajuan pelunasan maju Anda sudah disetujui.';
	else if($status == 'REJECTED') $inbox_message .= 'Mohon maaf, pengajuan pelunasan maju Anda belum dapat kami setujui. Terima kasih';
	else if($status == 'EXPIRED') $inbox_message .= 'Mohon maaf, pengajuan pelunasan maju Anda telah kadaluarsa. Terima kasih';
	
	$content					 = array();
	$content['ticket_number'] 	 = $ticket_number;
	$content['input_date'] 		 = $request_date;
	$content['status'] 		 	 = $status;
	$content['message']			 = $inbox_message;
	$content = json_encode($content);
	
	//update tabel inbox
	$now			= date('Y-m-d H:i:s');
	$queryCheck 	= "select id from inbox where content like '%$ticket_number%' ";
	$resultCheck	= mysqli_query($mysql_connection, $queryCheck);
	$dataCheck		= mysqli_fetch_array($resultCheck);
	$id_inbox		= $dataCheck['id'];
	$queryInsert 	= "update inbox set date='$now', status='0', content='$content' where id='$id_inbox'";
	mysqli_query($mysql_connection, $queryInsert);	
	//===============================================	
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Prepayment request updated successfully';
	
	echo json_encode($api_response);
	exit;
?>