<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	
	include "validate_token.php";
	
	$id_customer  	= sanitize_int(trim($_POST['id_customer']));
	$address_type 	= sanitize_sql_string(trim($_POST['address_type']));
	$address		= sanitize_sql_string(trim($_POST['address']));
	$rt				= sanitize_sql_string(trim($_POST['rt']));
	$rw				= sanitize_sql_string(trim($_POST['rw']));
	$kelurahan		= sanitize_sql_string(trim($_POST['kelurahan']));
	$kecamatan		= sanitize_sql_string(trim($_POST['kecamatan']));
	$city			= sanitize_sql_string(trim($_POST['city']));
	$zip_code		= sanitize_sql_string(trim($_POST['zip_code']));
	$phone_number	= sanitize_sql_string(trim($_POST['phone_number']));
	$mobile_number	= sanitize_sql_string(trim($_POST['mobile_number']));
	
	if($address_type=='' || $address=='' || $rt=='' || $rw=='' || $kelurahan=='' || $kecamatan=='' || 
	   $city=='' || $zip_code=='' || $mobile_number=='') {
		
		$api_response['status']		= 'failed';
		$api_response['message'] 	= 'Input not complete';
		
		echo json_encode($api_response);
		exit;
	}
	
	$queryCheck = "select id_customer_update from customer_updates where id_customer='$id_customer' and status='PENDING'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) > 0) {
		
		$api_response['status'] 	= 'failed';
		$api_response['message'] 	= 'Pending previous update';
		
		echo json_encode($api_response);
		exit;
	}
	
	$now = date("Y-m-d H:i:s");
	
	$queryInsert = "insert into customer_updates(id_customer, address_type, address, request_date, 
					rt, rw, kelurahan, kecamatan, city, zip_code, 
					phone_number, mobile_number, status) 
					values('$id_customer', '$address_type', '$address', '$now',
					'$rt', '$rw', '$kelurahan', '$kecamatan', '$city', '$zip_code', 
					'$phone_number', '$mobile_number', 'PENDING')";
	mysqli_query($mysql_connection, $queryInsert);
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Update Profile successful';
	
	echo json_encode($api_response);
	exit;
?>