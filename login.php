<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	include "afis_call.php";
	
	$email			= sanitize_sql_string(trim($_POST['email']));
	$password		= sanitize_sql_string(trim($_POST['password']));
	$password 		= md5($password);
	
	$queryCheck = "select * from customers where email='$email' and password='$password' and status='ACTIVE'";
	$resultCheck= mysqli_query($mysql_connection, $queryCheck);
	if(mysqli_num_rows($resultCheck) == 0) {
		
		$ip 	= $_SERVER['REMOTE_ADDR'];  
		$time	= time();  
		$diff 	= (time()-600); // 10 minutes 
		
		$queryInsert = "INSERT INTO login_limit(ip_address, time_diff) VALUES ('$ip','$time')";
		mysqli_query($mysql_connection, $queryInsert); 
		
		$query 	= "SELECT COUNT(id) FROM login_limit WHERE ip_address LIKE '$ip' AND time_diff > $diff";
		$result = mysqli_query($mysql_connection, $query); 
		$count 	= mysqli_fetch_array($result);
		if($count[0] > 3) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'You are allowed 3 attempts in 10 minutes';
		}
		else {
		
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'Invalid email or password';
		}
		
		echo json_encode($api_response);
		exit;
	}
	else {
		
		$ip 	= $_SERVER['REMOTE_ADDR'];  
		$time	= time();  
		$diff 	= (time()-600); // 10 minutes 
		
		$query 	= "SELECT COUNT(id) FROM login_limit WHERE ip_address LIKE '$ip' AND time_diff > $diff";
		$result = mysqli_query($mysql_connection, $query); 
		$count 	= mysqli_fetch_array($result);
		if($count[0] > 3) {
			
			$api_response['status'] 	= 'failed';
			$api_response['message'] 	= 'You are allowed 3 attempts in 10 minutes';
			echo json_encode($api_response);
			exit;
		}		
	}
	
	$data  			= mysqli_fetch_assoc($resultCheck);
	$id_customer	= $data['id_customer'];
	$id_number		= $data['id_number'];
	$phone_number	= $data['phone_number'];
	$now			= date('Y-m-d H:i:s');
	
	$queryUpdate = "update customers set last_login='$now' where email='$email' and password='$password' ";
	mysqli_query($mysql_connection, $queryUpdate);
	
	//=================================================================================================================	
	$afis_api_url  = $afis_api_url.'/Supplier/Info/'.$id_number.'/'.$phone_number;
	$afis_response = json_decode(afis_call($afis_api_url, 'GET'));
	$responseArray = $afis_response->Response->Data;
	$andalan_customer_id =  trim($responseArray[0]->CustomerID); 
	
	$customerGender		= $responseArray[0]->Gender;
	$customerBirthPlace	= $responseArray[0]->BirthPlace;
	$customerBirthDate	= $responseArray[0]->BirthDate;
	$customerMotherName	= $responseArray[0]->MotherName;
	
	if($andalan_customer_id <>'' && $andalan_customer_id <>'0') {
		
		$queryUpdate = "update customers set andalan_customer_id='$andalan_customer_id', 
					    gender='$customerGender',  
						place_of_birth='$customerBirthPlace', date_of_birth='$customerBirthDate', 
						mother_name='$customerMotherName' 
						where id_customer='$id_customer'";
		mysqli_query($mysql_connection, $queryUpdate); 
		
		//simpan alamat
		$cityName	= trim($responseArray[0]->City); 
		$queryCity	= "select * from regencies where name like '%$cityName'";
		$resultCity	= mysqli_query($mysql_connection, $queryCity);
		if(mysqli_num_rows($resultCity) == 1) {
			
			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}
		else if(mysqli_num_rows($resultCity) > 1) {
			
			$queryCity	= "select * from regencies where name = 'KOTA $cityName'";
			$resultCity	= mysqli_query($mysql_connection, $queryCity);
			$dataCity	= mysqli_fetch_array($resultCity);
			$cityID		= $dataCity['id'];
			$provinceID	= $dataCity['province_id'];
		}
		
		$districtName	= trim($responseArray[0]->Kecamatan); 
		$queryDistrict	= "select * from districts where name = '$districtName'";
		$resultDistrict	= mysqli_query($mysql_connection, $queryDistrict);
		$dataDistrict	= mysqli_fetch_array($resultDistrict);
		$districtID		= $dataDistrict['id'];
		
		$villageName	= trim($responseArray[0]->Kelurahan); 
		$queryVillage	= "select * from villages where name = '$villageName'";
		$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
		$dataVillage	= mysqli_fetch_array($resultVillage);
		$villageID		= $dataVillage['id'];
		
		if($villageID=='' || $villageID=='0') {
			
			//ambil id kelurahan yang terakhir
			$queryVillage	= "select id from villages where district_id = '$districtID' order by id DESC";
			$resultVillage	= mysqli_query($mysql_connection, $queryVillage);
			$dataVillage	= mysqli_fetch_array($resultVillage);
			$lastVillageID	= $dataVillage['id'];
			$villageID		= $lastVillageID + 1;
			
			$queryVillage	= "insert into villages(id, district_id, name) values('$villageID', '$districtID', '$villageName')";
			mysqli_query($mysql_connection, $queryVillage);
		}
		
		$zipCode	= trim($responseArray[0]->ZipCode);
		$address	= trim($responseArray[0]->Address);
		$rt			= trim($responseArray[0]->RT);
		$rw			= trim($responseArray[0]->RW);
		
		$queryInsert = "insert into address(province_id, regency_id, district_id, village_id,  
						zip_code, address, rt, rw) 
						values('$provinceID', '$cityID', '$districtID', '$villageID', 
						'$zipCode', '$address', '$rt', '$rw')";
		mysqli_query($mysql_connection, $queryInsert);
		
		$queryID 		= "select id from address where address='$address' order by id DESC LIMIT 1";
		$resultID		= mysqli_query($mysql_connection, $queryID);
		$dataID  		= mysqli_fetch_assoc($resultID);
		$id_address 	= $dataID['id'];
		
		$addressType = $responseArray[0]->AddressType;
		
		if($addressType == 'L') $addressType = 'LEGAL';
		else if($addressType == 'D') $addressType = 'DOMICILE';
		else if($addressType == 'O') $addressType = 'OFFICE';
		else $addressType = 'DOMICILE';
		
		//update dulu yang lain menjadi inactive
		$queryUpdate = "update address_customers set is_active='0' where user_id='$id_customer' and address_type='$addressType' ";
		mysqli_query($mysql_connection, $queryUpdate);
		
		$queryInsert = "insert into address_customers(address_type, user_id,  address_id, is_active) 
						values('$addressType', '$id_customer', '$id_address', '1')";
		mysqli_query($mysql_connection, $queryInsert);
	}
	
	//=================================================================================================================	
	
	$api_response['status'] 		= 'success';
	$api_response['message'] 		= 'Login successful';
	$api_response['id_customer'] 	= $data['id_customer'];
	$api_response['customer_name'] 	= $data['customer_name'];
	$api_response['id_number'] 		= $data['id_number'];
	$api_response['email'] 			= $data['email'];
	
	echo json_encode($api_response);
	exit;
?>