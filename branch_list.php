<?php
	ini_set("display_errors","0"); 
	error_reporting(0); 
	
	include "inc-db.php";
	include "sanitize.inc.php";
	include "validate_token.php";
	
	$query 	= "select * from branches order by name ASC ";
	$result = mysqli_query($mysql_connection, $query);
	
	$branche_list = array();
	$i = 0;
	
	while ($data = mysqli_fetch_assoc($result)) {
		$branche_list[$i] = $data;
		$i++;
	}
	
	$api_response['status'] 		= 'success';
	$api_response['branche_list'] 	= $branche_list;
	
	echo json_encode($api_response);
	exit;
?>